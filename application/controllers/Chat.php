<?php
require_once APPPATH.'controllers/Main.php';
class Chat extends Main {
        
        public function __construct()
        {
                parent::__construct();                
                /*if(empty($_SERVER['HTTP_VER'])){          
                    echo 'denied';
                    die();
                } */           
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                $this->load->model('querys');
        }
        
        public function loadView($param = array('view'=>'main'))
        {                                        
            $template = 'chat';
            $this->load->view($template,$param);
        }
        
        public function index($x = ''){
            $this->loadView('chat');
        }
        
        public function getMessages(){
            $this->form_validation->set_rules('user_id','user','required|integer');
            $this->form_validation->set_rules('destino','Destino','required|integer');
            if($this->form_validation->run()){
                $mensajes = $this->db->get_where('mensajes',array('user_id'=>$this->input->post('user_id')));
                if($mensajes->num_rows()==0){
                    $mensajes = array();
                    $this->db->insert('mensajes',array('user_id'=>$this->input->post('user_id'),'mensajes'=>'[]'));
                }else{
                    $me = array();
                    foreach($mensajes->result() as $m){
                        foreach(json_decode($m->mensajes) as $men){
                            if(($men->destino==$this->input->post('destino') && $men->user_id==$this->input->post('user_id')) || $men->user_id==$this->input->post('destino')){
                                if(empty($_POST['pedido']) || (!empty($_POST['pedido']) && !empty($men->pedido) && $men->pedido==$_POST['pedido'])){
                                    $me[] = $men;
                                }
                            }
                        }
                    }
                    $mensajes = $me;
                }
                
                echo json_encode($mensajes);
            }
        }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
