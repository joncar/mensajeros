<?php
require_once APPPATH.'controllers/Api.php';
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, ver, repartidor");
class Api_clientes extends Api {
        
        public function __construct()
        {
                parent::__construct();
        }
        
        public function clientes($x = '',$y = ''){
            if(empty($_POST['lat']) && empty($_POST['lon'])){
                $this->as = array('clientes'=>'user');
                if(isset($_POST) && !empty($_POST['search_field']) && in_array('password',$_POST['search_field'])){
                    foreach($_POST['search_field'] as $n=>$p){
                        if($p=='password'){
                            $_POST['search_text'][$n] = md5($_POST['search_text'][$n]);
                        }
                    }
                }
                $crud = $this->crud_function('',''); 
                $crud->unset_delete();
                $crud->unset_fields('fecha_registro');
                if($crud->getParameters()=='edit'){
                    $crud->required_fields('email');
                }elseif($crud->getParameters()=='add'){
                    $crud->required_fields('email','ciudades_id','password','celular','nombre');
                }
                 //Callbacks
                $crud->callback_before_update(function($post,$primary){
                    if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                        $post['password'] = md5($post['password']);                
                    }
                    return $post;
                });
                
                $crud->callback_before_insert(function($post){
                    $post['status'] = 1;
                    $post['admin'] = 0;
                    $post['fecha_registro'] = date("Y-m-d H:i:s");
                    $post['password'] = md5($post['password']);
                    return $post;
                });
                
                $crud->callback_after_insert(function($post,$primary){
                    get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>3));
                    get_instance()->db->insert('clientes',array('user_id'=>$primary));
                });

                $crud->callback_column('admin',function($val,$row){
                    $rep = get_instance()->db->get_where('clientes',array('user_id'=>$row->id));                
                    if($rep->num_rows()>0){
                        $datos = array(
                            'clientes_id'=>$rep->row()->id,
                            'convenio'=>$rep->row()->empresa_afiliada
                        );

                        return json_encode($datos);
                    }
                    return '{}';
                });
            }else{
                if(!empty($_POST['email'])){
                    unset($_POST['email']);
                }
                $crud = $this->crud_function('',''); 
                $crud->required_fields('lat','lon');
            }                                   
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function ciudades(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function tipos_servicios(){
            $crud = $this->crud_function('','');
            
            $crud->callback_column('descripcion',function($val,$row){
               $porcentaje = array();
                foreach($this->db->get_where('porcentaje_km_extra',array('tipos_servicios_id'=>$row->id))->result() as $p){
                    $porcentaje[] = $p;
                }
               return json_encode(array('porcentaje'=>$porcentaje,'descripcion'=>$row->descripcion)); 
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function ajustes(){
            $crud = $this->crud_function('','');
            $crud->field_type('cuentas_bancarias',function($val,$row){
               return $row->cuentas_bancarias; 
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function pedidos($x = '',$y = ''){                         
            $crud = $this->crud_function('','');            
            $crud->set_relation('repartidores_id','repartidores','user_id');
            $crud->set_relation('j8c93d82d.user_id','user','{nombre}|{email}|{foto}|{celular}');
            $crud->order_by('id','DESC');
            $crud->callback_column('descripcion',function($val,$row){
                $ar = array();
                $ar['descripcion'] = $val;
                $ar['tareas'] = array();
                foreach(get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$row->id))->result() as $d){
                    $ar['tareas'][] = $d;
                }
                return json_encode($ar);
            });
            $crud->callback_before_insert(function($post){
                $post['fecha_solicitud'] = date("Y-m-d H:i:s");
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->load->model('querys');
                get_instance()->querys->guardar_detalles(json_decode($_POST['detalles']), $primary);                
                return true;
            });
            if($crud->getParameters()=='edit'){
                $crud->required_fields('status');
            }
            //$crud->where('pedidos.id','1');
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function mensajeros(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_subject('mensajeros');
            $crud->set_table('repartidores');
            $crud->unset_read()->unset_delete()->unset_edit()->unset_add()->unset_print()->unset_export();
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        
        function calificaciones(){
            $crud = $this->crud_function('','');
            $crud->required_fields('calificacion');
            $crud->callback_after_insert(function($post){
               $repartidor = get_instance()->db->get_where('repartidores',array('id'=>$post['repartidores_id']));
               if($repartidor->num_rows()>0){
                   get_instance()->db->update('repartidores',array('calificacion'=>$post['calificacion']+$repartidor->row()->calificacion),array('id'=>$post['repartidores_id']));
               }
            });
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        
        function favoritos(){
            $crud = $this->crud_function('','');
            $crud->required_fields('nombre');
            $crud->callback_column('direccion1',function($val,$row){
               return $row->direccion1; 
            });
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        
        function uploadPhoto(){                  
            $this->form_validation->set_rules('image','Imagen','required');
            $this->form_validation->set_rules('user_id','user','required');
            if($this->form_validation->run()){
                $user = $this->db->get_where('user',array('id'=>$_POST['user_id']));
                if($user->num_rows()>0){                    
                    $str="data:image/jpeg;base64,";
                    $data=str_replace($str,"",$_POST['image']); 
                    $data = str_replace(' ', '+', $data);
                    $data = base64_decode($data);
                    $name = rand(1024,5000).$_POST['user_id'].'.jpeg';
                    file_put_contents('img/fotos/'.$name, $data);
                    //Verificar si ya tenia foto registrada                
                    if(file_exists('img/fotos/'.$user->row()->foto)){
                        unlink('img/fotos/'.$user->row()->foto);
                    }
                    $this->db->update('user',array('foto'=>$name),array('id'=>$_POST['user_id']));
                }
            }
        }
        
        public function gcm_clientes($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud->callback_before_insert(function($post){
                get_instance()->db->delete('gcm_clientes',array('clientes_id'=>$post['clientes_id'],'tipo'=>$post['tipo']));
                get_instance()->db->delete('gcm_clientes',array('valor'=>$post['valor']));
            });
            $crud = $crud->render();
            $this->loadView($crud);
      }
      
      function testPush(){
          $this->load->library('gcm_library');
          $pedido = $this->db->get_where('pedidos',array('id'=>1));
          $this->gcm_library->db = $this->db;
          echo $this->gcm_library->notificar($pedido->row(),'clientes');
      }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
