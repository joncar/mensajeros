<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
class Main extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            $this->load->helper('url');
            $this->load->helper('html');
            $this->load->helper('h');
            $this->load->database();
            $this->load->model('user');                    
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $this->load->library('traduccion');
            $this->load->model('querys');     
            $this->load->model('bdsource');
            $this->bdsource->init('ajustes',TRUE);
            ini_set('show_errors',1);
            ini_set('display_errors',1);
            if(!isset($_SESSION['convenio'])){
                $_SESSION['convenio'] = 0;
            }
            if(empty($_SESSION['lang'])){
                $_SESSION['lang'] = 'es';
            }
            
            //Payphone
            //Configuration
            require_once(APPPATH.'libraries/payphone/ConfigurationManager.php');
            require_once(APPPATH.'libraries/payphone/api/Transaction.php');
        }

        public function index($x = '')
        {
            $data = array(
                'view'=>'main',
                'tipo'=>'foods',
                'x'=>empty($x)?'2':''
            );
            $this->loadView($data);                
        }                

        public function success($msj)
        {
                return '<div class="alert alert-success">'.$msj.'</div>';
        }

        public function error($msj)
        {
                return '<div class="alert alert-danger">'.$msj.'</div>';
        }

        public function login()
        {
                if(!$this->user->log)
                {	
                        if(!empty($_POST['email']) && !empty($_POST['pass']))
                        {
                                $this->db->where('email',$this->input->post('email'));
                                $r = $this->db->get('user');
                                if($this->user->login($this->input->post('email',TRUE),$this->input->post('pass',TRUE)))
                                {
                                        if($r->num_rows()>0 && $r->row()->status==1)
                                        {
                                            if(!empty($_POST['remember']))$_SESSION['remember'] = 1;
                                            if(empty($_POST['redirect']))
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url('main').'"</script>');
                                            else
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
                                        }
                                        else $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                                }
                                else $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
                        }
                        else
                            $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

                        if(!empty($_SESSION['msj']))
                            header("Location:".base_url('registro/index/add'));
                }
                else header("Location:".base_url('registro/index/add'));
        }       

        public function unlog()
        {
                $this->user->unlog();                
                header("Location:".site_url());
        }

        public function loadView($param = array('view'=>'main'))
        {
            if(is_string($param)){
                $param = array('view'=>$param);
            }
            $view = $this->load->view('template',$param,TRUE);  
            if(!empty($_SESSION['lang'])){
            echo $this->traduccion->traducir($view,$_SESSION['lang']);
            }else{
                echo $view;
            }
            
        }
        
        public function traducir($idioma = 'ca'){
            $_SESSION['lang'] = $idioma;
            header("Location:".$_SERVER['HTTP_REFERER']);
        }

        public function loadViewAjax($view,$data = null)
        {
            $view = $this->valid_rules($view);
            $this->load->view($view,$data);
        }                

         function error404(){
            $this->loadView(array('view'=>'errors/403'));
        }    
        
        function contacto(){
            if(empty($_POST)){
                $page = new Bdsource('paginas');
                $page->where('titulo','contacto');
                $page->where('idioma',$_SESSION['lang']);
                $page->init();
            
                $this->loadView(array('view'=>'contacto','title'=>'Contactenos','p'=>$page));
            }else{
                $mensaje = '<h1>Hola, te han enviado un mensaje desde <a href="http://www.ensissciences.com">www.ensissciences.com</a></h1>';
                $mensaje.= '<h2>Datos</h2>';
                foreach($_POST as $n=>$p){
                    $mensaje.= '<p><b>'.ucfirst($n).'</b> = '.$p.'</p>';
                }
                correo($this->ajustes->correo,'Solicitud de contacto',$mensaje);
            }
        }
        
        function empresa(){
            $page = new Bdsource('paginas');
            $page->where('titulo','empresa');
            $page->where('idioma',$_SESSION['lang']);
            $page->init();
            $this->loadView(array('view'=>'empresa','title'=>'Empresa','p'=>$page));
        }
        
        function detail(){
            $this->loadView('detail');
        }
        
        function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
            foreach($usuario as $n=>$v){
                if(!is_array($v)){
                   $mensaje->texto = str_replace('{'.$n.'}',$v,$mensaje->texto);
                   $mensaje->titulo = str_replace('{'.$n.'}',$v,$mensaje->titulo);   
                }
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }
            correo('joncar.c@gmail.com',$mensaje->titulo,$mensaje->texto);
        }
        
        function appRepartidor()
        {            
            $this->loadView(array('view'=>'appRepartidor'));
        }
        
        function mapa(){
            $this->loadView('mapa');
        }
        
        function procesar_pago(){            
            $str = '';
            foreach($_POST as $n=>$p){
                $str.= '<p><b>'.$n.'</b> '.$p.'</p>';
            }
            correo('joncar.c@gmail.com','test',$str);
        }
        
        /*function pagar(){
            //Configuration
            require_once(APPPATH.'libraries/payphone/ConfigurationManager.php');
            require_once(APPPATH.'libraries/payphone/api/Transaction.php');
            $config = ConfigurationManager::Instance();
            $config->Token = "foSn8RVD0QHY0PRDKHPhcqmqkUwK3gp75CrGtbkm67OPr0e7Qvbaa8h8l7KQskIEcPc2h9IX-3hHi4Ha10El_btJZnVxc83s5Xcplivlksz2LQh-Gb88R-L_-A03tswzJBDDtxLh5gZ93ZZSab6dWdb4aZ8VFiEBeis4BdwlcMX5kLiVCRRtQCG61vZVxtuP0Iv0X4o5smLxTKVVG04cUasCzRy0Md01qG5Nv7cmRRPm8QJaJsnRYNMiMc7JeXm0eSMYJ2QRwwJTyATIwhcwDTXXOf6oh-4g1UgIYyoRWk7gHi3-Ok6DqgVdydaBqS2Hbf0208ZHL0S0QRy5HVCWdxintLb0M1gxSL5n6j9RoeCzchsXW95Kj-NZ8hq_F5AM0tJKnpIFnIvRcCGxxKMfcBWwpVf1D7qTWQE4oB1_G4yyHLtTDL8dVX0ErU_wtIcmwrUms0GwmOG2Jm5lIwxPBw";
            $config->ApplicationId = "77955046-3d4a-42fe-8fa1-5ba8fc4ede8c";
            $config->ApplicationPublicKey = "<RSAKeyValue><Modulus>qFN+Lsczzed6GfToqSkb9cFX78bPbWpkTQ870RiSm/xOtqlJN54LoVH6pgSZQlZ3AmLH9xlENkcidahoeK4LQ92lcgzp5h35X3iuwiB7CXEyRup/zGtgPXvpHUPgfXAL7LLUHtbfW06hqWUZsxSGFCjuaDuJd/FWT88uXdRLwMU=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
            $config->PrivateKey = "<RSAKeyValue><Modulus>rEhSfq3PkBoJLjtDpbGwcAUX50Ska5fy8q/RJiVmOj0T/KvByX8v2WWJd04VmsicW4QuAB+Lx5mJNqN7BmcVZ73rSTwEkhy3SKRgV5Usu0ZOUtlz4MelZI3z7CoGLMwOH0XbHxTdHSTwyzKZiDmRktkkNt8tGx8e3NgnUAn0G50=</Modulus><Exponent>AQAB</Exponent><P>7JuLAVo+5DjxM4bgj6ZzuxlXJJ4w3yxZyj8Q9xwXRHpBp7YSpC8NXNAH8pXfE8joJCzHUzweu4bO0khppEJyHQ==</P><Q>umcfSYA5U+KZQQvZjLY/YMDpvnqRhFH9hTIzn5DRg84TXxbt8klD50IuMloHBn6Z/5csGdGHdAHLwV6yx5gXgQ==</Q><DP>ltSpDlmrUe2CxWgr6ycfC5yh0rQNT5eEPctqUzzTEFInXHRS+dsM16e+CUTFCmW+pqDtCACBTuYnHiIPRikdeQ==</DP><DQ>TPWak0wfXyTlRVfRICl2jUnYt83/GnSHiWCqs6yk9Bg3I0FiSHA7WtWWIS/OSr4mcsJFcPtzQ1AigdnJUmLXAQ==</DQ><InverseQ>E1/Yjt9vdROLK+fWMN1z3i/Be4i/v4lGVghvA71PcUancVa+GC6Ly0GucVLQ+81Uv/Q9ZLraZPj/Do85jZNUbA==</InverseQ><D>Bcu6EL5uqumhUO+X90FKzv3w36wgvujh+LWe+uJLtd5bp4assodnl+/xR7T2B5rDndWMyZ7GEE0zP0YRK7Y4k1JmeYBh+vSEo8mOaIOJthidhoQr7RIP+UNW2GXd3SqmUwc7hZ2ZbP1v54JJ6GUWlAjSB/rVIZU00B5nVLU3JYE=</D></RSAKeyValue>";
            $config->ClientId = "a738bd6d-923b-4c36-bd58-cbb050c91331";
            $config->ClientSecret = '1b5595ad-d81b-4610-9409-dc2ebdfcb8e8';
            
            $instance = new Transaction();

            try {
                $data = new TransactionRequestModel();
                $data->Amount = 500;
                $data->AmountWithTax = 440;
                $data->Tax = 60;
                $data->AmountWithOutTax = 0;
                $data->Latitud = "-2.123123321";
                $data->Longitud = "-78.123123321";
                $data->PurchaseLanguage = "es";
                $data->TimeZone = -5;
                $data->Token = $config->Token;
                $data->ClientTransactionId = "acwrr-adl933";

                $encrypt = new PayPhoneEncrypt("<RSAKeyValue><Modulus>qFN+Lsczzed6GfToqSkb9cFX78bPbWpkTQ870RiSm/xOtqlJN54LoVH6pgSZQlZ3AmLH9xlENkcidahoeK4LQ92lcgzp5h35X3iuwiB7CXEyRup/zGtgPXvpHUPgfXAL7LLUHtbfW06hqWUZsxSGFCjuaDuJd/FWT88uXdRLwMU=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>");

                $decryptedData = $encrypt->Execute($data, "77955046-3d4a-42fe-8fa1-5ba8fc4ede8c", "https://www.mensajeros.ec/main/procesar_pago");
                
                echo '
                    <form method="POST" action="https://sandboxpayment.payphone.com.ec/Payment/Pay">

                        <input type="hidden" name="SessionKey" value="'.$decryptedData->SessionKey.'" />

                        <input type="hidden" name="XmlReq"  value="'.$decryptedData->XmlReq.'" />

                        <input type="hidden" name="IV" value="'.$decryptedData->IV.'" />

                        <input type="hidden" name="ApplicationId" value="'.$decryptedData->ApplicationId.'" />

                        <input type="hidden" name="CallBackUrl" value="'.$decryptedData->CallBackUrl.'" />

                        <input type="submit" nameclass="btn btn-primary" />
                        </form>
                ';
                
            } catch (PayPhoneWebException $exc) {

               header('HTTP/1.1 '.$exc->StatusCode.' Error');

               echo json_encode($exc->ErrorList);

            }
        }*/
        
        function test(){
            correo('joncar.c@gmail.com','TEST','TEST');
        }
}
