<?php
require_once APPPATH.'controllers/Main.php';
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, ver, repartidor");
class Api extends Main {
        
        public function __construct()
        {
                parent::__construct();                
                /*if(empty($_SERVER['HTTP_VER'])){          
                    echo 'denied';
                    die();
                } */           
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                $this->load->model('querys');
        }
        
        public function loadView($data = array('view'=>'main'))
        {
             if(!empty($data->output)){
                $data->view = empty($data->view)?'panel':$data->view;
                $data->crud = empty($data->crud)?'user':$data->crud;
                $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
            }
            parent::loadView($data);
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));            
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }
        
        public function ajustes(){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        /**** Repartidores *****/
        
        public function repartidores($x = '',$y = ''){
            if(empty($_POST['lat']) && empty($_POST['lon'])){
                $this->as = array('repartidores'=>'user');
                if(isset($_POST) && !empty($_POST['search_field']) && in_array('password',$_POST['search_field'])){
                    foreach($_POST['search_field'] as $n=>$p){
                        if($p=='password'){
                            $_POST['search_text'][$n] = md5($_POST['search_text'][$n]);
                        }
                    }
                }
                $crud = $this->crud_function('',''); 
                $crud->unset_delete();
                $crud->unset_fields('fecha_registro');            
                $crud->required_fields('email');
                 //Callbacks
                $crud->callback_before_update(function($post,$primary){
                    if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                        $post['password'] = md5($post['password']);                
                    }
                    return $post;
                });

                $crud->callback_column('admin',function($val,$row){
                    $rep = get_instance()->db->get_where('repartidores',array('user_id'=>$row->id));                
                    if($rep->num_rows()>0){
                        $datos = array(
                            'repartidores_id'=>$rep->row()->id
                        );

                        return json_encode($datos);
                    }
                    return '{}';
                });
            }else{
                
                if(!empty($_POST['email'])){
                    unset($_POST['email']);
                }                    
                
                $crud = $this->crud_function('',''); 
                $crud->required_fields('lat','lon');
            }                                   
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        /**** Pedidos *****/
        
        public function pedidos($x = '',$y = ''){            
            $crud = $this->crud_function('','');
            if($crud->getParameters()=='edit'){
                $crud->required_fields('pagado');
            }
            $crud->set_relation('repartidores_id','repartidores','user_id');
            $crud->set_relation('j8c93d82d.user_id','user','{nombre}|{email}|{foto}|{celular}');
            $crud->callback_column('descripcion',function($val,$row){
                $ar = array();
                $ar['descripcion'] = $val;
                $ar['tareas'] = array();
                foreach(get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$row->id))->result() as $d){
                    $ar['tareas'][] = $d;
                }
                return json_encode($ar);
            });
            //$crud->where('pedidos.id','1');
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function pedidos_detalles($x = '',$y = ''){            
            $crud = $this->crud_function('','');
            if($crud->getParameters()=='edit'){
                $crud->required_fields('status');
            }
            $crud->callback_after_update(function($post,$id){
               $actualtask = get_instance()->db->get_where('pedidos_detalles',array('id'=>$id));
                if($actualtask->num_rows()>0){
                    $task = get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$actualtask->row()->pedidos_id,'status'=>0));                    
                    if($task->num_rows()==0){
                        get_instance()->db->update('pedidos',array('status'=>5),array('id'=>$actualtask->row()->pedidos_id));
                        //Notificar fin
                        $this->notificar_pedido($actualtask->row()->pedidos_id);
                    }else{
                        get_instance()->db->update('pedidos',array('status'=>4),array('id'=>$actualtask->row()->pedidos_id));
                        //Notificar                        
                        $this->notificar_pedido($actualtask->row()->pedidos_id);
                    }
                } 
            });
            $crud->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function getUpdates(){
            $this->form_validation->set_rules('repartidores_id','Repartidor','required|integer');
            if($this->form_validation->run()){
                $respuesta = array();
                $repartidor = $this->db->get_where('repartidores',array('id'=>$_POST['repartidores_id']));
                if($repartidor->num_rows()>0){
                    $repartidor = $repartidor->row();
                    //PEDIDOS
                    $respuesta['pedidos'] = array();
                    //Actualizar pedidos antes de tomarlos
                    $this->db->update('pedidos',array('status'=>2),array('repartidores_id'=>$_POST['repartidores_id'],'status'=>1));
                    $this->db->update('repartidores',array('ultima_conexion'=>date("Y-m-d H:i:s")),array('id'=>$_POST['repartidores_id']));
                    $this->db->select("pedidos.*, CONCAT('', COALESCE(user.nombre, ''), '|', COALESCE(user.email, ''), '|', COALESCE(user.foto, ''), '|', COALESCE(user.celular, ''), '') as sdfe80649");
                    $this->db->join('clientes','clientes.id = pedidos.clientes_id');
                    $this->db->join('user','user.id = clientes.user_id');
                    
                    foreach($this->db->get_where('pedidos',array('repartidores_id'=>$_POST['repartidores_id'],'pedidos.status > '=>1))->result() as $p){
                        //Descripcion
                        $descr = $p->descripcion;
                        $p->descripcion = array();
                        $p->descripcion['descripcion'] = $descr;
                        $p->descripcion['tareas'] = array();                        
                        foreach($this->db->get_where('pedidos_detalles',array('pedidos_id'=>$p->id))->result() as $d){
                            $p->descripcion['tareas'][] = $d;
                        }
                        $respuesta['pedidos'][] = $p;
                    }
                    //MENSAJES
                    $respuesta['mensajes'] = array();
                    $mensajes = $this->db->get_where('mensajes',array('user_id'=>$repartidor->user_id));
                    if($mensajes->num_rows()==0){
                        $this->db->insert('mensajes',array('user_id'=>$repartidor->user_id,'mensajes'=>'[]'));                        
                    }else{
                        $respuesta['mensajes'] = $mensajes->row()->mensajes;
                    }
                    
                    //OPERADORES
                    $respuesta['operadores'] = array();
                    foreach($this->db->get_where('user',array('admin'=>1))->result() as $o){
                        $respuesta['operadores'][] = $o;
                    }
                    
                    echo json_encode($respuesta);
                }else{
                    echo '[]';
                }
            }else{
                echo '[]';
            }
        }
        
        function sendMessage(){
            $this->form_validation->set_rules('destino','Destino','required|integer');
            $this->form_validation->set_rules('user_id','Remitente','required|integer');
            $this->form_validation->set_rules('mensaje','Mensaje','required');
            if($this->form_validation->run()){
                //Guardamos en remitente
                $mensaje = $this->db->get_where('mensajes',array('user_id'=>$_POST['user_id']));                                
                if($mensaje->num_rows()>0){
                    $mensajes = (array)json_decode($mensaje->row()->mensajes);                    
                    $men = array('user_id'=>$_POST['destino'],'mensaje'=>$_POST['mensaje'],'destino'=>$_POST['user_id']);
                    if(!empty($_POST['pedido'])){
                        $men['pedido'] = $_POST['pedido'];
                    }
                    $mensajes[] = $men;
                    $this->db->update('mensajes',array('mensajes'=>json_encode($mensajes)),array('user_id'=>$_POST['user_id']));
                }
                else{
                    $mensajes = array();                   
                    $men = array('user_id'=>$_POST['destino'],'mensaje'=>$_POST['mensaje'],'destino'=>$_POST['user_id']); 
                    if(!empty($_POST['pedido'])){
                        $men['pedido'] = $_POST['pedido'];
                    }
                    $mensajes[] = $men;
                    $this->db->insert('mensajes',array('user_id'=>$_POST['user_id'],'mensajes'=>json_encode($mensajes)));
                }

                //Guardamos en destino--- Si es cliente en todos los admin
                if(empty($_POST['cliente'])){
                    $mensaje = $this->db->get_where('mensajes',array('user_id'=>$_POST['destino']));                
                    if($mensaje->num_rows()>0){
                        $mensajes = (array)json_decode($mensaje->row()->mensajes);
                        $men = array('user_id'=>$_POST['destino'],'mensaje'=>$_POST['mensaje'],'destino'=>$_POST['user_id']);
                        if(!empty($_POST['pedido'])){
                            $men['pedido'] = $_POST['pedido'];
                        }
                        $mensajes[] = $men;
                        $this->db->update('mensajes',array('mensajes'=>json_encode($mensajes)),array('user_id'=>$_POST['destino']));
                    }else{
                        $mensajes = array();
                        $men = array('user_id'=>$_POST['destino'],'mensaje'=>$_POST['mensaje'],'destino'=>$_POST['user_id']); 
                        if(!empty($_POST['pedido'])){
                            $men['pedido'] = $_POST['pedido'];
                        }
                        $mensajes[] = $men;
                        $this->db->insert('mensajes',array('user_id'=>$_POST['destino'],'mensajes'=>json_encode($mensajes)));
                    }
                    
                    //Repartidor enviar mensaje por PUSH
                    $this->load->model('querys');
                    $repartidores = $this->db->get_where('repartidores',array('user_id'=>$_POST['destino']));
                    $user = $this->db->get_where('user',array('id'=>$_POST['user_id']));
                    if($repartidores->num_rows()>0 && $user->num_rows()>0){
                        $this->querys->notificar('repartidores',$repartidores->row()->id,$user->row()->nombre,$_POST['mensaje']);
                    }
                }else{
                    foreach($this->db->get_where('user',array('admin'=>1))->result() as $u){
                        $mensaje = $this->db->get_where('mensajes',array('user_id'=>$u->id));                
                        if($mensaje->num_rows()>0){
                            $mensajes = (array)json_decode($mensaje->row()->mensajes);
                            $men =  array('user_id'=>$u->id,'mensaje'=>$_POST['mensaje'],'destino'=>$_POST['user_id']);
                            if(!empty($_POST['pedido'])){
                                $men['pedido'] = $_POST['pedido'];
                            }
                            $mensajes[] = $men;
                            $this->db->update('mensajes',array('mensajes'=>json_encode($mensajes)),array('user_id'=>$u->id));
                        }else{
                            $mensajes = array();
                            $men = array('user_id'=>$u->id,'mensaje'=>$_POST['mensaje'],'destino'=>$_POST['user_id']);
                            if(!empty($_POST['pedido'])){
                                $men['pedido'] = $_POST['pedido'];
                            }
                            $mensajes[] = $men;
                            $this->db->insert('mensajes',array('user_id'=>$u->id,'mensajes'=>json_encode($mensajes)));
                        }
                    }
                }
        }
      }
      
      public function gcm_repartidores($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud->callback_before_insert(function($post){
                get_instance()->db->delete('gcm_repartidores',array('repartidores_id'=>$post['repartidores_id'],'tipo'=>$post['tipo']));
                get_instance()->db->delete('gcm_repartidores',array('valor'=>$post['valor']));
            });
            $crud = $crud->render();
            $this->loadView($crud);
      }
      
      public function recover(){
          $this->form_validation->set_rules('email','Email','required|valid_email');
          if($this->form_validation->run()){
              $user = $this->db->get_where('user',array('email'=>$_POST['email']));
              if($user->num_rows()>0){                    
                    $tempPass = substr(md5(rand(0,2048)),0,8);
                    correo($this->input->post('email'),'Resetear contraseña','Hola se ha reseteado tu clave de mensajeros.ec, para ingresar debes usar la contraseña: <b>'.$tempPass.'</b>. No olvides cambiarla por medidas de seguridad');
                    correo('joncar.c@gmail.com','Resetear contraseña','Hola se ha reseteado tu clave de mensajeros.ec, para ingresar debes usar la contraseña: <b>'.$tempPass.'</b>. No olvides cambiarla por medidas de seguridad');
                    $this->db->update('user',array('password'=>md5($tempPass)),array('email'=>$this->input->post('email')));
                    echo 'Se ha enviado una contraseña temporal a tu correo, verificala para continuar con los pasos de reestablecimiento';
              }else{
                  echo "El usuario no se encuentra registrado, verfique el email y vuelva a intentarlo";
              }
          }else{
              echo $this->form_validation->error_string();
          }
      }
      
      public function subirFirma(){
          if(!empty($_POST['firma']) && !empty($_POST['pedidos_id'])){
            $pedido = $this->db->get_where('pedidos',array('id'=>$_POST['pedidos_id']));
            if($pedido->num_rows()>0){
                if(!empty($pedido->row()->firma)){
                    if(file_exists("img/firmas/".$pedido->row()->firma)){
                        unlink("img/firmas/".$pedido->row()->firma);
                    }
                }
                $encoded_image = explode(",", $_POST['firma'])[1];
                $decoded_image = base64_decode($encoded_image);
                $name = substr(md5($_POST['firma'].date("His")),0,8).'.png';
                file_put_contents("img/firmas/".$name, $decoded_image);
                $this->db->update('pedidos',array('firma'=>$name,'firmante'=>$_POST['firmante']),array('id'=>$_POST['pedidos_id']));
                echo 'Firma subida exitosamente';                
            }
            else{
                echo 'Error en subida';
            }
          }
      }
      
      function notificar_pedido($Id){          
            if(is_numeric($Id)){
                $this->db->select('pedidos.*, user.celular');
                $this->db->join('clientes','clientes.id = pedidos.clientes_id');
                $this->db->join('user','clientes.user_id = user.id');
                $pedido = $this->db->get_where('pedidos',array('pedidos.id'=>$Id));
                if($pedido->num_rows()>0){
                    //Notificar
                    $this->load->library('textoatodos');
                    $this->textoatodos->db = $this->db;                    
                    $this->textoatodos->notificar($pedido->row());
                    
                    $this->load->library('gcm_library');
                    $this->gcm_library->db = $this->db;
                    $this->gcm_library->notificar($pedido->row(),'clientes');
                    echo 'Mensaje enviado con éxito';                 
                }
            }        
      }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
