<section class="clear:both" id="home-section-1" style="background: #f2f2f2 none repeat scroll 0 0;">
    <div class="container"><!-- container via hooks -->	
        <div id="page-content-container">	
            <div class="row-fluid">
                <div class="col-xs-12 col-sm-4 col-sm-offset-4" style="background:white; padding:20px; box-shadow: 2px 2px 6px #f1f1f1 !important;">
                    <div class="form-container">
                        <form action="<?= base_url('registro/forget') ?>" method="post" onsubmit="return validar(this)" role="form" class="form-horizontal">
                            <?= !empty($msj)?$msj:'' ?>
                            <input type="email" name="email" id="email" data-val="required" class="form-control" value="<?= $_SESSION['email'] ?>" readonly><br/>
                            <input type="password" class="form-control" name="pass" id="pass" placeholder="Nuevo Password"><br/>
                            <input type="password" class="form-control" name="pass2" id="pass2" placeholder="Repetir Password"><br/>
                            <input type="hidden" name="key" value="<?= $key ?>">
                            <button type="submit" class="btn btn-success">Recuperar contraseña</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>