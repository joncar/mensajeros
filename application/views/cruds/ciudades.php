<?php echo $output ?>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBAmpz-tft5eyIVdHRZrXcEhj4ykyaC80E&signed_in=true&v=3.exp&sensor=true&libraries=drawing"></script>
<script>    
    var color;
    var drawingManager;
    var limit1;
    var limit2; 
    var map;
    $(document).on('ready',function(){
        var str = '<div id="ciudades_nombre_field_box" class="form-group">';
            str+= '<label id="ciudades_nombre_display_as_box" for="field-ciudades_nombre">';
            str+= 'Regiones<span class="required">*</span>:';
            str+= '</label>';
            str+= '<div class="row"><div class="col-xs-6"><div style="width:100%; height:500px;" id="map">MAPA</div></div><div class="col-xs-6"><p>Elige el tipo de limitación a dibujar</p><p>Limitación 1: <button type="button" class="btn btn-success" id="limit1">Elegir</button> <span id="ruta1">0,0</span></p><p>Limitación 2: <button type="button" class="btn btn-danger" id="limit2">Elegir</button> <span id="ruta2">0,0</span></p><p>Marcar Centro y Zoom <button id="markcenterzoom" type="button" class="btn btn-success">Marcar</button></p></div><button id="markcenterzoom2" type="button" class="btn btn-success">Marcar Zoom 2</button></p></div></div>';
        $("#ciudades_nombre_field_box").append(str);
        init_map();
        
        $("#limit1").click(function(){           
           color = "black";
           var polylineOptions = drawingManager.get('polygonOptions');
           polylineOptions.strokeColor = color;
           drawingManager.setDrawingMode('polygon');           
        });
        $("#limit2").click(function(){            
            color = "red";
            var polylineOptions = drawingManager.get('polygonOptions');
            polylineOptions.strokeColor = color;
            drawingManager.setDrawingMode('polygon');
            
        });
        
        $("#markcenterzoom").click(function(){
            $("#field-centro").val('('+map.center.lat()+','+map.center.lng()+')');
            $("#field-zoom").val(map.zoom);
        });
        
        $("#markcenterzoom2").click(function(){
            $("#field-zoom2").val(map.zoom);
        });
    });
    
    function init_map(){
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 11,
            center: {lat: -0.1862504, lng: -78.5709685}
        });
        
        drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.MARKER,
        drawingControl: true,
        drawingControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER,
          drawingModes: [
            google.maps.drawing.OverlayType.POLYGON            
          ]
        },
        polygonOptions: {
            strokeWeight: 0,
            fillOpacity: 0.45,
            editable: true
        }
      });
      drawingManager.setMap(map);      
      google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {            
            drawingManager.setDrawingMode(null);
            var newShape = e.overlay;
            newShape.type = e.type;
            newShape.id = 1;
            newShape.color = color;
            if(color==="black"){
                limit1 !== undefined?limit1.setMap(null):undefined;
                limit1 = newShape;
            }else{
                limit2 !== undefined?limit2.setMap(null):undefined;
                limit2 = newShape;
            }
            
            newShape.set('fillColor', color);
            addNewPolys(newShape);
            updateRuta();
            console.log(limit1);            
        });
        
        limite1 = $("#field-json_limitacion").val();
        limite2 = $("#field-json_limitacion2").val();
        if(limite1!=='' && limite2!==''){
            limite1 = JSON.parse(limite1);
            limite2 = JSON.parse(limite2);
            var ar = [];
            for(var i in limite1){
                ar.push(new google.maps.LatLng(limite1[i].lat, limite1[i].lng));
            }
            
            newPolys = new google.maps.Polygon({
                path: ar,
                strokeWeight: 0,
                fillColor: 'black',
                editable: true
              });
            newPolys.type = 'polygon';
            limit1 = newPolys;
            ar = [];
            for(var i in limite2){
                ar.push(new google.maps.LatLng(limite2[i].lat, limite2[i].lng));
            }
            newPolys = new google.maps.Polygon({
                path: ar,
                strokeWeight: 0,
                fillColor: 'red',
                editable: true
              });
            newPolys.type = 'polygon';
            limit2 = newPolys;
            
            limit1.setMap(map);          
            addNewPolys(limit1);
            
            limit2.setMap(map);          
            addNewPolys(limit2);
        }
    }
    
    function addNewPolys(newPoly) {
        google.maps.event.addListener(newPoly, 'click', function() {
             google.maps.event.addListener(newPoly.getPath(), 'set_at', function() {
                 updateRuta();
            });

            google.maps.event.addListener(newPoly.getPath(), 'insert_at', function() {
                 updateRuta();
            });
            setSelection(newPoly);                
        });
    }
    
    function updateRuta(){
        if(limit1!==undefined){
            $("#field-json_limitacion").val(JSON.stringify(limit1.getPath().getArray()));
            $("#ruta1").html('Puntos elegidos: '+limit1.getPath().getArray().length);
        }
        
        if(limit2!==undefined){
            $("#field-json_limitacion2").val(JSON.stringify(limit2.getPath().getArray()));
            $("#ruta2").html('Puntos elegidos: '+limit2.getPath().getArray().length);
        }
    }
</script>