<?php if(empty($output2)): ?>
<?= $output ?>
<?php else: ?>
    <div>

      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Activos</a></li>
        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Terminados</a></li>
        <li role="presentation"><a href="#tramite" aria-controls="tramite" role="tab" data-toggle="tab">Cancelados</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="home">
              <?= $output2 ?>
          </div>
          <div role="tabpanel" class="tab-pane" id="profile">
              <?= $output2 ?>
          </div>
          <div role="tabpanel" class="tab-pane" id="tramite">
              <?= $output3 ?>
          </div>
      </div>

    </div>
<?php endif ?>
<script>
    $(document).ready(function(){
        $("#tramite .filtering_form").attr('action','<?= base_url('pedidos/admin/'.$this->router->fetch_method().'/ajax_list') ?>?tipo=-1');
        $("#home .filtering_form").attr('action','<?= base_url('pedidos/admin/'.$this->router->fetch_method().'/ajax_list') ?>?tipo=1');
        $("#profile .filtering_form").attr('action','<?= base_url('pedidos/admin/'.$this->router->fetch_method().'/ajax_list') ?>?tipo=5');
        
        $("#tramite .filtering_form").attr('data-ajax-list-info-url','<?= base_url('pedidos/admin/'.$this->router->fetch_method().'/ajax_list_info') ?>?tipo=-1');
        $("#home .filtering_form").attr('data-ajax-list-info-url','<?= base_url('pedidos/admin/'.$this->router->fetch_method().'/ajax_list_info') ?>?tipo=1');
        $("#profile .filtering_form").attr('data-ajax-list-info-url','<?= base_url('pedidos/admin/'.$this->router->fetch_method().'/ajax_list_info') ?>?tipo=5');
        
    });
    var reloj = undefined;
    function update(){
        $(".filtering_form").submit();        
    }
    
    window.onRefreshFlexigrid = function(){
        clearTimeout(reloj);
        reloj = setTimeout(update,10000);
    };
</script>