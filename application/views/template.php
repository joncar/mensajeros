<!DOCTYPE html>
<html lang="es-ES">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <title><?= empty($title) ? 'Mensajeros' : $title ?></title>        
        <link rel="shortcut icon" href="<?= base_url('img/favicon.ico') ?>" type="image/x-icon" />        
        <link rel="shortcut icon" href="public/img/favicon.ico" sizes="256x256"/>        
        <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Lato:400,700,300'>
        <link href='//api.tiles.mapbox.com/mapbox-gl-js/v0.19.1/mapbox-gl.css' rel='stylesheet'/>        
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">        
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">                
        <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Lato:400,700,300'>
        <link rel="stylesheet" type="text/css" href="<?= base_url('css/chat.css') ?>"/>        
        <link rel="stylesheet" type="text/css" href="<?= base_url('css/template/style.css') ?>"/>        
        <link rel="stylesheet" type="text/css" href="<?= base_url('css/formulario_solicitud.css') ?>"/>        
        <script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="<?= base_url() . 'js/frame.js' ?>"></script>        
        <?php if(!empty($scripts)): ?>
            <?= $scripts ?>
        <?php else: ?>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <?php endif ?>
        <script>
            var base_url = '<?= base_url() ?>';
        </script>
        <!--Add the following script at the bottom of the web page (before </body></html>)-->
        <script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=41601473"></script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-75733150-3', 'auto');
            ga('send', 'pageview');

          </script>
          <script>
            function redire(){}
            if (navigator.appVersion.indexOf('iPhone') > -1) {
                $(document).on('ready',function(){$("#mobilemessageIOS").show();});
                function redire(){                    
                           window.location = "mensajerosec://main"; //fallback url
                           setTimeout(function(){
                                window.location = "https://itunes.apple.com/us/app/mensajeros-ec/id1179900254?l=es&mt=8"; //fallback url
                           },3000);
                }
              }
              else if (navigator.userAgent.indexOf('Android') > -1) {
                  $(document).on('ready',function(){$("#mobilemessageAndroid").show();});
              }   
        </script>
    </head>
    <body>  
        <div id="mobilemessageAndroid" style="display:none; position:fixed; top:0px; width:100%; z-index:100000;" class="alert alert-info" align="center">
            <a href="intent://scan/#Intent;scheme=mensajerosec;package=com.mensajeros.clientes;end">
            <i class="fa fa-mobile"></i> Usar la aplicación para solicitar un servicio</a>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        
        <div id="mobilemessageIOS" style="display:none; position:fixed; top:0px; width:100%; z-index:100000;" class="alert alert-info" align="center">
            <a href="javascript:redire()">
            <i class="fa fa-mobile"></i> Usar la aplicación para solicitar un servicio</a>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php $this->load->view('includes/template/header'); ?>
        <div>
            <?php $this->load->view($view); ?>   
        </div>
        <?php $this->load->view('includes/template/footer'); ?>
    </body>
</html>