<footer class="clearfix ng-scope">
    <div class="footer-wrapper">
        <div class="row">
            <div class="col-sm-4">
                    <ul class="social-media-buttons">
                        <li>
                            <a rel="nofollow" target="_blank" href="https://www.linkedin.com/company/mensajeros-urbanos">
                                <img alt="Encuéntranos en LinkedIN" src="<?= base_url('img').'/' ?>03-LinkedIn.png">
                            </a>
                        </li>
                        <li>
                            <a rel="nofollow" target="_blank" href="http://twitter.com/mensajerosurban">
                                <img alt="Encuéntranos en Twitter" src="<?= base_url('img').'/' ?>02-Twitter.png">
                            </a>
                        </li>
                        <li>
                            <a rel="nofollow" target="_blank" href="http://facebook.com/MensajerosUrbanos">
                                <img alt="Encuéntranos en Facebook" src="<?= base_url('img').'/' ?>01-Facebook.png">
                            </a>
                        </li>
                    </ul>
                    <div style="text-align:left;">
                        <img alt="Descarga nuestra aplicación" src="<?= base_url('img/').'/' ?>play.png">
                    </div>
                    <div style="text-align:left;">
                        <img alt="Descarga nuestra aplicación" src="<?= base_url('img/').'/' ?>itunes.png">
                    </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="<?= site_url() ?>"><p>Inicio</p></a>                        
                        <a href="<?= site_url('p/como-funciona') ?>"><p>¿Cómo funciona?</p></a>
                        <a href="<?= site_url('p/precios') ?>"><p>Precios</p></a>
                        <a href="<?= site_url('p/empresas') ?>"><p>Para empresas</p></a>
                        <a href="<?= site_url('solicitar-servicio') ?>"><p>Solicitar Servicio</p></a>
                        <a href="<?= site_url('panel') ?>"><p>Iniciar Sesión</p></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="color:white; line-height: 40px;">
                <div><i class="fa fa-map-marker" style="color:#5BC0DE"></i> Dirección</div>
                <div><i class="fa fa-phone" style="color:#5BC0DE"></i> Teléfono</div>
                <div><i class="fa fa-envelope" style="color:#5BC0DE"></i> Email</div>
                <div><i class="fa fa-clock-o" style="color:#5BC0DE"></i> Email</div>
            </div>
            
        </div>
    </div>
</footer>