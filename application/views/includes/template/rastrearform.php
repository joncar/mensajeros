<div class="col-xs-12 col-sm-4 col-sm-offset-4">
<form id="rastrear"  onsubmit="return rastrear()">
    <div class="form-group">
      <input id="exampleInputName2" class="form-control" type="text" style="display: inline-block; width: 74%;" name="id" placeholder="#Rastreo">
      <button type="submit" class="btn btn-info btn-round">Rastrear</button>
    </div>        
</form>
<script>
    function rastrear(){
        var data = document.getElementById('rastrear');
        var datos = new FormData(data);        
        $.ajax({
            url:'<?= base_url('pedidos/frontend/consultarRastreo') ?>',
            data:datos,
            type:'post',
            processData:false,
            cache:false,
            contentType: false,
            success:function(data){
                data = JSON.parse(data);
                if(data.length>0){
                    document.location.href="<?= base_url('pedidos/frontend/rastrear') ?>/"+data[0].id;
                }else{
                    emergente('Lo sentimos pero no hemos podido encontrar su paquete, por favor verfique la información suministrada')
                }
            }
        });                    
        return false;
    }
</script>
</div>