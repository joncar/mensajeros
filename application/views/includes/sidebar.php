<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(
                        'admin'=>array('admin/ajustes'),
                        'paginas'=>array('admin/paginas','admin/banner'),
                        'maestros'=>array('admin/ciudades','admin/tipos_servicios','admin/bancos'),
                        'clientes'=>array('admin/emails','admin/clientes','admin/solicitudes_empresas','admin/sms'),
                        'repartidores'=>array('admin/mensajeros','admin/solicitudes_repartidores','admin/repartidores_cuenta'),
                        'notificaciones'=>array('admin/notificaciones'),
                        'reportes'=>array('admin/reportmaker'),
                        'pedidos'=>array('admin/solicitudes','admin/estandar','admin/express','admin/tramite','admin/pagos','admin/cupones'),
                        'seguridad'=>array('grupos','funciones','user')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'paginas'=>array('CMS','fa fa-book'),
                        'maestros'=>array('Maestros','fa fa-table'),
                        'repartidores'=>array('Mensajeros','fa fa-motorcycle'),
                        'clientes'=>array('Clientes','fa fa-users'),
                        'seguridad'=>array('Seguridad','fa fa-user-secret'),
                        'solicitudes_empresas'=>array('Solicitudes'),
                        'solicitudes_repartidores'=>array('Solicitudes'),
                        'pedidos'=>array('Servicios','fa fa-cubes'),
                        'solicitudes'=>array('Agregar solicitud','fa fa-cubes')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
