<div class="form-container">
    <?php if(empty($_SESSION['user'])): ?>
    <?php if(!empty($msj))echo $msj ?>
    <?php if(!empty($_SESSION['msj']))echo $_SESSION['msj'] ?>
    <h3 align="left" class="login-register-label">Iniciar Sesión</h3>
    <form role="form" action="<?= base_url('main/login') ?>" onsubmit="return validar(this)" method="post">   
       <?= input('email','Email','email') ?>
       <?= input('pass','Contraseña','password') ?>       
       <input type="hidden" name="redirect" value="<?= empty($_GET['redirect'])?base_url('panel'):$_GET['redirect'] ?>">
       <div align="center"><button type="submit" class="btn btn-info">Entrar</button>
       <!--<a class="btn btn-link" href="<?= base_url('registro/index/add') ?>">Registrate</a><br/>-->
       <a class="btn btn-link" href="<?= base_url('registro/forget') ?>">¿Olvidastes tu contraseña?</a>
       </div>
    </form>
    <?php else: ?>
    <div align="center"><a href="<?= base_url('panel') ?>" class="btn btn-info btn-large">Entrar al sistema</a></div>
    <?php endif; ?>
    <?php $_SESSION['msj'] = null ?>
</div>