<section class="clear:both" id="home-section-1" style="background: #f2f2f2 none repeat scroll 0 0;">
    <div class="container"><!-- container via hooks -->	
        <div id="page-content-container">	
            <div class="row-fluid">
                <div class="col-xs-12 col-sm-4 col-sm-offset-1" style="background:white; padding:20px; box-shadow: 2px 2px 6px #f1f1f1 !important;">
                    <div class="form-container">
                        <?= $this->load->view('predesign/login') ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-offset-1">
                    <?= $output ?>
                </div>
            </div>
        </div>
    </div>
</section>