<?php 
    require_once APPPATH.'controllers/Panel.php';
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function verReportes($id = ''){
            $permited = array('PDF','HTML','EXCEL');
            if(count($this->uri->segments)>5){
                $vars = $this->uri->segments;
                $_POST = array('docType'=>$vars[5]);
                for($i=6;$i<count($this->uri->segments);$i+=2){
                    $_POST[$vars[$i]] = $vars[$i+1];
                }
            }
            $reporte = $this->db->get_where('reportes',array('id'=>$id));
            if($reporte->num_rows()>0){
                $reporte = $reporte->row();
                //$this->mostrarReporte($reporte,array('proveedores_id'=>'2'));
                if(!empty($reporte->variables) && empty($_POST)){
                    //Mostrar form
                    $this->mostrarForm($reporte);
                }
                elseif(empty($reporte->variables) || (!empty($reporte->variables) && !empty($_POST))){
                    //Convertir fechas
                    if(!empty($_POST['desde'])){
                        $_POST['desde'] = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['desde'])));
                    }
                    if(!empty($_POST['hasta'])){
                        $_POST['hasta'] = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['hasta'])));
                    }
                    $this->mostrarReporte($reporte,$_POST);
                }
            }
        }
        
        function reemplazarFunciones($reporte,$consultas){
            //En letras
            if(strstr('ValorEnLetras',$reporte->contenido)){
                $this->load->library('enletras');
                foreach(fragmentar($reporte->contenido,'ValorEnLetras(',')') as $f){
                    list($val,$mon) = explode(',',$f);
                    $reporte->contenido = str_replace('ValorEnLetras('.$f.')',$this->enletras->ValorEnLetras($val,$mon),$reporte->contenido);
                }
            }
            
            //grafico
            if(strpos(strip_tags($reporte->contenido),'Grafico')){                
                include(APPPATH.'/libraries/graph/Phpgraphlib.php');
                foreach(fragmentar($reporte->contenido,'Grafico(',')') as $f){
                    list($array,$width,$height,$title) = explode(',',$f);
                    $graph = new PHPGraphLib($width,$height,'/var/www/html/deporvida/img/grafico.png');                    
                    foreach($consultas[$array]->result() as $r){
                        $data = array();
                        foreach($r as $n=>$v){
                            $data[$n] = $v;
                        }
                         $graph->addData($data);
                    }                    
                   
                    $graph->setTitle($title);
                    $graph->setBars(false);
                    $graph->setLine(true);
                    $graph->setDataPoints(true);
                    $graph->setDataPointColor('maroon');
                    $graph->setDataValues(true);
                    $graph->setDataValueColor('maroon');
                    $graph->setGoalLine(.0025);
                    $graph->setGoalLineColor('red');
                    $graph->createGraph();
                    $reporte->contenido = str_replace('Grafico('.$f.')','<img src="'.base_url('img/grafico.png').'">',$reporte->contenido);                    
                }
            }
            return $reporte;
        }
        
        function mostrarForm($reporte){
            
            $variables = explode(',',$reporte->variables);            
            $this->loadView(array('view'=>'form','var'=>$variables,'reporte'=>$reporte));
        }
        
        function mostrarReporte($reporte,$variables){
            $reporte->query = strip_tags($reporte->query);
            $reporte->query = str_replace('&#39;','\'',$reporte->query);
            $reporte->query = str_replace('&gt;','>',$reporte->query);
            $reporte->query = str_replace('&lt;','<',$reporte->query);
            $querys = explode(';', $reporte->query);
            
            $consultas = array();
            //Sacamos el nombre de la variable y su query                        
            for($i=0;$i<count($querys);$i++){
                $posicion = strpos($querys[$i],"=");
                $variable = trim(substr($querys[$i],$i,$posicion));
                $variable = trim(str_replace('=','',$variable));
                $query = substr($querys[$i],$posicion);
                $query = substr($query,(strpos($query,'=')+1)); //Quitamos el otro igual                
                $query = str_replace('|selec|','SELECT',$query); //Quitamos la validación XSS
                foreach($variables as $n=>$v){
                    $query = str_replace('$_'.$n,"'".$v."'",$query);
                }
                //Consultamos
                $consultas[$variable] = $this->db->query($query);
            }            
            //Configuramos cuerpo
            $cuerpo = fragmentar(strip_tags($reporte->contenido),'[',']');            
            for($i = 0;$i<count($cuerpo);$i++){
                $data = explode(':',$cuerpo[$i]);
                $variable = $data[0];
                if(!empty($consultas[$variable])){
                    $qr=$consultas[$variable];                                
                    if(in_array('table',$data)){ //Tabla
                        $reporte->contenido = str_replace('['.$cuerpo[$i].']',sqltotable($qr,$data[2]),$reporte->contenido);
                    }
                    elseif(count($data)==3){ //Mostrar
                        if(!empty($qr->row($data[1]))){
                            $reporte->contenido = str_replace('['.$cuerpo[$i].']',$qr->row($data[1])->{$data[2]},$reporte->contenido);
                        }else{
                            $reporte->contenido = str_replace('['.$cuerpo[$i].']','No encontrado',$reporte->contenido);
                        }
                    }
                }
            }
            
            //Reemplazamos muestreo de variables input
            foreach($variables as $n=>$v){
                $reporte->contenido = str_replace('$_'.$n,$v,$reporte->contenido);
            }
            //Calculamos las funciones llamadas desde el reporte
            $reporte = $this->reemplazarFunciones($reporte,$consultas);
            switch($_POST['docType']){
                case 'pdf':
                    $this->load->library('html2pdf/html2pdf');
                    $papel = $reporte->papel;
                    $html2pdf = new HTML2PDF($reporte->orientacion,$papel,'fr', false, 'ISO-8859-15', array(5,5,5,8));
                    $html2pdf->setDefaultFont('courier');
                    $html2pdf->writeHTML(utf8_decode($reporte->contenido));
                    ob_clean();
                    $html2pdf->Output('Reporte-'.date("dmY").'-'.$reporte->titulo.'.pdf');
                break;
                case 'html':
                    echo utf8_decode($reporte->contenido);
                break;
                case 'csv':
                    
                break;
            }            
            
        }
        

        function reportmaker(){
            $this->as['reportmaker'] = 'reportes';
            $crud = $this->crud_function('','',$this);        
            if($crud->getParameters()=='add'){
                $crud->set_rules('identificador','Identificador','required|alpha_numeric|callback_identificador');
            }            
            $crud->columns('titulo','papel','orientacion');
            //$crud->field_type('query','string');
            $crud->field_type('papel','enum',array('A4','L'));
            $crud->field_type('orientacion','enum',array('P','L'));
            //$crud->set_rules('query','QUERY','required');
            $crud->add_action('<i class="fa fa-eye"></i> Ver Reporte','',base_url('reportes/admin/verReportes/').'/');
            //$crud->set_clone();
            $this->loadView($crud->render());
        }  
    }
?>
