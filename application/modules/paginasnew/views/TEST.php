<div>
    <section id="home-section-15" style="padding: 70px 20px;">
        <h1 style="text-align: center;"><span style="color: #ffffff;">Estamos en todo Quito</span></h1>
        <div class="row" style="margin-top: 50px;">
            <div class="col-xs-12 col-sm-3 col-sm-offset-2 well" style="color: black;">
                <h1 style="text-align: center; border-bottom: 5px solid #00bcd4;"><span style="color: #000000;"><strong>Estándar</strong></span></h1>
                <div tabindex="1">
                    <p style="text-align: center; font-size: 40px;"><strong>$ 3.50</strong></p>
                </div>
                <div id="layerContent" tabindex="1" style="text-align: center; color: #737373;">por envío</div>
                <div tabindex="1"></div>
                <div tabindex="1">
                    <div class="row">
                        <div class="col-xs-1"><i class="fa fa-check" style="color: #4caf50;"></i></div>
                        <div class="col-xs-10">un mensajero recogerá y entregará tu paquete en el transcurso del día</div>
                    </div>
                    <div class="row">
                        <div class="col-xs-1"><i class="fa fa-check" style="color: #4caf50;"></i></div>
                        <div class="col-xs-10">
                            <div id="layerContent" tabindex="1">hasta 10 kms. por recorrido</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-1"><i class="fa fa-check" style="color: #4caf50;"></i></div>
                        <div class="col-xs-10">
                            <div id="layerContent" tabindex="1">0,30 ctvs. por km. adicional</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-1"><i class="fa fa-check" style="color: #4caf50;"></i></div>
                        <div class="col-xs-10">
                            <div id="layerContent" tabindex="1">monitoreo en tiempo real</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-1"><i class="fa fa-check" style="color: #4caf50;"></i></div>
                        <div class="col-xs-10">
                            <div id="layerContent" tabindex="1">notificaciones de cada estado del envío</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-sm-offset-2 well" style="color: black;">
                <h1 style="text-align: center; border-bottom: 5px solid #00bcd4;"><span style="color: #000000;"><strong>Express</strong></span></h1>
                <div tabindex="1">
                    <p style="text-align: center; font-size: 40px;"><strong>$ 4.50</strong></p>
                </div>
                <div tabindex="1">
                    <div id="layerContent" tabindex="1" style="text-align: center; color: #737373;">por envío</div>
                    <div tabindex="1" style="text-align: center; color: #737373;"></div>
                    <div tabindex="1">
                        <div class="row">
                            <div class="col-xs-1"><i class="fa fa-check" style="color: #4caf50;"></i></div>
                            <div class="col-xs-10">
                                <div id="layerContent" tabindex="1">un mensajero estará en su puerta en menos de 60 minutos</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-1"><i class="fa fa-check" style="color: #4caf50;"></i></div>
                            <div class="col-xs-10">
                                <div id="layerContent" tabindex="1">disponibilidad inmediata</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-1"><i class="fa fa-check" style="color: #4caf50;"></i></div>
                            <div class="col-xs-10">
                                <div id="layerContent" tabindex="1">hasta 10 kms. por recorrido</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-1"><i class="fa fa-check" style="color: #4caf50;"></i></div>
                            <div class="col-xs-10">
                                <div id="layerContent" tabindex="1">0,30 ctvs. por km. adicional</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-1"><i class="fa fa-check" style="color: #4caf50;"></i></div>
                            <div class="col-xs-10">monitoreo en tiempo real</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-1"><i class="fa fa-check" style="color: #4caf50;"></i></div>
                            <div class="col-xs-10">notificaciones de cada estado del envío</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="home-section-3">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 well" style="color: black; font-size: 18px;">
                <p style="font-size: 32px;">Límites geográficos de nuestro servicio</p>
                <div class="row">
                    <div class="col-xs-6 col-sm-3">
                        <div id="layerContent" tabindex="1" style="color: #00bcd4;"><strong>Al Norte:</strong></div>
                    </div>
                    <div class="col-xs-6 col-sm-9">
                        <div id="layerContent" tabindex="1" style="color: #737373;">Calderón, mitad del mundo</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-3">
                        <div id="layerContent" tabindex="1" style="color: #00bcd4;"><strong>Al Sur:</strong></div>
                    </div>
                    <div class="col-xs-6 col-sm-9">
                        <div id="layerContent" tabindex="1" style="color: #737373;">Quitumbe</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-3">
                        <div id="layerContent" tabindex="1" style="color: #00bcd4;"><strong>Valles:</strong></div>
                    </div>
                    <div class="col-xs-6 col-sm-9">
                        <div id="layerContent" tabindex="1" style="color: #737373;">Espe, Tumbaco</div>
                    </div>
                </div>
            </div>
        </div>
    </section>   
</div>