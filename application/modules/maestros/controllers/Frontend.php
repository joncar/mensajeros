<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }        
        
        function read($url){
            $url = $this->db->get_where('paginas',array('titulo'=>$url));
            if($url->num_rows()>0){
                $this->loadView(array('view'=>'read','page'=>$url->row(),'title'=>ucfirst(str_replace('-',' ',$url->row()->titulo))));
            }else{
                throw new exception('404','No hemos encontrado la pagina que solicita');
            }
        }
    }
?>
