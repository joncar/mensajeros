<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function ciudades(){
            $crud = $this->crud_function('','');
            $crud->columns('ciudades_nombre');
            $crud->display_as('ciudades_nombre','Nombre');
            $crud->field_type('json_limitacion','hidden')
                 ->field_type('json_limitacion2','hidden');
            $crud->unset_read()->unset_export()->unset_delete()->unset_print();
            $crud = $crud->render();
            $crud->crud = 'ciudades';
            $this->loadView($crud);
        }
        
        function tipos_servicios(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Tipo de Servicio');            
            $crud->unset_add()->unset_delete()->unset_read()->unset_export()->unset_print();
            $crud->add_action('<i class="fa fa-motorcycle"></i> Porcentajes Extra','',base_url('maestros/admin/porcentaje_km_extra').'/');
            $crud->display_as('precio_fuera','Precio por km extra');
            $crud->display_as('limite','limite en km para el precio 1');
            $crud = $crud->render();
            $crud->title = 'Tipos de Servicio';
            $this->loadView($crud);
        }
        
        function porcentaje_km_extra($x = ''){
            if(is_numeric($x)){
                $crud = $this->crud_function('','');
                $crud->set_subject('Valor km extra para '.$this->db->get_where('tipos_servicios',array('id'=>$x))->row()->tipos_servicios_nombre);
                $crud->display_as('desde','Desde la cantidad de KM');
                $crud->display_as('hasta','Hasta la cantidad de KM');
                $crud->display_as('porcentaje_recargo','Porcentaje de recargo km extra');
                $crud->callback_column('porcentaje_recargo',function($val){return $val.'%';});
                $crud->unset_columns('tipos_servicios_id');
                $crud->where('tipos_servicios_id',$x);
                $crud->field_type('tipos_servicios_id','hidden',$x);
                $crud = $crud->render();
                $crud->title = 'Valor KM Extra';
                $this->loadView($crud);
            }
        }
        
        function bancos(){
            $crud = $this->crud_function('','');            
            $crud = $crud->render();            
            $this->loadView($crud);
        }
    }
?>
