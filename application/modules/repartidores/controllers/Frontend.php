<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }
        
        function planilla($x = ''){            
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_subject('Planilla de solicitud');
            $crud->set_table('solicitudes_repartidores');
            $crud->unset_edit()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_list()
                 ->unset_export()
                 ->unset_print();  
            $crud->required_fields_array();
            $crud->unset_back_to_list();
            $crud->set_lang_string("form_save","Enviar Datos");
            $crud->set_lang_string("insert_success_message","Sus datos han sido enviado con éxito");
            $crud->set_lang_string("form_add","");
            $crud->field_type('tiene_android','checkbox');
            $crud->display_as('ciudades_id','Ciudad');
            $x = empty($x)?'2':'';
            $crud = $crud->render($x);
            
            $this->loadView(array('view'=>'planilla','crud'=>$crud,'title'=>'Planilla de solicitud'));
        }
        
        function mensajeros(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_subject('mensajeros');
            $crud->set_table('repartidores');
            $crud->unset_read()->unset_delete()->unset_edit()->unset_add()->unset_print()->unset_export();
            $crud = $crud->render();            
            $this->loadView($crud);
        }
    }
?>
