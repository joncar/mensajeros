<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function mensajeros($x = '',$y = ''){
            $this->as['mensajeros'] = 'repartidores';
            $crud = $this->crud_function('','');
            //ADD EDIT
            $crud->field_type('user_id','hidden',0)
                 ->field_type('lat','hidden',0)
                 ->field_type('lon','hidden',0)
                 ->field_type('calificacion','hidden','0')
                 ->field_type('fecha_registro','hidden',date("Y-m-d H:i:s"))
                 ->field_type('password','password');
            $crud->field_type('status','dropdown',
                            array(
                                '4'=>'<span class="label label-success">Activo</span>',
                                '2'=>'<span class="label label-warning">Express</span>',
                                '1'=>'<span class="label label-info">Estandar</span>',
                                '3'=>'<span class="label label-danger">Trámite</span>',
                                '5'=>'<span class="label label-default">Inactivo</span>',
                                '6'=>'Bloqueado'));
            if($crud->getParameters()=='add'){
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            }else{
                $crud->set_rules('email','Email','required|valid_email');
            }
            $crud->required_fields('fecha_registro','nombre','email','password','direccion','telefono','status','placa','fecha_nacimiento','No_licencia');
            $crud->fields('user_id','lat','lon','calificacion','fecha_registro','nombre','email','password','direccion','telefono','status','placa','fecha_nacimiento','No_licencia');
            
            if($crud->getParameters()=='edit'){
                $this->db->select('user.*');
                $this->db->join('user','user.id = repartidores.user_id');
                $this->db->where('repartidores.id',$y);
                $this->usern = $this->db->get('repartidores')->row();
                $crud->callback_edit_field('nombre',function($val,$primary){
                    return form_input('nombre',$this->usern->nombre,'id="field-user" class="form-control"');
                });
                $crud->callback_edit_field('email',function($val,$primary){
                    return form_input('email',$this->usern->email,'id="field-email" class="form-control"');
                });
                $crud->callback_edit_field('telefono',function($val,$primary){
                    return form_input('telefono',$this->usern->telefono,'id="field-telefono" class="form-control"');
                });
                $crud->callback_edit_field('direccion',function($val,$primary){
                    return form_input('direccion',$this->usern->direccion,'id="field-direccion" class="form-control"');
                });
                $crud->callback_edit_field('password',function($val,$primary){
                    return form_password('password',$this->usern->password,'id="field-password" class="form-control"');
                });
            }
            //Callbacks
            $crud->callback_before_insert(function($post){
                $data = array(
                    'nombre'=>$post['nombre'],
                    'email'=>$post['email'],
                    'password'=>md5($post['password']),
                    'direccion'=>$post['direccion'],
                    'telefono'=>$post['telefono'],
                    'admin'=>0,
                    'status'=>1,
                    'ciudades_id'=>1
                );
                get_instance()->db->insert('user',$data);
                $post['user_id'] = get_instance()->db->insert_id();
                get_instance()->db->insert('user_group',array('user'=>$post['user_id'],'grupo'=>2));
                $p = array_diff($post,$data);
                $p['status'] = $post['status'];
                unset($p['password']);
                return $p;
            });
            //Callbacks
            $crud->callback_before_update(function($post){
                $password = $this->usern->password!=$post['password']?md5($post['password']):$this->usern->password;
                $data = array(
                    'nombre'=>$post['nombre'],
                    'email'=>$post['email'],                    
                    'direccion'=>$post['direccion'],
                    'telefono'=>$post['telefono'],
                    'password'=>$password,
                    'admin'=>0,
                    'status'=>1,
                    'ciudades_id'=>1
                );
                get_instance()->db->update('user',$data,array('id'=>$this->usern->id));
                $p = array_diff($post,$data);                
                unset($p['password']);
                return $p;
            });
            
            $crud->callback_column('calificacion',function($val,$row){
                 $estrellas = 0;
                 $calificaciones = get_instance()->db->get_where('calificaciones',array('repartidores_id'=>$row->id))->num_rows();
                 if($calificaciones>0){
                     $estrellas = $val/$calificaciones;
                 }
                 $estrellas = round($estrellas,0);
                 $blancas = 5-$estrellas;
                 $str = '';
                 for($i=0;$i<$estrellas;$i++){
                     $str.= '<i style="color:orange" class="fa fa-star"></i> ';
                 }
                 for($i=0;$i<$blancas;$i++){
                     $str.= '<i class="fa fa-star-o"></i> ';
                 }
                 return $str;
            });
            //LIST
            if($crud->getParameters()=='list'){
                $crud->set_relation('user_id','user','nombre');
                $crud->display_as('user_id','Nombre');
            }            
            $crud->unset_read()->unset_delete();
            $crud->columns('user_id','status','calificacion');
            $crud = $crud->render();
            $crud->crud = 'repartidores';
            $this->loadView($crud);
        }
        
        function repartidores_cuenta($x = '',$y = ''){
            $this->as = array('repartidores_cuenta'=>'repartidores');
            if(is_numeric($y) && $y!=$this->user->id){
                header("Location:".base_url('repartidores/admin/repartidores_cuenta/edit/'.$y));
            }
            $crud = $this->crud_function('','');
            $crud->display_as('user_id','Nombre');
            $crud->field_type('status','dropdown',
                            array(
                                '4'=>'<span class="label label-success">Activo</span>',
                                '2'=>'<span class="label label-warning">Express</span>',
                                '1'=>'<span class="label label-info">Estandar</span>',
                                '3'=>'<span class="label label-danger">Trámite</span>',
                                '5'=>'<span class="label label-default">Inactivo</span>',
                                '6'=>'Bloqueado'));
            $crud->where('user_id',$this->user->id);
            $crud->columns('user_id','status');
            $crud->unset_add()
                 ->unset_export()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function solicitudes_repartidores($x = '',$y = ''){
            if($x=="aprobar"){
                if(is_numeric($y)){
                    $repartidor = $this->db->get_where('solicitudes_repartidores',array('id'=>$y));
                    if($repartidor->num_rows()>0){
                        $repartidor = $repartidor->row();
                        $repartidor = (array)$repartidor;
                        $repartidor['password'] = md5('12345678');
                        $repartidor['status'] = 1;
                        $repartidor['admin'] = 0;
                        $repartidor['fecha_registro'] = date("Y-m-d H:i:s");
                        $repartidor['fecha_actualizacion'] = date("Y-m-d H:i:s");
                        $existe = $this->db->get_where('user',array('email'=>$repartidor['email']));
                        $repartidor_id = 0;
                        if($existe->num_rows()==0){
                            unset($repartidor['id']);
                            $this->db->insert('user',$repartidor);
                            $repartidor_id = $this->db->insert_id();
                            $this->db->insert('repartidores',array('user_id'=>$repartidor_id,'lat'=>0,'lon'=>0));
                            $this->db->insert('user_group',array('user'=>$repartidor_id,'grupo'=>2));
                            $this->db->delete('solicitudes_repartidores',array('id'=>$y));
                        }else{
                            $existe = $existe->row();
                            if($this->db->get_where('repartidores',array('user_id'=>$existe->id))->num_rows()>0){
                                echo 'El repartidor deseado ya existe registrado en el sistema <script>setTimeout(function(){document.location.href="'.base_url('repartidores/admin/solicitudes_repartidores').'"},3000);</script>';
                                die();
                            }else{
                                $this->db->insert('repartidores',array('user_id'=>$existe->id,'lat'=>0,'lon'=>0));
                                $this->db->insert('user_group',array('user'=>$repartidor_id,'grupo'=>2));
                                $this->db->delete('solicitudes_repartidores',array('id'=>$y));
                            }
                        }
                    }
                }
                header("Location:".base_url('repartidores/admin/solicitudes_repartidores/success'));
            }else{
                $crud = $this->crud_function('','');
                $crud->set_subject('Solicitud para ser repartidor');
                $crud->add_action('<span style="color:green"><i class="fa fa-check"></i> Aprobar</span>','',base_url('repartidores/admin/solicitudes_repartidores/aprobar').'/');
                $crud = $crud->render();
                $crud->title = "Solicitud para ser repartidor";
                $this->loadView($crud);
            }
        }
    }
?>
