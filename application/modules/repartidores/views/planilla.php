<?php get_header_crud($crud->css_files, $crud->js_files) ?>
<section style="margin-top:80px">
    <?= $crud->output ?>
</section>
<script>
    $(document).ready(function(){
        $(".btn-group").addClass('btn-block');
        $("#form-button-save").addClass('btn-lg btn-info').removeClass('btn-success');
        $("#form-button-save").css('width','100%');
    });
</script>