<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function ajustes(){
            $crud = $this->crud_function('','');            
            $crud->display_as('precio','Precio/km dentro de limitación')
                 ->display_as('precio_fuera','Precio/km fuera de limitación');
            $crud->set_field_upload('politicas','files');
            $crud = $crud->render();
            $this->loadView($crud);
        }   
        
        public function configuraciones(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        } 
        
        function ventasespera(){
            $this->as = array('ventasespera'=>'pedidos');
            $crud = $this->crud_function('','');
            $crud->set_subject('pedidos en espera');
            $crud->columns('id','s0d0e1b62','sdfe80649','status','precio');
            $crud->display_as('s0d0e1b62','Cliente')
                 ->display_as('sdfe80649','Repartidor')
                 ->display_as('fecha','Fecha de Creación')
                 ->display_as('id','#Pedido');            
            $crud->set_relation('clientes_id','clientes','user_id')
                 ->set_relation('j3eb7f57f.user_id','user','nombre');            
            $crud->set_relation('repartidores_id','repartidores','user_id');
            $crud->set_relation('j8c93d82d.user_id','user','nombre');
            $crud->display_as('clientes_id','Cliente')
                     ->display_as('repartidores_id','Repartidor');
            $crud->where('pedidos.status <=','2');
            $crud->callback_column('status',function($val,$row){
                switch($val){
                    case '-2':
                        return '<span class="label label-danger">Cancelado por el banco</span>';
                    break; 
                    case '-1': 
                        return '<span class="label label-danger">Cancelado</span>';
                    break;                     
                    case '0':
                        return '<span class="label label-warning">Esperando Pago</span>';
                    break;
                    case '1':
                        return '<span class="label label-warning">Sin Asignar</span>';
                    break;
                    case '2': 
                        return '<span class="label label-default">Mensajero Asignado</span>';                    
                    break;                
                    case '3': 
                        return '<span class="label label-info">Paquete Recogido</span>';
                    break;
                    case '4': 
                        return '<span class="label label-success">En Tránsito</span>';
                    break;
                    case '5': 
                        return '<span class="label label-success">Entregado</span>';
                    break;
                    case '6': 
                        return '<span class="label label-success">Entregado y Firmado</span>';
                    break;
                }
            });
            $crud->unset_add()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();
            $crud = $crud->render();
            echo $crud->output;
        }
        function ventastransito(){
            $this->as = array('ventastransito'=>'pedidos');
            $crud = $this->crud_function('','');
            $crud->set_subject('pedidos en transito');
            $crud->where('pedidos.status >','2');
            $crud->where('pedidos.status <','5');
            $crud->columns('id','s0d0e1b62','sdfe80649','status','precio');
            $crud->display_as('s0d0e1b62','Cliente')
                 ->display_as('sdfe80649','Repartidor')
                 ->display_as('fecha','Fecha de Creación')
                 ->display_as('id','#Pedido');            
            $crud->set_relation('clientes_id','clientes','user_id')
                 ->set_relation('j3eb7f57f.user_id','user','nombre');            
            $crud->set_relation('repartidores_id','repartidores','user_id');
            $crud->set_relation('j8c93d82d.user_id','user','nombre');
            $crud->display_as('clientes_id','Cliente')
                     ->display_as('repartidores_id','Repartidor');
            $crud->callback_column('status',function($val,$row){
                switch($val){
                    case '-2':
                        return '<span class="label label-danger">Cancelado por el banco</span>';
                    break; 
                    case '-1': 
                        return '<span class="label label-danger">Cancelado</span>';
                    break;                     
                    case '0':
                        return '<span class="label label-warning">Esperando Pago</span>';
                    break;
                    case '1':
                        return '<span class="label label-warning">Sin Asignar</span>';
                    break;
                    case '2': 
                        return '<span class="label label-default">Mensajero Asignado</span>';                    
                    break;                
                    case '3': 
                        return '<span class="label label-info">Paquete Recogido</span>';
                    break;
                    case '4': 
                        return '<span class="label label-success">En Tránsito</span>';
                    break;
                    case '5': 
                        return '<span class="label label-success">Entregado</span>';
                    break;
                    case '6': 
                        return '<span class="label label-success">Entregado y Firmado</span>';
                    break;
                }
            });
            $crud->unset_add()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();
            $crud = $crud->render();
            echo $crud->output;
        }
        function ventascompletado(){
            $this->as = array('ventascompletado'=>'pedidos');
            $crud = $this->crud_function('','');
            $crud->set_subject('pedidos completados');
            $crud->where('pedidos.status >=','5');
            $crud->columns('id','s0d0e1b62','sdfe80649','status','precio');
            $crud->display_as('s0d0e1b62','Cliente')
                 ->display_as('sdfe80649','Repartidor')
                 ->display_as('fecha','Fecha de Creación')
                 ->display_as('id','#Pedido');            
            $crud->set_relation('clientes_id','clientes','user_id')
                 ->set_relation('j3eb7f57f.user_id','user','nombre');            
            $crud->set_relation('repartidores_id','repartidores','user_id');
            $crud->set_relation('j8c93d82d.user_id','user','nombre');
            $crud->display_as('clientes_id','Cliente')
                     ->display_as('repartidores_id','Repartidor');
            $crud->callback_column('status',function($val,$row){
                switch($val){
                    case '-2':
                        return '<span class="label label-danger">Cancelado por el banco</span>';
                    break; 
                    case '-1': 
                        return '<span class="label label-danger">Cancelado</span>';
                    break;                     
                    case '0':
                        return '<span class="label label-warning">Esperando Pago</span>';
                    break;
                    case '1':
                        return '<span class="label label-warning">Sin Asignar</span>';
                    break;
                    case '2': 
                        return '<span class="label label-default">Mensajero Asignado</span>';                    
                    break;                
                    case '3': 
                        return '<span class="label label-info">Paquete Recogido</span>';
                    break;
                    case '4': 
                        return '<span class="label label-success">En Tránsito</span>';
                    break;
                    case '5': 
                        return '<span class="label label-success">Entregado</span>';
                    break;
                    case '6': 
                        return '<span class="label label-success">Entregado y Firmado</span>';
                    break;
                }
            });
            $crud->unset_add()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();
            $crud = $crud->render();
            echo $crud->output;
        }
        function clientesonline(){
            $this->as = array('clientesonline'=>'clientes');
            $crud = $this->crud_function('','');            
            $crud->set_subject('Clientes');
            $crud->columns('clientes_nombre','email','celular','ultima_conexion')
                     ->display_as('clientes_nombre','Cliente');
            $crud->where("ultima_conexion!='0000-00-00 00:00:00' AND MINUTE(TIMEDIFF(TIME(NOW()),TIME(ultima_conexion)))<=5",null,TRUE);
            $crud->unset_add()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();
            $crud = $crud->render();
            echo $crud->output;
        }
        function repartidoresonline(){
            $this->as = array('repartidoresonline'=>'repartidores');
            $crud = $this->crud_function('','');         
            $crud->set_subject('Repartidores');
            $crud->columns('repartidores_nombre','email','celular','ultima_conexion')
                     ->display_as('repartidores_nombre','Repartidor');
            $crud->where("ultima_conexion!='0000-00-00 00:00:00' AND MINUTE(TIMEDIFF(TIME(NOW()),TIME(ultima_conexion)))<=5",null,TRUE);
            $crud->unset_add()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_edit();
            $crud = $crud->render();
            echo $crud->output;
        }
    }
?>
