<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }        
        
        function read($url){
            $url = $this->db->get_where('paginas',array('titulo'=>$url));
            if($url->num_rows()>0){
                $this->loadView(array('view'=>'read','page'=>$url->row(),'title'=>ucfirst(str_replace('-',' ',$url->row()->titulo))));
            }else{
                throw new exception('404','No hemos encontrado la pagina que solicita');
            }
        }
        
        function registrar(){
            $this->form_validation->set_rules('email','Email','required|valid_email|is_unique[emails.email]');
            if($this->form_validation->run()){
                $this->db->insert('emails',array('email'=>$_POST['email']));
                echo $this->success('Gracias por subscribirte');
            }else{
                echo $this->error($this->form_validation->string_error());
            }
        }
    }
?>
