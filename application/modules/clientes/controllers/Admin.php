<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function clientes(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function emails(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function solicitudes_empresas(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Solicitud para ser empresa');
            $crud = $crud->render();
            $crud->title = "Solicitud para ser empresa";
            $this->loadView($crud);
        }
        
        function sms(){
            $resp = '';
            if(!empty($_POST)){
                $this->form_validation->set_rules('dest','Dest','required');
                $this->form_validation->set_rules('mensaje','Mensaje','required');
                $this->form_validation->set_rules('tipo','Tipo','required');
                if($this->form_validation->run()){
                   switch($_POST['tipo']){
                       case 'sms':
                           $this->load->library('textoatodos');
                           $this->db->join($_POST['dest'],$_POST['dest'].'.user_id = user.id','inner');
                           foreach($this->db->get('user')->result() as $u){
                               $this->textoatodos->send($u->celular,$_POST['mensaje']);
                           }
                       break;
                       case 'push':
                           $this->load->library('gcm_library');
                           foreach($this->db->get('gcm_'.$_POST['dest'])->result() as $u){
                               $this->gcm_library->send(array($u->valor),$this->gcm_library->encodeMsj('Mensaje de mensajeros.ec',$_POST['mensaje']));
                           }
                       break;
                       $resp = $this->success('mensaje enviado con éxito');
                   }
                }else{
                    $resp = $this->error($this->form_validation->error_string());
                }
            }
            $this->loadView(array('view'=>'sms','resp'=>$resp));
        }
    }
?>
