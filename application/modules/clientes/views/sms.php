<div><?= !empty($resp)?$resp:'' ?></div>
<div class="col-xs-12 col-sm-6 col-sm-offset-3">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title">Enviar SMS mediante Mensaje a Todos</h1>
        </div>
        <div class="panel-body">
            <form action="" method="post">
                <div class="form-group">
                    <label>Destinatarios</label>
                    <input type="radio" name="dest" value="repartidores">Mensajeros
                    <input type="radio" name="dest" value="clientes">Clientes
                </div>
                 <div class="form-group">
                    <label>Mensaje</label>
                    <textarea name="mensaje" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label>Tipo</label>
                    <input type="radio" name="tipo" value="sms">SMS
                    <input type="radio" name="tipo" value="push">PUSH
                </div>
                <button class="btn btn-success" type="submit">Enviar SMS</button>                   
            </form>
        </div>
    </div>
</div>    