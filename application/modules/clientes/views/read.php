<section id="courier-section-1">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h1>¡Establece tu horario y encuentra trabajo de mensajero siempre!</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <a target="_blank" href="http://mensajerosurbanos.com/unete-formulario" class="btn btn-yellow">
                    ÚNETE AHORA
                </a>
            </div>
            <div class="col-xs-12 col-sm-6">
                <button ng-click="$parent.scrollTo('courier-section-4')" class="btn btn-outlined">
                    REQUISITOS
                </button>
            </div>
        </div>
    </section>

    <section id="courier-section-2">
        <h2>Mira Cuánto Puedes Ganar</h2>
        <h3>¿Cuántos kilómetros de mensajería quieres "rodar" en un mes?</h3>
        <div ng-show="vm.earn === undefined" class="row">
            <form novalidate="" name="calculateEarn" class="ng-pristine ng-invalid ng-invalid-required">
                <div class="col-xs-12 col-sm-4">
                    <input type="number" required="" ng-model="vm.kilometers" placeholder="Kilómetros" name="kilometers" class="ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required">
                    <p ng-show="calculateEarn.kilometers.$invalid &amp;&amp; calculateEarn.kilometers.$dirty" class="error-input ng-hide">
                        Por favor ingresa la cantidad de kilómetros
                    </p>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <select required="" ng-options="city as city.name for city in vm.cities track by city.id" ng-model="vm.city" class="ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-required"><option label="Bogotá" value="1" selected="selected">Bogotá</option><option label="Cali" value="2">Cali</option><option label="Medellín" value="3">Medellín</option><option label="Barranquilla" value="4">Barranquilla</option><option label="Villavicencio" value="5">Villavicencio</option></select>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <button ng-click="vm.calculate()" class="btn btn-blue btn-block">
                        Calcular
                    </button>
                </div>
            </form>
        </div>
        <div ng-hide="vm.earn === undefined" class="ng-hide">
            <h4 class="ng-binding">Gana hasta $ COP en Bogotá</h4>
            <button ng-click="vm.earn = undefined" class="btn btn-yellow">
                Volver a calcular
            </button>
        </div>
    </section>

    <section id="courier-section-3">
        <h2>¿Por qué ser aliado de Mensajeros Urbanos?</h2>
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <img alt="" src="public/img/landing/recruit-courier/ic-ingresos.svg">
                <h3>Obtén mejores ingresos</h3>
                <p>
                    Obtén trabajo de mensajero y aumenta tu salario mes a mes.
                </p>
            </div>
            <div class="col-xs-12 col-sm-3">
                <img alt="" src="public/img/landing/recruit-courier/ic-microempresario.svg">
                <h3>Sé un microempresario</h3>
                <p>
                    Tus ingresos dependen del compromiso con tus clientes.
                </p>
            </div>
            <div class="col-xs-12 col-sm-3">
                <img alt="" src="public/img/landing/recruit-courier/ic-esfuerzo.svg">
                <h3>Valoramos tu esfuerzo</h3>
                <p>
                    Ingresa a nuestro grupo VIP y obtén beneficios adicionales.
                </p>
            </div>
            <div class="col-xs-12 col-sm-3">
                <img alt="" src="public/img/landing/recruit-courier/ic-soporte.svg">
                <h3>Soporte 24/7</h3>
                <p>
                    Ten una comunicación directa con nuestro equipo las 24hrs.
                </p>
            </div>
        </div>
    </section>

    <section id="courier-section-4">
        <h2>Lo que necesitas para ser nuestro aliado</h2>
        <p>El trabajo de mensajero dentro de Mensajeros Urbanos requiere un compromiso serio.</p>

        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <img alt="" src="public/img/landing/recruit-courier/ic-android.svg">
                <p>
                    Celular Android <br> Plan de datos activo
                </p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <img alt="" src="public/img/landing/recruit-courier/ic-email.svg">
                <p>
                    Moto o Bicicleta <br> en buen estado
                </p>
            </div>
            <div class="col-xs-12 col-sm-4">
                <img alt="" src="public/img/landing/recruit-courier/ic-moto.svg">
                <p>
                    Correo electrónico <br> propio y activo
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <img alt="" src="public/img/landing/recruit-courier/ic-papeles.svg">
                <ul>
                    <li><p>Certificado EPS</p></li>
                    <li><p>RUT actividad económica 5320 o 8299 - (Opcional)</p></li>
                    <li><p>Recibo servicios públicos con dirección de residencia</p></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6">
                <img alt="" src="public/img/landing/recruit-courier/ic-fotocopia.svg">
                <p>Fotocopia pase ampliado al 150%</p>
                <p>Fotocopia SOAT y Tecnomecánica</p>
                <p>Fotocopia cédula ampliada al 150%</p>
                <p>Fotocopia tarjeta de propiedad del vehículo</p>
            </div>
        </div>
    </section>