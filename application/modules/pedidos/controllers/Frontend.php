<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }
        
        function solicitar($x = ''){
            $clientes_id = isset($this->user->clientes_id)?$this->user->clientes_id:0;
            $this->loadView(array('view'=>'front_solicitud','clientes_id'=>$clientes_id,'title'=>'Solicitar Servicio'));
        }
        
        function favoritos(){
            if(isset($_GET['id'])){
                $crud = new ajax_grocery_CRUD();            
                $crud->set_theme('crud_dependency');
                $crud->set_subject('<i class="fa fa-heart"></i> Mis favoritos');
                $crud->set_table('favoritos');

                if(!empty($_SESSION['user'])){
                    $crud->where('user_id',$this->user->id);
                }else{
                    $crud->where('user_id',-1);
                }

                $crud->callback_column('direccion1',function($val,$row){
                    return '<a href=\'javascript:setFavorito('.$_GET['id'].','.json_encode($row).')\'><i class="fa fa-download"></i> </a>'.$row->direccion1;
                });

                $crud->unset_edit()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_export()
                     ->unset_print()
                     ->unset_add();
                $crud->norequireds = array('direccion2','numeracion');
                $crud->required_fields_array();
                $crud->columns('direccion1');            
                $crud = $crud->render();
                echo get_header_crud($crud->css_files, $crud->js_files,TRUE).$crud->output;
            }else{
                echo "Ha ocurrido un error interno, por favor intente más tarde";
            }
        }
        
        function pedidos($x = '',$y = ''){            
            $crud = new ajax_grocery_CRUD();            
            $crud->set_theme('crud_dependency');
            $crud->set_subject('Pedidos');
            $crud->set_table('pedidos');
            $crud->required_fields_array();
            $crud->set_rules('clientes_id','cliente','callback_is_login');
            $crud->unset_back_to_list();
            $crud->callback_before_insert(function($post){
                $cliente = $this->db->get_where('clientes',array('user_id'=>$this->user->id));
                if($cliente->num_rows()>0){
                    $post['clientes_id'] = $cliente->row()->id;
                }
                return $post;
            });
            $crud->display_as('s0d0e1b62','Cliente')
                 ->display_as('sdfe80649','Repartidor')
                 ->display_as('fecha','Fecha de Creación')
                 ->display_as('id','#Pedido');            
            $crud->set_relation('clientes_id','clientes','user_id')
                 ->set_relation('j3eb7f57f.user_id','user','nombre');
            
            $crud->set_relation('repartidores_id','repartidores','user_id');
            $crud->set_relation('j8c93d82d.user_id','user','nombre');
            $crud->columns('id','s0d0e1b62','sdfe80649','status','precio','fecha_solicitud','turno','fecha','fecha_solicitud','fecha_entregado');
            $crud->set_rules('valor_paquete','Valor del Paquete','required|numeric');
            if($crud->getParameters()!=='insert' || !empty($_SESSION['user'])){                
                $crud->callback_after_insert(function($post,$primary){
                    if($post['tipo_pago']==1){
                        $_SESSION['irapayphone'] = 1;
                    }//Traguear pago a payphone una vez llegada
                    if(!empty($post['ida_vuelta'])){
                        $_POST['detalles'][] = $_POST['detalles'][0];
                    }
                    get_instance()->querys->guardar_detalles($_POST['detalles'],$primary);                    
                });
                $crud->render(); 
            }    
            
        }
        
        function is_login(){
            if(empty($_SESSION['user'])){
                $_SESSION['pedido'] = $_POST;
                $this->form_validation->set_message('is_login','<script>document.location.href="'.base_url('registro/index/add').'?redirect='.base_url('pedidos/admin/procesar_pendiente').'";</script>');
                return false;
            }else{
                $_POST['user_id'] = $this->user->id;                
            }
            return true;
        }
        
        function consultarRastreo(){
            $this->form_validation->set_rules('id','ID','required');
            if($this->form_validation->run()){
                $pedido = $this->db->get_where('pedidos',array('guia'=>$this->input->post('id')));
                if($pedido->num_rows()>0){
                    echo json_encode(array(array('id'=>$pedido->row()->id)));
                }else{
                    echo '[]';
                }
            }
        }
        
        function rastreame($id){
            if(is_numeric($id)){
                $pedido = $this->db->get_where('pedidos',array('id'=>$id));
                $data = array();
                if($pedido->num_rows()>0){         
                    $this->db->select('user.*, repartidores.lat, repartidores.lon');
                    $this->db->join('user','user.id = repartidores.user_id');
                    $pedido->row()->repartidor = $this->db->get_where('repartidores',array('repartidores.id'=>$pedido->row()->repartidores_id));
                    $pedido->row()->repartidor->lat = !empty($pedido->row()->repartidor->lat)?$pedido->row()->repartidor->lat:0;
                    $pedido->row()->repartidor->lon = !empty($pedido->row()->repartidor->lon)?$pedido->row()->repartidor->lon:0;
                    
                    $data['grafico'] = $this->load->view('_grafico_rastreo',array('pedido'=>$pedido->row()),TRUE);
                    $data['mensajero'] = $this->load->view('_grafico_repartidor',array('pedido'=>$pedido->row()),TRUE);
                    $data['pedido'] = $pedido->row();                    
                }
                echo json_encode($data);
            }
        }
        
        function rastrear($id){
            if(!empty($_SESSION['irapayphone'])){
                unset($_SESSION['irapayphone']);
                redirect(base_url('pedidos/admin/pagar/payphone/'.$id));
            }
            if(is_numeric($id)){
                $pedido = $this->db->get_where('pedidos',array('id'=>$id));
                if($pedido->num_rows()>0){
                    $pedido->row()->direcciones = $this->db->get_where('pedidos_detalles',array('pedidos_id'=>$id));
                    $this->db->select('user.*, repartidores.lat, repartidores.lon');
                    $this->db->join('user','user.id = repartidores.user_id');
                    $pedido->row()->repartidor = $this->db->get_where('repartidores',array('repartidores.id'=>$pedido->row()->repartidores_id));
                    $pedido->row()->repartidor->lat = !empty($pedido->row()->repartidor->lat)?$pedido->row()->repartidor->lat:0;
                    $pedido->row()->repartidor->lon = !empty($pedido->row()->repartidor->lon)?$pedido->row()->repartidor->lon:0;
                    $pedido->row()->ciudad = $this->db->get_where('ciudades',array('id'=>$pedido->row()->ciudades_id))->row();
                    $pedido->row()->tipos_servicios = $this->db->get_where('tipos_servicios',array('id'=>$pedido->row()->tipo_tramite))->row();
                    $this->loadView(array('view'=>'rastrear','id'=>$id,'pedido'=>$pedido->row()));
                }else{
                    header("Location:".base_url(''));
                }
            }else{
                header("Location:".base_url(''));
            }
        }
        
        function pagook(){
            $decrypt = new PayPhoneDecrypt('<RSAKeyValue><Modulus>7GjdrmmP22e/rjggkHn93PgJ7LosLbWvaWEZEKfT6BkknmgvaxEd91V6QrXYFpH4tcT5rkJqRU7o9SlnyLJ2Y3wu2EaPHYyPGbNuH9171WePO/a+rRW0Tilp4myYka19FmWAUS91XF0Bu/5eMYZYkOJ8w1Hf152Dsq2FH3/3h80=</Modulus><Exponent>AQAB</Exponent><P>+JiT7ttnM4mnmgyKgQBi2Tq9imWiwme1xEMyY5wH/pVtbceTM4EUhaAoLKsZf4NHPVr/gtM3xDAev2/6Lq9Pdw==</P><Q>83NfeywR6igIqTt6Ufsj2LmMH7N1ZS/VuBW8SfNbjG9I7m38rzLvGklASw6CBkQ8V6BecahdE5hb1d7Xu4sb2w==</Q><DP>ndbqno7p4Lw3EUWDL1MacAiuzc+oXrzQmRN9S+eG3+rqdvhvixUa/ZjpySq93stwkRFVCDCpyMQw0aX8eGXREw==</DP><DQ>7kLgE6BR+CUuNNJtKIbt0lHl/bCZGGr0c7FyEb4Gvy665f7m6mCi+crvDYYpYxCtLeVaCUqJrX/TgUETSg0fOQ==</DQ><InverseQ>WrJ6K/7coNXgfj/L7uyF/h9LalZbO8ZygbeC0pbAK6ojMX/lwoVyrqyIycwfUUhX312u7VKaJ0y5CWMuuzfV9Q==</InverseQ><D>c339gnXGliC2pzV7TymRqDAypQgEiiYetiP920GqB9K0rYBnXe5kTc7f5hQxJ0GTl7VNZuDCSu60ZwSd05G/vITuTxEfYOjaWOa1bvNv4EPfGOzx3XJ/8WkcBX/3y6aCWSRg9VL6h9AFQNTftfo74pxREQSRdP9lKY/Gg5uZ4f8=</D></RSAKeyValue>');
            $response = new DataSend();
            $response->IV = $_POST['IV'];
            $response->SessionKey = $_POST['SessionKey'];
            $response->XmlReq = $_POST['XmlReq'];
            //Descifrar los datos
            $respuesta = $decrypt->Execute($response);
            //print_r($respuesta);
            if($respuesta->StatusCode==3){
                $this->db->update('pedidos',array('status'=>2),array('id'=>$respuesta->ClientTransactionId));
                $this->loadView(array('view'=>'_pagook','response'=>$respuesta));
            }else{
                $this->loadView(array('view'=>'_pagonok','response'=>$respuesta));
            }
            
        }
        /**************** MOVIL PAYMENT **************************/
        function pagarMovil($idc,$url){
            if(is_numeric($idc)){
                $pedido = $this->db->get_where('pedidos',array('id'=>$idc,'status <'=>1));
                if($pedido->num_rows()>0){
                    $this->loadView(array('view'=>'_payphone_movil','id'=>$idc,'pedido'=>$pedido,'url'=>$url));
                }else{
                    //header("Location:".base_url('pedidos/admin/pedidos'));
                }
            }
        }
        
        function pagookMovil(){
            $decrypt = new PayPhoneDecrypt('<RSAKeyValue><Modulus>rEhSfq3PkBoJLjtDpbGwcAUX50Ska5fy8q/RJiVmOj0T/KvByX8v2WWJd04VmsicW4QuAB+Lx5mJNqN7BmcVZ73rSTwEkhy3SKRgV5Usu0ZOUtlz4MelZI3z7CoGLMwOH0XbHxTdHSTwyzKZiDmRktkkNt8tGx8e3NgnUAn0G50=</Modulus><Exponent>AQAB</Exponent><P>7JuLAVo+5DjxM4bgj6ZzuxlXJJ4w3yxZyj8Q9xwXRHpBp7YSpC8NXNAH8pXfE8joJCzHUzweu4bO0khppEJyHQ==</P><Q>umcfSYA5U+KZQQvZjLY/YMDpvnqRhFH9hTIzn5DRg84TXxbt8klD50IuMloHBn6Z/5csGdGHdAHLwV6yx5gXgQ==</Q><DP>ltSpDlmrUe2CxWgr6ycfC5yh0rQNT5eEPctqUzzTEFInXHRS+dsM16e+CUTFCmW+pqDtCACBTuYnHiIPRikdeQ==</DP><DQ>TPWak0wfXyTlRVfRICl2jUnYt83/GnSHiWCqs6yk9Bg3I0FiSHA7WtWWIS/OSr4mcsJFcPtzQ1AigdnJUmLXAQ==</DQ><InverseQ>E1/Yjt9vdROLK+fWMN1z3i/Be4i/v4lGVghvA71PcUancVa+GC6Ly0GucVLQ+81Uv/Q9ZLraZPj/Do85jZNUbA==</InverseQ><D>Bcu6EL5uqumhUO+X90FKzv3w36wgvujh+LWe+uJLtd5bp4assodnl+/xR7T2B5rDndWMyZ7GEE0zP0YRK7Y4k1JmeYBh+vSEo8mOaIOJthidhoQr7RIP+UNW2GXd3SqmUwc7hZ2ZbP1v54JJ6GUWlAjSB/rVIZU00B5nVLU3JYE=</D></RSAKeyValue>');
            $response = new DataSend();
            $response->IV = $_POST['IV'];
            $response->SessionKey = $_POST['SessionKey'];
            $response->XmlReq = $_POST['XmlReq'];
            //Descifrar los datos
            $respuesta = $decrypt->Execute($response);
            //print_r($respuesta);
            if($respuesta->StatusCode==3){
                $this->db->update('pedidos',array('status'=>2),array('id'=>$respuesta->ClientTransactionId));
            }else{
                echo 'error';
            }
        }
    }
?>
