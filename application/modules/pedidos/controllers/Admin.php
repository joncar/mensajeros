<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function getpedidos(){
                $crud = $this->getADMINCRUD();
                $crud->callback_before_update(function($post,$primary){
                    $rep = get_instance()->db->get_where('pedidos',array('id'=>$primary));
                    if($rep->num_rows()>0){
                        //Notificar
                        if($post['status']>1 && $post['pagado']==1){
                            get_instance()->querys->notificar('repartidores',$post['repartidores_id'],'Se te ha asignado un pedido','Haz sido asignado para el pedido #'.$primary.' Entra en pedidos del dia en la aplicación para más detalles');                    
                        }
                        
                        //Verificar si pago
                        //Insertar datos
                        switch($post['status']){
                            case '-2':
                            case '-1':
                            case '0': $post['pagado'] = 0; break;
                            case '1':
                            case '2': 
                            case '3':
                            case '4':                                           
                            case '5': $post['pagado'] = 1; break;
                        }
                        return $post;
                    }
                });
                $crud->callback_before_insert(function($post){
                    $post['fecha_solicitud'] = date("Y-m-d H:i:s",strtotime($post['fecha_solicitud']));
                    return $post;
                });
                $crud->add_action('<i class="fa fa-phone"></i> Notificar','',base_url('pedidos/admin/notificar').'/');
                return $crud;   
        }
        
        function pedidos(){            
            $crud = $this->getpedidos();
            $out = $crud->render();
            $out->crud = 'pedidos';
            $out->title = 'Mis Servicios';
            $this->loadView($out);
        }
        
        function estandar(){
            $this->as['estandar'] = 'pedidos';
            $crud = $this->getpedidos();  
            $crud->unset_add();
            $crud->where('tipo_tramite',1);
            $crud->set_subject('Pedidos Estándar');
            $this->draw($crud);
        }
        
        function express(){
            $this->as['express'] = 'pedidos';
            $crud = $this->getpedidos();  
            $crud->unset_add();
            $crud->where('tipo_tramite',2);
            $crud->set_subject('Pedidos Express');
            $this->draw($crud);
        }
        
        function tramite(){
            $this->as['tramite'] = 'pedidos';
            $crud = $this->getpedidos();  
            $crud->unset_add();
            $crud->where('tipo_tramite',3);
            $crud->set_subject('Pedidos de Trámite');
            $this->draw($crud);
        }
        
        function draw($crudobject){             
            if(empty($_GET['tipo']) || $_GET['tipo']==1){ 
                $crud = clone $crudobject;
                $crud->where('pedidos.status >=',0);
                $crud->where('pedidos.status <',5);
                $out = $crud->render();
            }
            if(empty($_GET['tipo']) || $_GET['tipo']==5 || $_GET['tipo'] == 6){ 
                $crud = clone $crudobject;
                $crud->where('pedidos.status >=',5);
                $out->output2 = $crud->render();
                $out->output2 = $out->output2->output;
            }
            
            if(empty($_GET['tipo']) || $_GET['tipo']==-1){ 
                $crud = clone $crudobject;
                $crud->where('pedidos.status',-1);
                $out->output3 = $crud->render();
                $out->output3 = $out->output3->output;
            }
            
            $out->crud = 'pedidos';
            $out->title = 'Mis Servicios';
            $this->loadView($out);
        }
        
        function getADMINCRUD(){
            $crud = $this->crud_function('','');
            $crud->field_type('status','dropdown',array(
                '-2'=>'Cancelado por el banco',
                '-1'=>'Cancelado',
                '0'=>'Esperando Pago',
                '1'=>'Sin asignar',
                '2'=>'Mensajero Asignado',
                '3'=>'Paquete Recogido',
                '4'=>'En Tránsito',
                '5'=>'Entregado',
                '6'=>'Entregado y Firmado'
            ))
            ->field_type('tipo_tramite','dropdown',array(
                '1'=>'ESTANDAR',
                '2'=>'EXPRESS',
                '3'=>'TRAMITE',                
            ))
                ->field_type('fecha_solicitud','input');
            $crud->display_as('s0d0e1b62','Cliente')
                 ->display_as('sdfe80649','Repartidor')
                 ->display_as('fecha','Fecha de Creación')
                 ->display_as('id','#Pedido');            
            $crud->set_relation('clientes_id','clientes','user_id')
                 ->set_relation('j3eb7f57f.user_id','user','nombre');            
            $crud->set_relation('repartidores_id','repartidores','user_id');
            $crud->set_relation('j8c93d82d.user_id','user','nombre');
            $crud->order_by('fecha_solicitud','DESC');
            //if($crud->getParameters()=='list'){
                $crud->field_type('pagado','true_false',array('0'=>'NO','1'=>'SI'));
            /*}else{
                $crud->field_type('pagado','hidden');
            } */           
            $crud->columns('id','s0d0e1b62','sdfe80649','status','precio','fecha_solicitud','turno','fecha','fecha_entregado','pagado');
            $crud->fields('repartidores_id','clientes_id','status','fecha','fecha_solicitud','fecha_entregado','pagado','tipo_tramite','precio','precio2','distancia','descripcion','tipo_pago_detalle','turno','observacion_operador');
            $crud->add_action('<i class="fa fa-search"></i> Rastrear','',base_url('pedidos/frontend/rastrear').'/');            
            $crud->callback_column('status',function($val,$row){
                switch($val){
                    case '-2':
                        return '<span class="label label-danger">Cancelado por el banco</span>';
                    break; 
                    case '-1': 
                        return '<span class="label label-danger">Cancelado</span>';
                    break;                     
                    case '0':
                        return '<span class="label label-warning">Esperando Pago</span>';
                    break;
                    case '1':
                        return '<span class="label label-warning">Sin Asignar</span>';
                    break;
                    case '2': 
                        return '<span class="label label-default">Mensajero Asignado</span>';                    
                    break;                
                    case '3': 
                        return '<span class="label label-info">Paquete Recogido</span>';
                    break;
                    case '4': 
                        return '<span class="label label-success">En Tránsito</span>';
                    break;
                    case '5': 
                        return '<span class="label label-success">Entregado</span>';
                    break;
                    case '6': 
                        return '<span class="label label-success">Entregado y Firmado</span>';
                    break;
                }
            });
            
            $crud->callback_after_insert(function($post,$primary){
                //Insertar datos
                if(!empty($post['ida_vuelta'])){
                    $_POST['detalles'][] = $_POST['detalles'][0];
                }
                if($post['status']>0){
                    $post['pagado'] = 1;
                }
                get_instance()->querys->guardar_detalles($_POST['detalles'],$primary);
                return $post;
            });
            
            $crud->callback_after_update(function($post,$primary){
                //Insertar datos
                switch($post['status']){
                    case '-2':
                    case '-1':
                    case '0': 
                    case '1':
                    case '2': get_instance()->db->update('pedidos_detalles',array('status'=>0),array('pedidos_id'=>$primary)); break;
                    case '3':
                    case '4':
                        $detalles = get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$primary));
                        if($detalles->num_rows()>0){                            
                            get_instance()->db->update('pedidos_detalles',array('status'=>0),array('pedidos_id'=>$primary));
                            get_instance()->db->update('pedidos_detalles',array('status'=>1),array('id'=>$detalles->row()->id));
                        }
                    break;
                    case '5':
                        get_instance()->db->update('pedidos_detalles',array('status'=>1),array('pedidos_id'=>$primary)); break;
                    break;
                }
            });
            
            $crud->callback_field('repartidores_id',function($val){
                get_instance()->db->select('repartidores.id, user.nombre');
                get_instance()->db->join('user','user.id = repartidores.user_id');
                return form_dropdown_from_query('repartidores_id','repartidores','id','nombre',$val,'id="field-repartidores_id"');
            });
            $crud->callback_field('clientes_id',function($val){
                get_instance()->db->select('clientes.id, user.nombre');
                get_instance()->db->join('user','user.id = clientes.user_id');
                return form_dropdown_from_query('clientes_id','clientes','id','nombre',$val,'id="field-clientes_id"');
            });
            $crud->set_rules('valor_paquete','Valor del Paquete','required|numeric');
            $crud->unset_delete()->unset_read()->unset_export()->unset_print();
            return $crud;
        }
        
        function mispedidos(){
            $this->as = array('mispedidos'=>'pedidos');
            $crud = $this->crud_function('','');
            //$crud->set_theme('bootstrapsinhead');
            $crud->where('clientes_id',$this->db->get_where('clientes',array('user_id'=>$this->user->id))->row()->id);
            $crud->field_type('status','dropdown',array(
                '-2'=>'Cancelado por el banco',
                '-1'=>'Cancelado',
                '0'=>'Esperando Pago',
                '1'=>'Esperando Pago',
                '2'=>'Mensajero Asignado',
                '3'=>'Paquete Recogido',
                '4'=>'En Tránsito',
                '5'=>'Entregado',
                '6'=>'Entregado y firmado'
            ))
                ->field_type('fecha_solicitud','input');
            $crud->display_as('s0d0e1b62','Cliente')
                 ->display_as('sdfe80649','Repartidor')
                 ->display_as('id','#Pedido')
                 ->display_as('fecha_solicitud','Fecha')
                 ->display_as('status','Estado del pedido')
                 ->display_as('observacion_operador','Observación');
                 
            $crud->set_relation('clientes_id','clientes','user_id')
                 ->set_relation('j3eb7f57f.user_id','user','nombre');
            
            $crud->set_relation('repartidores_id','repartidores','user_id');
            $crud->set_relation('j8c93d82d.user_id','user','nombre');            
            $crud->columns('fecha_solicitud','id','sdfe80649','status','precio','descripcion','observacion_operador');
            $crud->add_action('<i class="fa fa-search"></i> Rastrear','',base_url('pedidos/frontend/rastrear').'/');            
            $crud->callback_column('fecha_solicitud',function($val){
               return date("d-m-Y",strtotime($val)); 
            });
            $crud->callback_column('status',function($val,$row){
                switch($val){
                    case '-2':
                        return '<span class="label label-danger">Cancelado por el banco</span>';
                    break; 
                    case '-1': 
                        return '<span class="label label-danger">Cancelado</span>';
                    break;    
                    case '0':
                        return '<span class="label label-warning">Esperando Pago</span>';
                    break;
                    case '1':
                        return '<span class="label label-warning">Esperando Pago</span>';
                    break;
                    case '2': 
                        return '<span class="label label-default">Mensajero Asignado</span>';                    
                    break;                
                    case '3': 
                        return '<span class="label label-info">Paquete Recogido</span>';
                    break;
                    case '4': 
                        return '<span class="label label-success">En Tránsito</span>';
                    break;
                    case '5': 
                        return '<span class="label label-success">Entregado</span>';
                    break;
                    case '6': 
                        return '<span class="label label-success">Entregado y Firmado</span>';
                    break;
                }
            });
            
            $crud->callback_after_insert(function($post,$primary){
                //Insertar datos
                if(!empty($post['ida_vuelta'])){
                    $_POST['detalles'][] = $_POST['detalles'][0];
                }
                get_instance()->querys->guardar_detalles($_POST['detalles'],$primary);
            });
            $crud->callback_field('repartidores_id',function($val){
                get_instance()->db->select('repartidores.id, user.nombre');
                get_instance()->db->join('user','user.id = repartidores.user_id');
                return form_dropdown_from_query('repartidores_id','repartidores','id','nombre',$val,'id="field-repartidores_id"');
            });
            $crud->callback_field('clientes_id',function($val){
                get_instance()->db->select('clientes.id, user.nombre');
                get_instance()->db->join('user','user.id = clientes.user_id');
                return form_dropdown_from_query('clientes_id','clientes','id','nombre',$val,'id="field-clientes_id"');
            });
            $crud->unset_delete()->unset_read()->unset_add()->unset_export()->unset_edit()->unset_print()->unset_jquery();
            $out = $crud->render();
            $out->output = $out->output;
            $out->scripts = get_header_crud($out->css_files, $out->js_files,TRUE);
            $out->title = 'Mis Servicios';
            $this->loadView($out);
        }
        
        function procesar_pendiente(){
            $cliente = $this->db->get_where('clientes',array('user_id'=>$this->user->id));
            if($cliente->num_rows()>0){
                if(!empty($_SESSION['pedido'])){
                    $post = $_SESSION['pedido'];                    
                    $data = array();
                    $detalle = array();
                    foreach($post as $n=>$p){
                        if(!is_array($p)){
                            $data[$n] = $p;
                        }else{
                            $detalle = $p;
                        }
                    }
                    $data['fecha_solicitud'] = date('Y-m-d H:i:s',strtotime($data['fecha_solicitud']));
                    $data['clientes_id'] = $cliente->row()->id;
                    $this->db->insert('pedidos',$data);
                    if($data['tipo_pago']==1){
                        $_SESSION['irapayphone'] = 1;
                    }//Traguear pago a payphone una vez llegada
                    
                    $primary = $this->db->insert_id();
                    if(!empty($data['ida_vuelta'])){
                        $detalle[] = $detalle[0];
                    }
                    $this->querys->guardar_detalles($detalle, $primary);
                }
                unset($_SESSION['pedido']);
                header("Location:".base_url('pedidos/admin/rastrear/'.$primary));
            }else{
                throw new Exception('Usted no es cliente registrado',404);
            }
        }
        
        function pagar($id,$idc = ''){
            if(is_numeric($id)){
                $pedido = $this->db->get_where('pedidos',array('id'=>$id));
                if($pedido->num_rows()>0 && ($pedido->row()->status==0 || $pedido->row()->status==1)){
                    $this->load->view('pagar',array('id'=>$id,'pedido'=>$pedido));                    
                }else{
                    header("Location:".base_url('pedidos/admin/pedidos'));
                }
            }else{
                switch($id){
                    case 'payphone':
                        if(is_numeric($idc)){
                            $pedido = $this->db->get_where('pedidos',array('id'=>$idc,'status <'=>1));
                            if($pedido->num_rows()>0){
                                $this->loadView(array('view'=>'_payphone','id'=>$idc,'pedido'=>$pedido));
                            }else{
                                header("Location:".base_url('pedidos/admin/pedidos'));
                            }
                        }
                    break;
                    default:
                        header("Location:".base_url('pedidos/admin/pedidos'));
                    break;
                }                
            }
        }
        
        function rastrear($id){
            if(is_numeric($id)){
                header("Location:".base_url('pedidos/frontend/rastrear/'.$id));
            }else{
                header("Location:".base_url('pedidos/admin/pedidos'));
            }
        }
        
        function pagos(){
            $crud = $this->crud_function('','');  
            $crud->callback_before_insert(function($post){
                $post['descripcion'] = '';
                foreach($post as $n=>$p){
                    $post['descripcion'] .= '<p><b>'.$n.'</b>: '.$p.'</p>';
                }
                return $post;
            });
            $crud->callback_after_insert(function($post){
                if(!empty($post['credito']) || !empty($post['efectivo'])){
                    get_instance()->db->update('pedidos',array('status'=>2),array('id'=>$post['pedidos_id']));
                }
            });
            $crud->unset_back_to_list();
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        
        function favoritos(){            
                $crud = new ajax_grocery_CRUD();            
                $crud->set_theme('crud_dependency');
                $crud->set_subject('<i class="fa fa-heart"></i> Mis favoritos');
                $crud->set_table('favoritos');
                $crud->where('user_id',$this->user->id);
                $crud->unset_read()
                     ->unset_export()
                     ->unset_print();
                $crud->norequireds = array('direccion2','numeracion');
                $crud->required_fields_array();                
                $crud = $crud->render();
                $this->loadView($crud);
        }
        
        function cupones(){
            $crud = $this->crud_function('','');                        
            $crud->unset_read()
                 ->unset_export()
                 ->unset_print();
            $crud->display_as('porcentaje','Porcentaje de descuento');
            $crud->field_type('nro','hidden');
            $crud->callback_after_insert(function($post,$primary){
                $post['nro'] = substr(md5($primary),0,8);
                get_instance()->db->update('cupones',array('nro'=>$post['nro']),array('id'=>$primary));
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function asignacion_manual($id){
            $this->load->model('querys');
            $this->querys->asignar_repartidor($id);
        }
        
        function notificar($id = ''){
            if(is_numeric($id)){
                $this->db->select('pedidos.*, user.celular');
                $this->db->join('clientes','clientes.id = pedidos.clientes_id');
                $this->db->join('user','clientes.user_id = user.id');
                $pedido = $this->db->get_where('pedidos',array('pedidos.id'=>$id));
                if($pedido->num_rows()>0){
                    //Notificar
                    $this->load->library('textoatodos');
                    $this->textoatodos->db = $this->db;                    
                    $this->textoatodos->notificar($pedido->row());
                    
                    $this->load->library('gcm_library');
                    $this->gcm_library->db = $this->db;
                    $this->gcm_library->notificar($pedido->row(),'clientes');
                    echo 'Mensaje enviado con éxito';                 
                }
            }
        }
        
        function solicitudes(){            
            $clientes_id = isset($this->user->clientes_id)?$this->user->clientes_id:0;
            $this->loadView(array('view'=>'back_solicitud','clientes_id'=>$clientes_id,'title'=>'Solicitar Servicio'));
        }
    }
?>
