<section>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Estado del Servicio
                </div>
                <div class="panel-body" style="position:relative;">
                    <div class="hidden-xs" style="position:absolute; top:40px; left:0px; width:100%; height:3px; background: lightgray"></div>
                    <ul class="rastreo" style="text-align:center;">
                        <?php $this->load->view('_grafico_rastreo',array('pedido'=>$pedido)); ?>        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Detalles del servicio
                </div>
                <div class="panel-body">
                    <div class="panel panel-default">
                            <div class="panel-heading">
                                <div>Datos principales</div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="ciudades_id">Ciudad:</label>
                                    <div class="form-control"><?= $pedido->ciudad->ciudades_nombre ?></div>
                                </div>
                                <div class="form-group">
                                    <label for="ciudades_id">Fecha Servicio:</label>
                                    <div class="form-control"><?= date("d-m-Y H:i:s",strtotime($pedido->fecha_solicitud)) ?></div>
                                </div>
                                <div class="form-group">
                                    <label for="ciudades_id">Tipo Servicio:</label>
                                    <div class="form-control"><?= $pedido->tipos_servicios->tipos_servicios_nombre ?></div>
                                </div>
                                <?php if($pedido->tipo_tramite==2): ?>
                                <div class="form-group">
                                    <label for="ciudades_id">Tiempo aproximado a la llegada del mensajero:</label>
                                    <div class="form-control">59 Minutos</div>
                                </div>
                                <?php endif ?>
                                <div class="form-group">
                                    <label for="ciudades_id">Nro Guía:</label>
                                    <div class="form-control"><?= $pedido->guia ?></div>
                                    <div class="alert alert-success">
                                        Comparte este código si quieres que otra persona haga seguimiento a este servicio. <a href="<?= base_url('pedidos/frontend/rastrear/'.$pedido->id) ?>"><?= base_url('pedidos/frontend/rastrear/'.$pedido->id) ?></a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ciudades_id">Distancia:</label>
                                    <div class="form-control"><?= $pedido->distancia ?> Kilometros</div>
                                </div>
                                <div class="form-group">
                                    <label for="ciudades_id">Direcciones:</label>
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <?php foreach($pedido->direcciones->result() as $d): 
                                        $d->direccion2 = !empty($d->direccion2)?'& '.$d->direccion2:$d->direccion2;
                                    ?>                                                                            
                                        <div class="panel panel-default">
                                          <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $d->id ?>" aria-expanded="true" aria-controls="collapse<?= $d->id ?>">
                                                  <i class="fa fa-arrow-down"></i> <?= $d->direccion1.' '.$d->direccion2 ?>
                                              </a>
                                            </h4>
                                          </div>
                                          <div id="collapse<?= $d->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $d->id ?>">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <?php if(!empty($d->referencia)): ?><div class="col-xs-6">Referencia: <?= $d->referencia ?> </div><?php endif ?>
                                                    <?php if(!empty($d->numeracion)): ?><div class="col-xs-6">Numeración: <?= $d->numeracion ?></div><?php endif ?>
                                                    <?php if(!empty($d->contacto)): ?><div class="col-xs-6">Contacto: <?= $d->contacto ?></div><?php endif ?>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                        
                                        <?php endforeach ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ciudades_id">Descripción:</label>
                                    <div class="form-control"><?= $pedido->descripcion ?></div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Información de entrega
                </div>
                <div class="panel-body" id="mensajeroinfo">                    
                        <?php $this->load->view('_grafico_repartidor',array('pedido'=>$pedido)); ?>                                        
                </div>
            </div>
            <?php if($pedido->status<5): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Ubicación del repartidor
                            <span class="pull-right favclick mensajeroSearch" title="Ubicar al mensajero" style=""><i class="fa fa-motorcycle"></i></span>
                        </div>
                        <div class="panel-body" style="padding:0px">
                            <div id="map" style="width:100%; height:400px;"></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Chatea con el mensajero
                        </div>
                        <div class="panel-body">
                            <?php if(!empty($_SESSION['user'])): ?>
                            <form class="form-inline" onsubmit="return sendMessage();">
                                <div class="mensajes" style="height:200px; border:1px solid black; width:100%; overflow-y:auto">                            
                                    <ul></ul>
                                </div>
                                <div style="width: 87%;" class="form-group">
                                  <label for="text" class="sr-only">Mensaje</label>
                                  <input type="text" class="form-control" id="field-mensaje" placeholder="Escribe aqui tu pregunta" style="width:100%">
                                </div>
                                <button type="submit" class="btn btn-default">Enviar</button>
                            </form>                    
                            <?php else: ?>
                            <a class='btn btn-success btn-block' href="<?= base_url('registro/index/add') ?>?redirect=<?= base_url('pedidos/admin/rastrear/'.$pedido->id) ?>">Iniciar Sesión</a>
                            <?php endif ?>
                        </div>
                    </div>
            <?php endif ?>
        </div>
    </div>
    <script src="<?= base_url('js/group.js') ?>"></script>
    <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBAmpz-tft5eyIVdHRZrXcEhj4ykyaC80E&libraries=drawing,places"></script>
    <script>
        var base_url = '<?= base_url() ?>';
        var map = undefined;
        var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
        var grupo = new Group();
        var repartidor = new Direccion();
        function init_map(){
                map = new google.maps.Map(document.getElementById('map'), {
                    zoom: <?= $pedido->ciudad->zoom ?>,
                    center: new google.maps.LatLng<?= $pedido->ciudad->centro ?>,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                
                <?php foreach($pedido->direcciones->result() as $d): ?>
                    dir = new Direccion();
                    dir.id = <?= $d->id ?>;
                    dir.setMark(<?= $d->lat ?>,<?= $d->lon ?>);                    
                    grupo.add(dir);
                <?php endforeach ?>
                <?php if($pedido->status>1): ?>
                    //repartidor.icon = '';
                    var repartidorData = <?= json_encode($pedido->repartidor->row()) ?>;
                    repartidor.id = -4;
                    repartidor.title = 'Mensajero <?= $pedido->repartidor->row()->nombre ?>';
                    repartidor.icon = '<?= base_url('img/repartidormark.png') ?>';                    
                    console.log(repartidorData);
                    if(repartidorData.lat!=null && repartidorData.lon!=null){
                        repartidor.setMark(repartidorData.lat,repartidorData.lon);
                    }
                    updateMensajero();
                <?php endif ?>
                    //destroy_map();
                    //ruta();
                    console.log(grupo.list);
        }
        
        function destroy_map(){
            map = undefined;
            $("#map").html('');
            setTimeout(updateStatus,2000);
        }
        var enfoqued = false;
        function updateMensajero(){
            $.post('<?= base_url('repartidores/frontend/mensajeros/json_list') ?>',{'search_field[]':'user_id','search_text[]':<?= $pedido->repartidor->row()->id ?>},function(data){
                data = JSON.parse(data);
                data = data[0];                
                repartidor.setMark(data.lat,data.lon);
                if(map!== undefined && !enfoqued){
                    enfoqued = true;
                    map.panTo(new google.maps.LatLng(data.lat,data.lon));
                    map.setZoom(16);
                    console.log(map);
                }
                updateStatus();
            });
        }
        
        function moverMapa(data){
            var bounds = new google.maps.LatLngBounds();
            for(var i in data){
                 bounds.extend(new google.maps.LatLng(data[i].lat,data[i].lon));
            }
            //console.log(data);
            map.fitBounds(bounds);
        }
        
        <?php if($pedido->status>=2): ?>
            $(document).on('ready',function(){                      
               init_map();
            });
        <?php else: ?>
            updateStatus();
        <?php endif ?>
        $('.mensajeroSearch').click(function(){
            if(map!==undefined){
                enfoqued = false;
            }
        });
        function updateStatus(){
            $.post('<?= base_url('pedidos/frontend/rastreame/'.$pedido->id) ?>',{},function(data){
                data = JSON.parse(data);                
                $(".rastreo").html(data.grafico);
                $("#mensajeroinfo").html(data.mensajero);
                if(map!==undefined){
                    setTimeout(updateMensajero,2000);
                }else{
                    setTimeout(updateStatus,2000);
                }
                if(map===undefined && parseInt(data.pedido.status)>=2){
                    init_map();
                }
                if(map!==undefined && parseInt(data.pedido.status)<2){
                    destroy_map();
                }
            });
        }                
        
        function cargar_pago(){
            $.post('<?= base_url('pedidos/admin/pagar/'.$pedido->id) ?>/',{pedidos_id:<?= $pedido->id ?>},function(data){
                emergente(data);
            });
        }

        
        
        <?php if(!empty($_SESSION['user'])): ?>
            function getMessages(){
                $.post('<?= base_url('chat/getMessages') ?>',{user_id:<?= $_SESSION['user'] ?>,destino:1,cliente:1,pedido:<?= $pedido->id ?>},function(data){
                    var mensajes = JSON.parse(data);
                    $(".mensajes ul").html('');
                    for(var i in mensajes){
                        var clase = parseInt(mensajes[i].user_id)!==parseInt(<?= $this->user->id ?>)?'class="active"':'';
                        $(".mensajes ul").append('<li '+clase+'>'+mensajes[i].mensaje+'</li>');
                    }
                    setTimeout(getMessages,10000);
                });
            }

            function sendMessage(retorno){
                var mensaje = $("#field-mensaje").val();
                $("#field-mensaje").val('');
                $.post('<?= base_url('api/sendMessage') ?>',{user_id:<?= $_SESSION['user'] ?>,destino:1,mensaje:mensaje,cliente:1,pedido:<?= $pedido->id ?>},function(data){
                    getMessages();
                });            
                return false;
            }
            getMessages();                        
        <?php endif ?>        
    </script>
</section>