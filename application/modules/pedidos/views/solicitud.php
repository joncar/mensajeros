<form id="crudForm2" onsubmit="return enviardatos()">    
    <div class="row">
        <div class="col-xs-12 col-sm-6" style="max-height:800px; overflow: auto; overflow-x: hidden">
            <div class="row">
                <div id="general" class="col-xs-12 col-sm-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="tipodesc">
                             Datos principales
                            <?php foreach($this->db->get('tipos_servicios')->result() as $t): ?>
                             <span style="<?= $t->id!=1?'display:none':'' ?>" class="pull-right help t<?= $t->id ?>" data-content="<?= $t->descripcion ?>" title="<?= ucfirst($t->tipos_servicios_nombre) ?>"><i class="fa fa-question-circle"></i></span>
                            <?php endforeach ?>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="ciudades_id">Tipo Servicio:</label>
                                <div class="row tipo">                                    
                                    <a id="tipo2" href="javascript:tipo(2)"><div class="col-xs-6"><i class="fa fa-clock-o"></i> <br/> EXPRESS</div></a>
                                    <a id="tipo1" href="javascript:tipo(1)" class="active"><div class="col-xs-6"><i class="fa fa-cubes"></i> <br/> ESTANDAR</div></a>                                    
                                </div>
                            </div>
                            <div class="tipodesc">    
                                <?php foreach($this->db->get('tipos_servicios')->result() as $t): ?>
                                 <div style="<?= $t->id!=1?'display:none':'' ?>" class="help t<?= $t->id ?>"><?= $t->descripcion ?></div>
                                <?php endforeach ?>
                           </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6" style="display:none">
                                    <label for="ciudades_id">Ciudad:</label>
                                    <select id="field-ciudades_id" class="form-control chosen-select" name="ciudades_id">                                
                                        <?php foreach($this->db->get('ciudades')->result() as $c): ?>
                                        <option value="<?= $c->id ?>" data-json='<?= $c->json_limitacion ?>' data-json2='<?= $c->json_limitacion2 ?>'" data-center="<?= $c->centro ?>" data-zoom="<?= $c->zoom ?>" data-zoom2="<?= $c->zoom2 ?>"><?= $c->ciudades_nombre ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <label for="ciudades_id">Fecha Servicio:</label>
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' class="form-control" value="<?= date("d-m-Y H:i:s") ?>" name="fecha_solicitud" id="field-fecha"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div id='turno'>
                                        <label for="ciudades_id">Hora Entrega:</label>
                                        <div class='row'>
                                            <div class="col-xs-12">
                                                <label>
                                                    <input id="turnoM" type="radio" name="turno" value="Mañana"> Mañana (8:00-13:00)
                                                </label>
                                            </div>
                                            <div class="col-xs-12">
                                                <label>
                                                    <input id="turnoT" type="radio" name="turno" value="Tarde"> Tarde (14:00-18:00)
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 col-sm-10" id="direcciones">
                    <div>
                        <div class='panel panel-primary itinerarioinputs'>
                            <div class='panel-heading'>
                                <i class="fa fa-upload"></i> Dirección de recogida
                                <span style="color:white;" class="pull-right favclick" title="Cargar dirección guardada"><i class="fa fa-heart"></i> <span class="hidden-xs">Cargar dirección anterior</span></span>
                            </div>
                            <div class='panel-body'>
                                <div class="col-xs-12">
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <input type="text" name="" value="" data-name='detalles' data-cell='direccion1' class="form-control servLugar" placeholder="Calle principal *">
                                            <div class="input-group-addon"><a href="#" class="marcarposicion" title="Marcar la posición actual"><i class="fa fa-map-marker"></i></a></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <input type="text" name="" value="" data-name='detalles' data-cell='direccion2' class="form-control servCalle" placeholder="Calle secundaria">
                                    </div>
                                    <div class='col-xs-12' style="display:none;">
                                        <input type="text" name="" value="" data-name='detalles' data-cell='numeracion' class="form-control numeracion" placeholder="Numeración">
                                    </div>
                                    <div class="col-xs-6 hidden-xs">
                                        <input type="text" name="" value="" data-name='detalles' data-cell='contacto' class="form-control" placeholder="Nombre de contacto">
                                    </div>
                                    <div class="col-xs-6 hidden-xs">
                                        <input type="text" name="" value="" data-name='detalles' data-cell='referencia' class="form-control" placeholder="Lugar de referencia">
                                        <input type='hidden' name='' data-name='detalles' data-cell='lat' class='lat' value='0'>
                                        <input type='hidden' name='' data-name='detalles' data-cell='lon' class='lon' value='0'>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox" class="confirmar" name="" value="1" data-name='detalles' data-cell='guardar'> Confirmar dirección
                                            </label>
                                        </div>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='panel panel-primary itinerarioinputs'>
                            <div class='panel-heading'>
                                <i class="fa fa-download"></i> Dirección de entrega
                                <span title="Eliminar dirección" class="pull-right servDel" style="color:#8a6d3b; margin-left:3px; display: none;" href="#"><i class="fa fa-remove"></i></span> 
                                <span style="color:white;" class="pull-right favclick" title="Cargar dirección guardada"><i class="fa fa-heart"></i> <span class="hidden-xs">Cargar dirección anterior</span></span>
                            </div>
                            <div class='panel-body'>
                                <div class="col-xs-12">
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <input type="text" name="" value="" data-name='detalles' data-cell='direccion1' class="form-control servLugar" placeholder="Calle principal *">
                                            <div class="input-group-addon"><a href="#" class="marcarposicion" title="Marcar la posición actual"><i class="fa fa-map-marker"></i></a></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <input type="text" name="" value="" data-name='detalles' data-cell='direccion2' class="form-control servCalle" placeholder="Calle secundaria">
                                    </div>
                                    <div class='col-xs-12' style="display:none;">
                                        <input type="text" name="" value="" data-name='detalles' data-cell='numeracion' class="form-control numeracion" placeholder="Numeración">
                                    </div>
                                    <div class="col-xs-6 hidden-xs">
                                        <input type="text" name="" value="" data-name='detalles' data-cell='contacto' class="form-control" placeholder="Nombre de contacto">
                                    </div>
                                    <div class="col-xs-6 hidden-xs">
                                        <input type="text" name="" value="" data-name='detalles' data-cell='referencia' class="form-control" placeholder="Lugar de referencia">
                                        <input type='hidden' name='' data-name='detalles' data-cell='lat' class='lat' value='0'>
                                        <input type='hidden' name='' data-name='detalles' data-cell='lon' class='lon' value='0'>
                                    </div>                                    
                                    <div class="col-xs-12">
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox" class="confirmar" name="" value="1" data-name='detalles' data-cell='guardar'> Confirmar dirección
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="position:relative; margin-bottom: 40px;">
                        <div style="position:absolute; left:0px; top:-15px;">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="idavuelta" name="ida_vuelta" value="1"> Ida y vuelta
                                </label>
                            </div>
                        </div>
                        <div id="addDir" style="position:absolute; right:0px;">
                            <a href="javascript:addRow()">Añadir Dirección</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ciudades_id">Valor declarado del paquete:</label>
                        <input type="number" class="form-control" name="valor_paquete" id="field-valor">
                    </div>
                    <div class="form-group">
                        <label for="ciudades_id">Descripción del servicio: *</label>
                        <textarea type="number" class="form-control" name="descripcion" id="field-descripcion"></textarea>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="row" id="mapa">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Mapa de actuación                        
                    </div>
                    <div class="panel-body" style="padding:0px">
                        <div id="map" style="width:100%; height:400px;"></div>
                    </div>                
                </div>
            </div>
            <div class="row" id="costo">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div>Detalle de costos</div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="ciudades_id">kilómetros a recorrer: *</label>
                                        <div class="form-control" id="kmlabel">0</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ciudades_id">Precio: *</label>
                                        <input type="number" class="form-control" name="precio" id="field-precio" readonly="" value="0">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group" id="kmadicional" style="display:none">
                                        <label for="ciudades_id">Costo de Kilométros adicionales:</label>
                                        <input type="number" class="form-control" name="precio2" id="field-precio2" readonly="" value="0">
                                    </div>
                                    <div class="form-group">
                                        <label for="ciudades_id">Tienes un cupón:</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="cupon" id="field-cupon">
                                            <div class="input-group-addon"><a href="#" class="marcarposicion" title="Aplicar Cupón"><i class="fa fa-check"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="ciudades_id">Tipo de pago: *</label>
                                        <?php if($_SESSION['convenio']==0): ?>
                                            <?= form_dropdown('tipo_pago',array('1'=>'Tarjeta de crédito','2'=>'Depósito o Transferencia Bancaria','3'=>'Pago en efectivo al mensajero'),'1','id="field-tipo_pago" class="form-control"') ?>
                                        <?php else: ?>
                                            <input type="hidden" name="tipo_pago" value="5">
                                            <div class="form-control">Convenio empresa</div>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>                            
                            <div class="form-group" style="display: none" id="dondeseraelpago">
                                <label for="ciudades_id">Seleccionar la dirección.: *</label>
                                <select class="form-control" id="field-efectivo_donde"></select>                                
                            </div>                            
                            <div class="form-group">
                                <input type="hidden" name="status" id='field-status' value='<?= $_SESSION['convenio']==0?0:'2' ?>'>
                                <?php if(!empty($clientes_id)): ?>
                                    <input type="hidden" name="clientes_id" value='<?= $clientes_id ?>'>
                                <?php endif ?>
                                <input type="hidden" name="tipo_tramite" id="field-tipo_tramite" value='1'>
                                <input type="hidden" name="distancia" id="field-distancia" value='0'>
                                <input type="hidden" name="fecha" id="field-fecha" value='<?= date("Y-m-d H:i:s"); ?>'>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" id="aceptar"> <a href="<?= base_url('files/'.$this->db->get_where('ajustes')->row()->politicas) ?>">Acepto los terminos y condiciones de uso</a>
                                    </label>
                                </div>
                                <button type="submit" id="guardar" class="btn btn-success btn-block" disabled="">Solicitar servicio</button>
                                <div id='message' style="display:none; text-align: center;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id='depositoHTML' style="display:none">
        <?= $this->db->get('ajustes')->row()->cuentas_bancarias ?>
    </div>
    <div id='dineroHTML' style="display:none">
        <?= $this->db->get('ajustes')->row()->dinero_electronico ?>
    </div>
</form>
<div class="visible-xs pestanas">
    <div class="row">
        <a href="#general"><div class="col-xs-3 well"><i class="fa fa-home"></i> <br/>Inicio</div></a>
        <a href="#direcciones"><div class="col-xs-3 well"><i class="fa fa-location-arrow"></i> <br/>Ubicación</div></a>
        <a href="#mapa"><div class="col-xs-3 well"><i class="fa fa-map-marker"></i> <br/>Mapa</div></a>
        <a href="#costo"><div class="col-xs-3 well"><i class="fa fa-money"></i> <br/>Precio</div></a>
    </div>
</div>
<?= $this->load->view('predesign/bootstrapdatepicker') ?>
<script src="<?= base_url('js/group.js') ?>"></script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBAmpz-tft5eyIVdHRZrXcEhj4ykyaC80E&libraries=drawing,places"></script>
<script>
    var precios = [<?php 
        foreach($this->db->get('tipos_servicios')->result() as $t){
            $porcentaje = array();
            foreach($this->db->get_where('porcentaje_km_extra',array('tipos_servicios_id'=>$t->id))->result() as $p){
                $porcentaje[] = $p;
            }
            echo '{precio:'.$t->precio.',precio_fuera:'.$t->precio_fuera.',porcentaje:'.json_encode($porcentaje).',limite:'.$t->limite.'},';
        }
    ?>];
    var js_date_format = 'dd-mm-yy H:i:s';
    var validation_url = '<?= base_url('pedidos/admin/pedidos/insert_validation') ?>';
    var map = '';
    var limitacion1 = [];
    var limitacion2 = [];
    var direcciones = [];
    var grupo = [];
    var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
    var init = false;
    var limitehorahoy = [];        
    var limitehoranormal = [];
    var minDate = moment("<?= date("H")<13?date("m/d/Y"):date("m/d/Y",strtotime('+1 days '.date("Y-m-d"))) ?>");
    $(document).on('ready',function(){
        $('.help').popover({            
            toggle:"popover",
            container:"body",
            placement:"top",            
        });
        //Hora normal
        for(var i=8;i<18;i++){
            if(i!==13){
                limitehoranormal.push(i);
            }
        }
        //Hora hoy
        var f = new Date();
        for(var i=f.getHours();i<18;i++){
            if(i!==13){
                limitehorahoy.push(i);
            }
        }
        <?php ?>        
        $('#datetimepicker1').datetimepicker({
            defaultDate: "<?= date("H")<13?date("m/d/Y"):date("m/d/Y",strtotime('+1 days '.date("Y-m-d"))) ?>",
            minDate:moment("<?= date("H")<13?date("m/d/Y"):date("m/d/Y",strtotime('+1 days '.date("Y-m-d"))) ?>"),
            enabledHours: limitehoranormal,
            format: 'DD-MM-YYYY',
            locale: 'es'
        });
        
        <?php 
            $dia = date("H")<13?date("d"):date("d",strtotime('+1 days '.date("Y-m-d")));
            if($dia==date("d")):
        ?>
            $("#turnoM").attr('disabled',true);
            $('#datetimepicker1').data('DateTimePicker').enabledHours(limitehorahoy);
        <?php endif ?>
        
        $('#datetimepicker1').on('dp.change',function(e){
            validar_turno(e);
        });
        
        init_map();
        $("#field-ciudades_id").change(function(){
           refreshMap();
        });
        
        var tag = '';
        $(document).on('mouseover','.servNumberDel',function(){
            $(this).addClass('servDel').removeClass('.servNumber');
            tag = $(this).html();
            $(this).html('<i class="fa fa-remove"></i>');
        });
        
        $(document).on('mouseout','.servNumberDel',function(){
            $(this).addClass('servNumber').removeClass('servDel');            
            $(this).html(tag);
        });
        
        $(document).on('change','.servLugar, .servCalle',function(){
            var el = $(this).parents('.itinerarioinputs').data('id');            
            el = grupo.getFromId(el);
            if(el!==null){                
                el.fillDataFromText();                
            }
        });
        
        $(document).on('change',".itinerarioinputs input[type='text']",function(){            
           $(this).parents('.itinerarioinputs').find("input[type='checkbox']").prop('checked',false); 
        });
        
        $(document).on('click','#idavuelta',function(){
            if($(this).prop('checked')){
                $("#addDir").hide();
            }else{
                $("#addDir").show();
            }
            sumar();
        });
        
        $(document).on('click','.confirmar',function(){
            var confirmado = true;
            for(var i in grupo.list){
                if(!grupo.list[i].confirmado()){
                    confirmado = false;
                }
            }
            if(confirmado){
                ruta();
                sumar();
            }
        });
        
        $(document).on('click','#aceptar',function(){            
           habilitar($(this).prop('checked')); 
        });
        
        $(document).on('change','#field-descripcion',function(){
            sumar();
        });
        
        $(document).on('change','#field-tipo_pago',function(){
            switch($(this).val()){                                    
                case '1': //Tarjeta de credito
                    $("#field-status").val(0);                    
                    $("#dondeseraelpago").hide();
                    $("#field-efectivo_donde").html('');
                break;
                case '2': //Deposito
                    $("#field-status").val(0);
                    $("#dondeseraelpago").hide();
                    $("#field-efectivo_donde").html('');
                break;
                case '4': //Dinero electr
                    $("#field-status").val(0);
                    $("#dondeseraelpago").hide();
                    $("#field-efectivo_donde").html('');
                break;
                case '3': //Efectivo
                    $("#field-status").val(2);
                    $("#field-efectivo_donde").html('');
                    for(var i in grupo.list){
                        var dir = grupo.list[i].id===0?'Dirección de Recogida':'Dirección de entrega #'+(grupo.list[i].id)
                        $("#field-efectivo_donde").append('<option value="'+grupo.list[i].id+'">'+dir+'</option>');
                    }
                    $("#dondeseraelpago").show();
                break;
            }
        });
    });
    
    function validar_turno(e){
        var picker = e.date._d;
        var fecha = new Date();
        console.log(fecha.getDate()+'==='+picker.getDate());
        if(
                fecha.getDate()===picker.getDate() && 
                fecha.getMonth()===picker.getMonth() &&
                fecha.getFullYear()===picker.getFullYear()
        ){
            $("#turnoM").attr('disabled',true);
            if(fecha.getHours()>13){
                $("#turnoT").attr('disabled',true);
            }
            $('#datetimepicker1').data('DateTimePicker').enabledHours(limitehorahoy);
        }else{
            $("#turnoM").attr('disabled',false);
            $("#turnoT").attr('disabled',false);
            $('#datetimepicker1').data('DateTimePicker').enabledHours(limitehoranormal);
        }
    }
    //Iniciales
    function init_map(){
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: {lat: -0.1862504, lng: -78.5709685},
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            typeControl:"off",
            navigationControl:"DEFAULT",
            infoWindowMaxWidth:"300",
            streetViewControl:true,
            viewOnMapScroll:false
        });                
        google.maps.event.addListener(map,"click",function(e){
            vacio = grupo.buscarVacio();
            if(vacio!==null){
                if(vacio.puedemarcar(e.latLng.lat(),e.latLng.lng())){
                    vacio.setMark(e.latLng.lat(),e.latLng.lng());
                    vacio.buscarDireccion();
                    sumar();
                }
            }
        });    
        refreshMap();
        dir = new Direccion();
        dir.id = 0;
        dir.dom = $($(document).find('.itinerarioinputs')[0]);
        grupo = new Group();
        grupo.agregar(dir);
        
        dir = new Direccion();
        dir.id = 1;
        dir.dom = $($(document).find('.itinerarioinputs')[1]);        
        grupo.agregar(dir);
        console.log(grupo);
        
        limitacion1 = $("#field-ciudades_id option:selected").data('json');
        limitacion2 = $("#field-ciudades_id option:selected").data('json2');

        limitacion1 = new google.maps.Polygon({paths: limitacion1});
        limitacion2 = new google.maps.Polygon({paths: limitacion2});
    }
    
    function refreshMap(){
        if($("#field-ciudades_id").val()!==''){
            limitacion1 = $("#field-ciudades_id option:selected").data('json');
            limitacion2 = $("#field-ciudades_id option:selected").data('json2');
            centro = $("#field-ciudades_id option:selected").data('center');
            zoom = $("#field-ciudades_id option:selected").data('zoom2');
            limitacion1 = new google.maps.Polygon({paths: limitacion1});
            limitacion2 = new google.maps.Polygon({paths: limitacion2});
            centro = centro.replace('(','');
            centro = centro.replace(')','');
            centro = centro.split(',');
            map.panTo(new google.maps.LatLng(centro[0],centro[1]));
            map.setZoom(zoom);

        }
    }
    
    //Controles
    function addRow(fill,lat,lng){
        var id = grupo.list.length;
        var dir = new Direccion();
        dir.id = id;
        dir.draw();
        grupo.agregar(dir);
    }
    
    $(document).on('click','.servDel',function(){
       var id = $(this).parents('.itinerarioinputs').data('id');
       grupo.borrar(id);
       sumar();
    });
    
    $(document).on('click','.marcarposicion',function(e){
        e.preventDefault();
        var id = $(this).parents('.itinerarioinputs').data('id');
        var dir = grupo.getFromId(id);
        if(dir!==null){
            dir.setActualPosition();
        }
    });
    
    $(document).on('click','.favclick',function(){
        var id = $(this).parents('.itinerarioinputs').data('id');
        $.post('<?= base_url('pedidos/frontend/favoritos') ?>?id='+id,{},function(data){
            emergente(data);
        });
    });
    
    $(document).on('change','#field-prioridad',function(){
       sumar(); 
    });
    
    function setFavorito(id,json){
        var obj = grupo.getFromId(id);
        if(obj!==null){
            obj.setOptions(json);
        }
    }
    
    function sumar(){
        if(grupo.list.length>1){
            var markers = grupo.list;            
            destinations = [];
            for(var i=1;i<markers.length;i++){
                if(markers[i].lat!==undefined && markers[i].lng!==undefined){
                    if(markers[i].confirmado()){
                        destinations.push(new google.maps.LatLng(markers[i].lat,markers[i].lng));
                    }
                }
            }                        
            if(markers[0].confirmado() && destinations.length>0){
                var service = new google.maps.DistanceMatrixService;
                service.getDistanceMatrix({
                  origins: [new google.maps.LatLng(markers[0].lat,markers[0].lng)],
                  destinations: destinations,
                  travelMode: google.maps.TravelMode.DRIVING,
                  unitSystem: google.maps.UnitSystem.METRIC,
                  avoidHighways: false,
                  avoidTolls: false
                }, function(response, status) {
                  if (status !== google.maps.DistanceMatrixStatus.OK) {
                    alert('Error was: ' + status);
                  } else {                
                    var distance = 0;
                    if(response.rows.length>0){                        
                        if(response.rows[0].elements.length>0){
                            var el = response.rows[0].elements;                     
                            var precio = precios[parseInt($("#field-tipo_tramite").val())-1];
                            if($("#field-tipo_tramite").val()==='3' && $("#field-prioridad").val()==='1'){
                                precio = precios[3];
                            }
                            var monto = 0;
                            var distancia = 0;
                            var recargo = 0;
                            for(i=0;i<el.length;i++){
                                point = grupo.list[i+1];
                                point.distancia = (el[0].distance.value/1000);
                                distancia+= el[0].distance.value;
                               /* monto += precio.precio*point.distancia;
                                if(!point.limite2 && precio.porcentaje.length>0){//Recargar porcentaje
                                    var porcentajev = 1;                                    
                                    for(var i in precio.porcentaje){
                                        if(point.distancia >= parseFloat(precio.porcentaje[i].desde) && point.distancia <= parseFloat(precio.porcentaje[i].hasta)){
                                            porcentajev = precio.porcentaje[i].porcentaje_recargo;
                                        }
                                    }
                                    porcentaje = point.distancia*(porcentajev/100);
                                    recargo+= porcentaje;
                                    monto+= porcentaje;
                                    console.log(porcentajev);
                                }*/
                                
                                var km = distancia/1000;
                                var km_extra = 0;
                                if(km>=precio.limite){
                                    km_extra = km-precio.limite;
                                    km = precio.limite;
                                }
                                monto = precio.precio+(km_extra*precio.precio_fuera);
                                recargo = km_extra*precio.precio_fuera;
                                
                            }                            
                            if(grupo.list[0].lat!==undefined && grupo.list[0].lng!==undefined && $("#idavuelta").prop('checked')){
                                distancia+= el[0].distance.value;
                            }
                            
                            distancia = distancia/1000;
                            console.log(monto);
                            $("#field-distancia").val(distancia.toFixed(2));
                            $("#kmlabel").html(distancia.toFixed(2));
                            $("#field-precio").val(monto.toFixed(2));
                            $("#field-precio2").val(recargo.toFixed(2));
                            if(recargo===0){
                                $("#kmadicional").hide();
                            }else{                                
                                $("#kmadicional").show();
                            }
                            habilitar(true);
                        }
                    }
                  }
                });
            }
        }
    }        
    
    function tipo(id){
        $(".tipo a").removeClass('active');
        $("#field-tipo_tramite").val(id);
        $(".tipodesc span, .tipodesc div").hide();
        switch(id){
            case 1: //Estandar
                $("#turno").show();
                $("#tipo1").addClass('active');
                $(".t1").show();
                $("#field-prioridad").val('0');
                $(".prioridad").hide();
                $('#datetimepicker1').data('DateTimePicker').format("DD-MM-YYYY");
                $('#datetimepicker1').data('DateTimePicker').minDate(minDate);
            break;
            case 2: //Express
                <?php if(date("H")>=8 && date("H")<18): ?>
                $("#tipo2").addClass('active');
                $(".t2").show();
                $("#turno").hide();
                $("#field-prioridad").val('1');
                $(".prioridad").hide();
                $('#datetimepicker1').data('DateTimePicker').format("DD-MM-YYYY HH:mm");
                $('#datetimepicker1').data('DateTimePicker').date(new Date());
                $('#datetimepicker1').data('DateTimePicker').minDate(moment("<?= date("m/d/Y") ?>"));
                $("#field-fecha").val("<?= date("d-m-Y H:i") ?>");
                <?php else: ?>
                    emergente('El horario de servicio express es de Lunes a Viernes de 08H00 - 18H00');
                    tipo(1);
                <?php endif ?>
            break;
            case 3: //Tramite
                $("#tipo3").addClass('active');
                $(".t3").show();
                $("#turno").hide();
                $("#field-prioridad").val('1');
                $(".prioridad").show();
                $('#datetimepicker1').data('DateTimePicker').format("DD-MM-YYYY HH:mm");
                $('#datetimepicker1').data('DateTimePicker').date(new Date());
                $('#datetimepicker1').data('DateTimePicker').minDate(moment("<?= date("m/d/Y") ?>"));     
                $("#field-fecha").val("<?= date("d-m-Y H:i") ?>");
            break;
        }
        sumar();
    }
    
    function habilitar(habilitar){
        if(habilitar && $("#aceptar").prop('checked') && $("#field-descripcion").val()!==''){
            $("#guardar").attr('disabled',false);
        }else{
            $("#guardar").attr('disabled',true);
        }
    }
    
    //Guardar    
    function enviardatos(){
        var data = document.getElementById('crudForm2');
        var datos = new FormData(data);
        <?php if($_SESSION['convenio']==0): ?>
        var detallepago = $("#field-tipo_pago").val()!=='3'?$("#field-tipo_pago option:selected").html():$("#field-efectivo_donde").val();
        <?php else: ?>
        var detallepago = 'Pago por afiliación';
        <?php endif ?>
        datos.append('tipo_pago_detalle',detallepago);
        $("#guardar").hide();
        $.ajax({
            url:'<?= base_url('pedidos/frontend/pedidos/insert_validation') ?>',
            data:datos,
            type:'post',
            processData:false,
            cache:false,
            contentType: false,
            success:function(data){
                data = data.replace('<textarea>','');
                data = data.replace('</textarea>','');
                data = JSON.parse(data);
                if(data.success){
                    $.ajax({
                        url:'<?= base_url('pedidos/'.$action.'/pedidos/insert') ?>',
                        data:datos,
                        type:'post',
                        processData:false,
                        cache:false,
                        contentType: false,
                        success:function(data){
                            data = data.replace('<textarea>','');
                            data = data.replace('</textarea>','');
                            data = JSON.parse(data);
                            if(data.success){
                                $(".panel").addClass('panel-success').removeClass('.panel-default');                                
                                $("#message").removeClass('alert alert-danger').addClass('alert alert-success');
                                $("#message").html('Sus datos han sido guardados con éxito <a href="<?= base_url('pedidos/admin/rastrear/') ?>/'+data.insert_primary_key+'">Rastrear paquete</a>');
                                $("#message").fadeIn(500);
                                
                                //Redirección
                                switch($("#field-tipo_pago").val()){                                    
                                    case '1':
                                        document.location.href="<?= base_url('pedidos/admin/rastrear') ?>/"+data.insert_primary_key;
                                    break;
                                    case '2':
                                        emergente($("#depositoHTML").html());
                                        $(".modal-footer").html('<a href="<?= base_url('pedidos/admin/rastrear') ?>/'+data.insert_primary_key+'" class="btn btn-danger">Cerrar</a>');
                                    break;
                                    case '3':
                                    case '5':
                                        document.location.href="<?= base_url('pedidos/admin/rastrear') ?>/"+data.insert_primary_key;
                                    break;
                                    case '4':
                                        emergente($("#dineroHTML").html());
                                        $(".modal-footer").html('<a href="<?= base_url('pedidos/admin/rastrear') ?>/'+data.insert_primary_key+'" class="btn btn-danger">Cerrar</a>');
                                    break;
                                    default:
                                        document.location.href="<?= base_url('pedidos/admin/rastrear') ?>/"+data.insert_primary_key;
                                    break;
                                }
                                
                            }else{

                            }
                        }
                    });
                }else{
                    $(".panel").addClass('panel-success').removeClass('.panel-default');
                    $("#guardar").show();
                    $("#message").removeClass('alert alert-success').addClass('alert alert-danger');
                    $("#message").html(data.error_message);
                    $("#message").fadeIn(500);
                }
            }
        });
        return false;
    }
</script>