<section>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Pago recibido
                </div>
                <div class="panel-body" style="position:relative; text-align: center">
                    <p><span style="color:green"><i class="fa fa-check fa-5x"></i></span><br/>Pago recibido, gracias</p>
                    <p><a href="<?= base_url('pedidos/frontend/rastrear/'.$response->ClientTransactionId) ?>" class="btn btn-info">Rastrear Pedido</a></p>
                </div>
            </div>
        </div>
    </div>
</section>