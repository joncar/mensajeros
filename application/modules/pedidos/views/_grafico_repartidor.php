<div class="col-xs-12 col-sm-6">
        <?php if($pedido->status>1): ?>
        <div class="form-group">
            <label for="ciudades_id">Datos del mensajero:</label>
            <div class="row">
                <div class="col-xs-6 col-sm-2"><img class="nav-user-photo" src="<?= base_url(empty($pedido->repartidor->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$pedido->repartidor->foto) ?>"  style="width:100%"/></div>
                <div class="col-xs-6 col-sm-10"><?= $pedido->repartidor->num_rows()>0?$pedido->repartidor->row()->nombre:'Sin asignar' ?></div>
            </div>
        </div>
        <?php elseif($pedido->status==0): ?>
        <div class="form-group">
            <label for="ciudades_id">Información de pago faltante:</label>
            <div class="row">
                <div class="col-xs-12">
                    <a href="javascript:cargar_pago()" class="btn btn-success"><i class="fa fa-credit-card"></i> Cargar datos del pago</a>
                </div>
            </div>
        </div>
        <?php endif ?>

        <div class="form-group">                        
            <?php if($pedido->status<0): ?><div>Servicio Cancelado</div><?php endif ?>
            <?php if($pedido->status==0): ?><div>Por favor complete su pago para poder asignar un mensajero para su servicio requerido</div><?php endif ?>
            <?php if($pedido->status==1): ?><div>Estamos asignando a su mensajero.</div><?php endif ?>
            <?php if($pedido->status==2 && $pedido->tipo_tramite==2): ?><div>El mensajero está dirigiéndose a la dirección de recogida, en menos de 60 minutos llegará y a partir de ese momento ud podrá monitorearlo en tiempo real.</div><?php endif ?>
            <?php if($pedido->status==2 && ($pedido->tipo_tramite==1 || $pedido->tipo_tramite==3)): ?><div>El mensajero asignado cumplirá se dirigirá a la dirección de recogida en los tiempos establecidos a la dirección de entrega, a partir de ese momento ud podrá monitorearlo en tiempo real.</div><?php endif ?>
            <?php if($pedido->status==3 || $pedido->status==4): ?><div>Puede monitorear al mensajero hasta que su paquete llegue a su destino.</div><?php endif ?>
            <?php if($pedido->status==5): ?><div>Su paquete ha sido entregado satisfactoriamente.</div><?php endif ?>
        </div>
</div>
<?php if(!empty($pedido->firma) && $pedido->status>=5): ?>
    <div class="col-xs-12 col-sm-6">
        <img src="<?= base_url('img/firmas/'.$pedido->firma) ?>" style="width:50%"><br/>
        <center>
            <b>Atentamente</b><br/><?= $pedido->firmante ?>
        </center>
    </div>
    <script>
        $("#map").remove();
    </script>
<?php endif ?>