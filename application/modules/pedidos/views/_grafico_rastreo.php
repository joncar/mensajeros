<?php if($pedido->status>=0): ?>
<?php if($pedido->tipo_tramite!=3): ?>
    <li class="<?= $pedido->status!=0?'hidden-xs':'' ?>"><div class="number <?= $pedido->status>=0?'active':'' ?>">1</div> <div class="text"><?= $pedido->status==0?'Esperando Pago':'Sin Asignar' ?></div></li>
    <li class="<?= $pedido->status!=2?'hidden-xs':'' ?>"><div class="number <?= $pedido->status>=2?'active':'' ?>">2</div> <div class="text">Mensajero Asignado</div></li>
    <li class="<?= $pedido->status!=3?'hidden-xs':'' ?>"><div class="number <?= $pedido->status>=3?'active':'' ?>">3</div> <div class="text">Paquete Recogido</div></li>
    <li class="<?= $pedido->status!=4?'hidden-xs':'' ?>"><div class="number <?= $pedido->status>=4?'active':'' ?>">4</div> <div class="text">En Tránsito</div></li>
    <li class="<?= $pedido->status!=5?'hidden-xs':'' ?>"><div class="number <?= $pedido->status>=5?'active':'' ?>">5</div> <div class="text">Entregado</div></li>
<?php else: ?>
    <li class="<?= $pedido->status!=0?'hidden-xs':'' ?>"><div class="number <?= $pedido->status>=0?'active':'' ?>">1</div> <div class="text"><?= $pedido->status==0?'Esperando Pago':'Sin Asignar' ?></div></li>
    <li class="<?= $pedido->status!=2?'hidden-xs':'' ?>"><div class="number <?= $pedido->status>=2?'active':'' ?>">2</div> <div class="text">Mensajero Asignado</div></li>
    <li class="<?= $pedido->status!=3?'hidden-xs':'' ?>"><div class="number <?= $pedido->status>=3?'active':'' ?>">3</div> <div class="text">Trámite en proceso</div></li>                            
    <li class="<?= $pedido->status!=5?'hidden-xs':'' ?>"><div class="number <?= $pedido->status>=5?'active':'' ?>">4</div> <div class="text">Trámite Finalizado</div></li>
<?php endif ?>
<?php else: ?>
<li><div class="number active">5</div> <div class="text">Cancelado</div></li>                        
<?php endif ?>