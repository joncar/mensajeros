<section style="min-height:600px;">
    <div id='message'></div>
    <div class='col-xs-12 col-sm-12'>

      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" <?= $pedido->row()->tipo_pago=='1'?'class="active"':'' ?>><a href="#tarjeta" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-credit-card"></i> Tarjeta de crédito</a></li>
        <li role="presentation" <?= $pedido->row()->tipo_pago=='2'?'class="active"':'' ?>><a href="#deposito" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-bank"></i> Depósito o Transferencia Bancaria</a></li>
        <li role="presentation" <?= $pedido->row()->tipo_pago=='4'?'class="active"':'' ?>><a href="#electronico" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-bank"></i> Dinero Electrónico</a></li>
        <li role="presentation" <?= $pedido->row()->tipo_pago=='3'?'class="active"':'' ?>><a href="#efectivo" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-money"></i> Pago en efectivo al mensajero</a></li>
      </ul>      
      <!-- Tab panes -->
      <div class="tab-content">
          <div role="tabpanel" class="tab-pane <?= $pedido->row()->tipo_pago=='1'?'active':'' ?>" id="tarjeta">
              <?= img('img/payphone.png','width:100%;') ?><br/>
              <center>
                    <a href="<?= base_url('pedidos/admin/pagar/payphone/'.$pedido->row()->id) ?>" class="btn btn-success">
                        <i class="fa fa-credit-card"></i> Pagar con payphone
                    </a>
              </center>
          </div>
          <div role="tabpanel" class="tab-pane <?= $pedido->row()->tipo_pago=='2'?'active':'' ?>" id="deposito">
              <form onsubmit="return guardar(this)">
                <?= $this->db->get('ajustes')->row()->cuentas_bancarias ?>
                <div class="form-group">
                    <label for="ciudades_id">Depósito en el banco: *</label>
                    <?= form_dropdown_from_query('bancos','bancos','bancos_nombre','bancos_nombre') ?>
                </div>
                <div class="form-group">
                    <label for="confirmacion">Nro. Confirmación: *</label>
                    <input type='text' name='confirmacion' id='field-confirmacion' class="form-control">
                </div>
                <div class="form-group">
                    <label for="nombre">Nombre del depósitante: *</label>
                    <input type='text' name='nombre' id='field-nombre' class="form-control">
                </div>
                <div class="form-group">
                    <label for="monto">Monto: *</label>
                    <input type='number' name='deposito' id='field-monto' class="form-control" value="<?= $pedido->row()->precio ?>">
                </div>
                <div class="form-group">
                    <label for="fecha">Fecha Deposito: *</label>
                    <input type='text' name='fecha' id='field-fecha' class="form-control" value="<?= date("d-m-Y H:i:s") ?>">
                </div>
                <div class='form-group'>
                    <input type='hidden' value='<?= $id ?>' name='pedidos_id'>
                    <button type="submit" class="btn btn-success btn-block">Enviar datos</button>
                </div>
              </form>
          </div>
          <div role="tabpanel" class="tab-pane <?= $pedido->row()->tipo_pago=='3'?'active':'' ?>" id="efectivo">
              <form onsubmit="return guardar(this)">
                <div class="form-group">
                    <label for="confirmacion">Total Efectivo: *</label>
                    <input type='number' name='efectivo' id='field-confirmacion' class="form-control" value="<?= $pedido->row()->precio ?>">
                </div>
                <div class="form-group">
                    <label for="nombre">Total Tarjeta: *</label>
                    <input type='number' name='tarjeta' id='field-nombre' class="form-control" value="0">
                </div>
                <div class='form-group'>
                    <input type='hidden' value='<?= $id ?>' name='pedidos_id'>
                    <input type='hidden' value='<?= $this->user->nombre ?>' name='nombre'>
                    <button type="submit" class="btn btn-success btn-block">Enviar datos</button>
                </div>
              </form>
          </div>
          <div role="tabpanel" class="tab-pane <?= $pedido->row()->tipo_pago=='4'?'active':'' ?>" id="electronico">
              <form onsubmit="return guardar(this)">
                <div class="form-group">
                    <label for="confirmacion">Total: *</label>
                    <input type='number' name='electronico' id='field-confirmacion' class="form-control" value="<?= $pedido->row()->precio ?>">
                </div>
                <div class="form-group">
                    <label for="confirmacion">Nro. Confirmación: *</label>
                    <input type='text' name='confirmacion' id='field-confirmacion' class="form-control">
                </div>
                <div class='form-group'>
                    <input type='hidden' value='<?= $id ?>' name='pedidos_id'>
                    <input type='hidden' value='<?= $this->user->nombre ?>' name='nombre'>
                    <button type="submit" class="btn btn-success btn-block">Enviar datos</button>
                </div>
              </form>
          </div>
      </div>

    </div>
    <script>
        function guardar(form){
            var datos = new FormData(form);
            $.ajax({
                url:'<?= base_url('pedidos/admin/pagos/insert_validation') ?>',
                data:datos,
                type:'post',
                processData:false,
                cache:false,
                contentType: false,
                success:function(data){
                    data = data.replace('<textarea>','');
                    data = data.replace('</textarea>','');
                    data = JSON.parse(data);
                    if(data.success){
                        $.ajax({
                            url:'<?= base_url('pedidos/admin/pagos/insert') ?>',
                            data:datos,
                            type:'post',
                            processData:false,
                            cache:false,
                            contentType: false,
                            success:function(data){
                                data = data.replace('<textarea>','');
                                data = data.replace('</textarea>','');
                                data = JSON.parse(data);
                                if(data.success){                                
                                    $("#message").removeClass('alert alert-danger').addClass('alert alert-success');
                                    $("#message").html(data.success_message);
                                    $("#message").fadeIn(500);
                                    setTimeout(function(){document.location.href="<?= base_url('pedidos/admin/pedidos') ?>";},2000);
                                }else{

                                }
                            }
                        });
                    }else{                    
                        $("#guardar").show();
                        $("#message").removeClass('alert alert-success').addClass('alert alert-danger');
                        $("#message").html(data.error_message);
                        $("#message").fadeIn(500);
                    }
                }
            });
            return false;
        }
    </script>
</section>