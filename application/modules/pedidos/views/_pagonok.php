<section>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Pago recibido
                </div>
                <div class="panel-body" style="position:relative; text-align: center">
                    <p><span style="color:red"><i class="fa fa-remove fa-5x"></i></span><br/>Ha habido un problema con su pago</p>                                        
                </div>
            </div>
        </div>
    </div>
</section>