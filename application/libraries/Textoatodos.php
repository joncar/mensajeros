<?php

class Textoatodos
{
    protected $user = "4E28EE0CBD2796D";
    protected $password = "33FE9FC040";
    
    public function __construct() {
        
    }
    
    public function send($celular = '',$mensaje = ''){
            if(strlen($celular)>=9){
                $celular = substr($celular,0,1)=='0'?'593'.substr($celular,1):'593'.$celular;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,'http://envia-movil.com/api/Envios');
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode(array('destinatarios'=>array($celular),'mensaje'=>$mensaje,'mensajes'=>array($mensaje))));
                curl_setopt($ch,CURLOPT_HTTPHEADER,
                    array(
                      "Authorization: Basic " . base64_encode($this->user . ":" .$this->password),
                      "Content-type: application/json",
                      "Accept: application/json"
                ));

                $server_output = curl_exec ($ch);
                curl_close ($ch);
                print_r($server_output);
                return $server_output; 
            }
    }
    
    public function notificar($pedido){
        switch($pedido->status){
            case '-2':
                 $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'sms_send_on_cancelado_banco','activo'=>1));
            break; 
            case '-1': 
                $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'sms_send_on_cancelado','activo'=>1));
            break;                     
            case '1':
                $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'sms_send_on_esperando_pago','activo'=>1));
            break;
            case '2': 
                $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'sms_send_on_asignado','activo'=>1));
            break;                
            case '3': 
                $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'sms_send_on_recogido','activo'=>1));
            break;
            case '4': 
                $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'sms_send_on_transito','activo'=>1));
            break;
            case '5':
            case '6':
                $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'sms_send_on_entregado','activo'=>1));                
            break;
        }
        if($mensaje->num_rows()>0){
            $mensaje = $mensaje->row()->titulo;
            $this->send($pedido->celular,$mensaje);
            return true;
        }else{
            return false;
        }
    }
}

