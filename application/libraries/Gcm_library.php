<?php
class Gcm_library {
    function __construct()
    {
        $this->apiKey = 'AIzaSyBiTQp5QUWTeD0XQpNnv3kevaKrtokkpnA';
        $this->appIdOneSignal = '8486056a-23b5-4103-abd5-db26a28d3ed1';
        $this->apiKeyOneSignal = 'MTkwYTQ2ZmUtNGM2Ni00MjI5LTk4N2YtYzM5OGViOWZiMGQz';
    }
    
    function Send($registrationIdsArray,$messageData,$type = 'Android')
    {
        if($type=='Android'){
            $headers = array("Content-Type:" . "application/json", "Authorization:" . "key=" . $this->apiKey);
            $data = array(
            'data' => $messageData,
            'registration_ids' => $registrationIdsArray
            );

            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch, CURLOPT_URL, "https://android.googleapis.com/gcm/send" );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
        }else{
            return $this->iosSend($registrationIdsArray,$messageData);
        }
    }
    
    function iosSend($registrationIdsArray,$messageData){
        $headers = array("Content-Type:" . "application/json", "Authorization:" . "Basic " . $this->apiKeyOneSignal);
        $data = array(
        'app_id' => $this->appIdOneSignal,
        'contents' => array('en'=>$messageData['message']),
        'include_player_ids'=>$registrationIdsArray
        );

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications" );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
    
    function encodeMsj($titulo,$msj,$extra = array())
    {
        // Message to send
        $message = $msj;
        $tickerText = rand(0,255);
        $contentTitle = $titulo;
        $contentText = $titulo;
        $message = array('message' => $message, 'tickerText' => $tickerText, 'contentTitle' => $contentTitle, "contentText" => $contentText,'title'=>$titulo);
        return array_merge($message,$extra);
    }
    
    public function notificar($pedido,$tipo = 'repartidores'){
        switch($pedido->status){
            case '-2':
                 $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'push_send_on_cancelado_banco','activo'=>1));
            break; 
            case '-1': 
                $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'push_send_on_cancelado','activo'=>1));
            break;                     
            case '1':
                $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'push_send_on_esperando_pago','activo'=>1));
            break;
            case '2': 
                $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'push_send_on_asignado','activo'=>1));
            break;                
            case '3': 
                $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'push_send_on_recogido','activo'=>1));
            break;
            case '4': 
                $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'push_send_on_transito','activo'=>1));
            break;
            case '5':
            case '6':
                $mensaje = $this->db->get_where('configuraciones',array('identificador'=>'push_send_on_entregado','activo'=>1));                
            break;
        }
        if($mensaje->num_rows()>0){
            //Android
            $ids = $this->db->get_where('gcm_'.$tipo,array($tipo.'_id'=>$pedido->{$tipo.'_id'},'tipo'=>'Android'));
            if($ids->num_rows()>0){                
                $ida = array();
                foreach($ids->result() as $i){
                    $ida[] = $i->valor;
                }            
                $this->Send($ida,$this->encodeMsj('Actualización de pedido',$mensaje->row()->titulo,array('pedido'=>$pedido->id)));            
            }
            
            //IOS
            $ids = $this->db->get_where('gcm_'.$tipo,array($tipo.'_id'=>$pedido->{$tipo.'_id'},'tipo'=>'IOS'));
            if($ids->num_rows()>0){                
                $ida = array();
                foreach($ids->result() as $i){
                    $ida[] = $i->valor;
                }            
                $this->Send($ida,$this->encodeMsj('Actualización de pedido',$mensaje->row()->titulo,array('pedido'=>$pedido->id)),'IOS');
            }
        
            return true;
        }else{
            return false;
        }
    }
        
}
?>