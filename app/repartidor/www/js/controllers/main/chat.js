controllers.controller('Chat', function($scope,$rootScope,$http,$ionicModal,$ionicPopup,Api,UI,User,Group) {
    $scope = User.getData($scope);
    $scope.loadingSpinner = false;
    
    $scope.consultar = function(){
        $rootScope.$on('onUpdate',function(evt,data){
            $scope.mensajes = data.mensajes; 
            $scope.operadores = data.operadores;
            $scope.loadingSpinner = false;
        });
        $rootScope.$broadcast('triggerUpdate');
    };
    $scope.consultar();
});