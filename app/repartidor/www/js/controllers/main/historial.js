controllers.controller('Historial', function($scope,$rootScope,$http,$ionicModal,$ionicPopup,Api,UI,User,Group) {
    $scope = User.getData($scope);
    $scope.pedidos = [];
    $scope.loadingSpinner = true;
    $scope.consultar = function(){
        $rootScope.$on('onUpdate',function(evt,data){            
            $scope.pedidos = data.pedidos;            
            $scope.loadingSpinner = false;
            for(var i in $scope.pedidos){                
                $scope.pedidos[i].cliente = $scope.pedidos[i].sdfe80649;
                $scope.pedidos[i].cliente = $scope.pedidos[i].cliente.split('|');
                $scope.pedidos[i].foto = 'http://mensajeros.ec/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';
                if($scope.pedidos[i].cliente[2]!==''){
                    $scope.pedidos[i].foto = 'http://mensajeros.ec/img/fotos/'+$scope.pedidos[i].cliente[2];
                }
            }
        });
    };
    
    $scope.transformarStatus = function(pedido){
        switch(pedido.tipo_tramite){
            case '1':
            case '2':
                switch(pedido.status){
                    case '1': return 'Esperando Pago'; break;
                    case '2': return 'Mensajero Asignado';  break;
                    case '3': return 'Paquete Recogido';  break;
                    case '4': return 'En Tránsito';  break;
                    case '5': return 'Entregado';  break;
                }
            break;
            case '3':
                switch(pedido.status){
                    case '1': return 'Esperando Pago'; break;
                    case '2': return 'Mensajero Asignado';  break;
                    case '3': return 'Trámite en proceso';  break;
                    case '5': return 'Trámite Finalizado';  break;
                }
            break;
        }
    };
    
    $scope.consultar();
    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };
});