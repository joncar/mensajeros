controllers.controller('Pedidos', function($scope,$http,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.pedidos = [];
    $scope.consultar = function(){
        Api.list('pedidos',{'status >':'0','repartidores_id':$scope.repartidores_id},$scope,$http,function(data){
            $scope.pedidos = data;
            for(var i in $scope.pedidos){
                var foto = 'https://mensajeros.ec/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';                
                if($scope.pedidos[i].repartidores_id!==null){
                    var datos = $scope.pedidos[i].sdfe80649;
                    datos = datos.split('|');
                    if(datos.length===3){
                        foto = 'http://mensajeros.ec/img/fotos/'+datos[2];
                    }
                }
                $scope.pedidos[i].foto = foto;
            }
        });
    };
    
    $scope.transformarStatus = function(pedido){
        switch(pedido.tipo_tramite){
            case '1':
            case '2':
                switch(pedido.status){
                    case '1': return 'Esperando Pago'; break;
                    case '2': return 'Mensajero Asignado';  break;
                    case '3': return 'Paquete Recogido';  break;
                    case '4': return 'En Tránsito';  break;
                    case '5': return 'Entregado';  break;
                }
            break;
            case '3':
                switch(pedido.status){
                    case '1': return 'Esperando Pago'; break;
                    case '2': return 'Mensajero Asignado';  break;
                    case '3': return 'Trámite en proceso';  break;
                    case '5': return 'Trámite Finalizado';  break;
                }
            break;
        }
    };
    
    $scope.consultar();
});