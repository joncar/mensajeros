controllers.controller('Itinerario', function($scope,$rootScope,$http,$ionicModal,$ionicPopup,Api,UI,User,Group) {
    $scope = User.getData($scope);
    $scope.activo = 1;
    $scope.pedidos = [];
    $scope.tareas = Group.load('tareas');
    $scope.list = [];
    $scope.loadingSpinner = true;
    $scope.shownGroup = null;
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    UI.getModalBox($ionicModal,'templates/main/modal/perfil.html',$scope,function(modal){
        $scope.perfil = modal;
    });
    UI.getModalBox($ionicModal,'templates/main/modal/pedido.html',$scope,function(modal){
        $scope.modal = modal;            
    });
    $scope.alert = UI.getShowAlert($ionicPopup);
    $scope.consultar = function(){
        $rootScope.$on('onUpdate',function(evt,data){
            $scope.pedidos = data.pedidos;
            if(data.pedidos.length===0){
                $scope.tareas.list = [];                
            }
            for(var i in $scope.pedidos){
                for(var k in $scope.pedidos[i].descripcion.tareas){
                    if($scope.pedidos[i].descripcion.tareas[k].status==='0'){
                        if($scope.tareas.getFromId($scope.pedidos[i].descripcion.tareas[k].id)===null){                        
                            $scope.tareas.add($scope.pedidos[i].descripcion.tareas[k]);
                        }else{
                            $scope.tareas.update($scope.pedidos[i].descripcion.tareas[k]);
                        }
                    }else{
                        if($scope.tareas.getFromId($scope.pedidos[i].descripcion.tareas[k].id)!==null){
                            $scope.tareas.remove($scope.pedidos[i].descripcion.tareas[k]);
                        }
                    }
                }                
                $scope.pedidos[i].cliente = $scope.pedidos[i].sdfe80649;
                $scope.pedidos[i].cliente = $scope.pedidos[i].cliente.split('|');
                $scope.pedidos[i].foto = 'https://mensajeros.ec/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';
                if($scope.pedidos[i].cliente[2]!==''){
                    $scope.pedidos[i].foto = 'https://mensajeros.ec/img/fotos/'+$scope.pedidos[i].cliente[2];
                }
            }
            //Limpiar tareas no actualizadas
            for(var k in $scope.tareas.list){
                var enc = false;
                for(var i in $scope.pedidos){
                    if($scope.pedidos[i].id===$scope.tareas.list[k].pedidos_id){
                        enc = true;
                    }
                }
                if(!enc){
                    $scope.tareas.remove($scope.tareas.list[k]);
                }
            }
            $scope.list = $scope.tareas.list;
            $scope.tareas.save('tareas');
            $scope.loadingSpinner = false;            
        });
    };
    $rootScope.$broadcast('triggerUpdate');
    $scope.mover = function(direccion,item){
        var t = $scope.tareas.list;
        for(var i in $scope.tareas.list){
            if($scope.tareas.list[i].id===item.id){
                i = parseInt(i);              
                direccion = i+parseInt(direccion);                
                if($scope.tareas.list[direccion]!==undefined){
                    var aux = t[direccion];
                    t[direccion] = item;
                    t[i] = aux;
                }
            }
        }  
        $scope.tareas.list = t;
        $scope.list = $scope.tareas.list;
        $scope.tareas.save('tareas');
        if(!$scope.$$phase){
            $scope.$apply();
        }        
    };
    
    $scope.select = function(item){
        for(var i in $scope.pedidos){
            if($scope.pedidos[i].id === item.pedidos_id){
                $scope.selected = $scope.pedidos[i];
                $scope.selected.cliente = $scope.pedidos[i].sdfe80649;
                $scope.selected.cliente = $scope.selected.cliente.split('|');
                $scope.selected.foto = 'https://mensajeros.ec/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';
                if($scope.selected.cliente[2]!==''){
                    $scope.selected.foto = 'https://mensajeros.ec/img/fotos/'+$scope.selected.cliente[2];
                }
                $scope.selected.direccion = item;
                $scope.modal.show();
            }
        }        
    };
    
    $scope.navegar = function(pedido){
        lat = pedido.lat;
        lon = pedido.lon;
        window.open('http://maps.google.com/maps?q='+lat+','+lon, '_system');
    };
    
    $scope.agrandar = function(pedido){
      $scope.perfild = pedido;  
      $scope.perfil.show();
    };
    $scope.sendit = false;
    $scope.completar = function(pedido){
        var validado = true;
        for( var i in $scope.pedidos){
            if($scope.pedidos[i].id === pedido.pedidos_id){
                var descripcion = $scope.pedidos[i].descripcion.tareas;
                var total = descripcion.length;
                var completas = 0;
                for(var k in descripcion){
                    if(descripcion[k].status==="1"){
                        completas++;
                    }
                }
                if(completas === (total-1) && ($scope.pedidos[i].firma==='' || $scope.pedidos[i].firma===null)){
                    validado = false;
                }
            }
        }
        if(validado){
            $scope.data = {status:1};
            $scope.sendit = true;
            Api.update('pedidos_detalles',pedido.id,$scope,$http,function(data){
                $scope.alert('Tarea Actualizada','Su tarea ha sido actualizada y notificada al cliente');
                $scope.modal.hide();
                $rootScope.$broadcast('triggerUpdate');
                $scope.sendit = false;
            });
        }else{
            $scope.showAlert('Falta la firma','Por favor cargue la firma del cliente antes de completar su pedido');
        }
    };
    $scope.consultar();
    
    
    $scope.transformarStatus = function(pedido){
        switch(pedido.tipo_tramite){
            case '1':
            case '2':
                switch(pedido.status){
                    case '1': return 'Esperando Pago'; break;
                    case '2': return 'Mensajero Asignado';  break;
                    case '3': return 'Paquete Recogido';  break;
                    case '4': return 'En Tránsito';  break;
                    case '5': return 'Entregado';  break;
                }
            break;
            case '3':
                switch(pedido.status){
                    case '1': return 'Esperando Pago'; break;
                    case '2': return 'Mensajero Asignado';  break;
                    case '3': return 'Trámite en proceso';  break;
                    case '5': return 'Trámite Finalizado';  break;
                }
            break;
        }
    };
    
    $scope.transformarEntrega = function(fecha,turno){
        if(fecha===undefined || fecha.search('00:00:00')>-1 || fecha===''){
            if(turno==='Tarde'){
                return '14:00-18:00';
            }else{
                return '08:00-13:00';
            }
        }else{
            fecha = new Date(fecha);
            return fecha.getDate()+'-'+fecha.getMonth()+'-'+fecha.getFullYear()+' '+fecha.getHours()+':'+fecha.getMinutes();           
        }
    };
    
    $scope.transformarTipoTramite = function(id){
        switch(id){
            case '1': return 'Estandar';
            case '2': return 'Express';
            case '3': return 'Trámite';
        }
    };
    
    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };
    
    $scope.enviarpago = function(datos){
        $scope.confirm = UI.getConfirmBox($ionicPopup);
        $scope.confirm('Cargar pago','Esta usted seguro que desea cargar este pago?',function(){
            $scope.showAlert = UI.getShowAlert($ionicPopup);
            $scope.data = {pagado:1};
            Api.update('pedidos',datos.id,$scope,$http,function(data){
                $scope.showAlert('Pago enviado','Notificación de pago recolectado enviada');
                $rootScope.$broadcast('triggerUpdate');
            });
        });
        
    };
});