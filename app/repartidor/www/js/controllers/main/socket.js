controllers.controller('Socket', function($scope,$rootScope,$http,$ionicPopup,$ionicPlatform,$ionicSideMenuDelegate,getDirFromGPS,Api,User,oneSignal){
    $scope = User.getData($scope);                     
    var timeout = null;
    $scope.consultar = function(){
        getDirFromGPS.native = true;        
        $scope.data = {'repartidores_id':$scope.repartidores_id};
        Api.query('getUpdates',$scope,$http,function(data){
            $rootScope.$broadcast('onUpdate',data);
            console.log(data);
            if(typeof(localStorage.disponible)==='undefined' || localStorage.disponible==='1'){
                getDirFromGPS.native = true;
                getDirFromGPS.getDirFromGps(function(){});
            }else{
                getDirFromGPS.stopNativeGps(function(){});
            }
        },'where');
        timeout = setTimeout($scope.consultar,5000);
    };
    
    $rootScope.$on('triggerUpdate',function(){
       clearTimeout(timeout);
       $scope.consultar(); 
    });
    
    $ionicPlatform.ready(function(){
        //Existe conexión?
        if(window.Connection) {
            if(navigator.connection.type === Connection.NONE) {
                $scope.showAlert('¿Haz perdido la conexión a internet?, necesitas conectarte para poder seguir usando la aplicación');
            }
        }
        oneSignal.init($scope);        
        $scope.consultar();
    });        
});