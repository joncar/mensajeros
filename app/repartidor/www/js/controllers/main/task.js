controllers.controller('Task', function($scope,$stateParams,$rootScope,$ionicPopup,$http,Api,UI,User,getDirFromGPS) {
    $rootScope.$broadcast('updateReloj');
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.task = [];
    $rootScope.$on('updateTask',function(evt,data){
        if(data.length>0){
            $scope.task = data;        
            $scope.itinerario = $scope.task[0].task;
            for(var i in $scope.itinerario){
                if($stateParams.id === $scope.itinerario[i].id){
                    $scope.taskSelected = $scope.itinerario[i];
                }
            }
        }
    });
    
    $scope.openNavigation = function(lat,lon){
        lat = lat || $scope.task[0].lat;
        lon = lon || $scope.task[0].lon;
        window.open('http://maps.google.com/maps?q='+lat+','+lon, '_system');
    };
    
    $scope.fillDirection = function(i){
        console.log('Llenar dirección por GPS');
        if(i.direccion===''){
            $scope.gps();
        }
    };
    
    
    $scope.selectFieldsFunction = function(place,lat,lon){
        var direccion = $scope.direccion;
        $scope.dir = getDirFromGPS.selectFieldsFunction(place);           
        $scope.data = {
            direccion:$scope.dir.direccion===undefined?direccion:$scope.dir.direccion,
            lat:lat,
            lon:lon,
            status:$scope.taskSelected.status
        };
        Api.update('pedidos_detalles',$scope.taskSelected.id,$scope,$http,function(data){
            $scope.showAlert('Dirección almacenada del gps');
            $rootScope.$broadcast('updateReloj');
        });
    };
    
    $scope.gps = function(){
      getDirFromGPS.native = false;
      getDirFromGPS.getDirFromGps(function(lat,lon,dir){
          getDirFromGPS.native = true;
          $scope.direccion = dir.direccion;
          dir.lat = lat;
          dir.lon = lon;
          $scope.selectFieldsFunction(dir,lat,lon);
      });
    };
    
    
    $scope.complete_task = function(i){
        if(i.status==='0' && i.precio==='0.00'){            
            $scope.confirm = UI.getConfirmBox($ionicPopup);
            $scope.confirm('¿Estás seguro?','¿Seguro que deseas completar esta orden?',function(){
               $scope.data = {status:1};
               Api.update('pedidos_detalles',i.id,$scope,$http,function(data){
                   $rootScope.$broadcast('updateReloj');
                   document.location.href="#/tab/main";
               });
            });
        }else{
            $scope.cobrar(i);
        }
    };
    
    $scope.cobrar = function(i){
       document.location.href="#/tab/cobrar/"+i.id; 
    };
});