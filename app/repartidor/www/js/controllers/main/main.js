var myPopup = '';
var controllers = angular.module('starter.controllers', []);

controllers.controller('Main', function($scope,$rootScope,$state,$http,$ionicPlatform,$ionicModal,$ionicPopup,Api,UI,User) {
    
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.basel = {lat:localStorage.lat,lon:localStorage.lon};
    $scope.task = [];
    $scope.markers = [];
    $scope.innerheight = localStorage.innerHeight;
    $scope.iconMap = 'http://74.208.12.230/pizzasapp/img/fooddeliveryservice.png';
    
    $rootScope.$on('updateTask',function(evt,data){
        $scope.task = data;
        $scope.itinerario = {};
        $scope.markers = [];
        if(data.length>0){            
            $scope.itinerario = $scope.task[0].task;            
            $scope.markers.push({lat:localStorage.lat,lon:localStorage.lon,name:'Mi ubicación'});
            $scope.markers.push({lat:$scope.task[0].lat,lon:$scope.task[0].lon,name:'Pedido #'+$scope.task[0].id});
        }
    });
    
    //Controles
    $scope.start = function(){        
        $scope.data = {status:2,sucursales_id:$scope.task[0].sucursales_id};
        Api.update('pedidos',$scope.task[0].id,$scope,$http,function(data){
            $scope.showAlert('Notificación','Mensaje enviado al cliente.');
            $rootScope.$broadcast('updateReloj',false);
        });
    };
    
    $scope.showDirection = function(){
      $scope.showAlert('Ubicación','<p><b>Cliente: </b>'+$scope.task[0].sucursal+'</p><p><b>Dirección: </b>'+$scope.task[0].direccion_envio+'</p>');  
    };
    
    $scope.openNavigation = function(lat,lon){
        lat = lat || $scope.task[0].lat;
        lon = lon || $scope.task[0].lon;
        window.open('http://maps.google.com/maps?q='+lat+','+lon, '_system');
    };        
    
    $scope.complete = function(){       
       document.location.href="#/tab/itinerario";
    };
    
    $rootScope.$broadcast('updateReloj');
});