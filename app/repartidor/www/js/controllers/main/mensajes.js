controllers.controller('Mensajes', function($scope,$rootScope,$ionicPlatform,$stateParams,$http,$ionicModal,$ionicPopup,Api,UI,User,Group) {
    $scope = User.getData($scope);
    $scope.loadingSpinner = false;
    $scope.mensajes = [];
    $scope.destino = '';
    $scope.texto = '';
    $ionicPlatform.ready(function(){        
        $scope.destino = $stateParams.nombre;
        $scope.destinoid = $stateParams.userid;
    });
    $scope.consultar = function(){
        $rootScope.$on('onUpdate',function(evt,data){
            $scope.mensajes = JSON.parse(data.mensajes);
        });
        $rootScope.$broadcast('triggerUpdate');
    };
    
    $scope.sendMessage = function(texto){
        if(texto!==''){            
            $scope.data = {destino:$scope.destinoid,mensaje:texto,user_id:$scope.user};
            $scope.mensajes.push($scope.data);
            Api.query('sendMessage',$scope,$http,function(data){
                
            });
            $scope.texto = '';
        }
    };
    $scope.consultar();
});