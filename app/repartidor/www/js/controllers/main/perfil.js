controllers.controller('Perfil', function($scope,$http,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.datos = {
        nombre:$scope.nombre,
        password: $scope.password
    };
    
    $scope.send = function(datos){
        $scope.data = datos;
        $scope.data.email = $scope.email;
        Api.update('repartidores',$scope.user,$scope,$http,function(data){
            $scope.showAlert('Datos almacenados con éxito');
            localStorage.nombre = datos.nombre;
        });
    };
    
    $scope.unlog = function(){
        User.cleanData();
        document.location.href="index.html";
    };
});