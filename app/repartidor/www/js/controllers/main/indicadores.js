controllers.controller('Indicadores', function($scope,$rootScope,$http,$ionicPopup,Api,UI,User){
    $scope.workin = typeof(localStorage.disponible)==='undefined' || localStorage.disponible === '1'?true:false;
    $scope = User.getData($scope);
    $scope.toggleWorkin = function(){
        if(typeof(localStorage.disponible)==='undefined'  || localStorage.disponible !== '0'){
            $scope.workin = false;
            localStorage.disponible = '0';
            $scope.data = {status:5};
        }else{
            $scope.workin = true;
            localStorage.disponible = '1';
            $scope.data = {status:1};
        }
        $rootScope.$broadcast('triggerUpdate');
        /*$scope.data.email = $scope.email;
        Api.update('repartidores',$scope.user,$scope,$http,function(data){
            $scope.showAlert = UI.getShowAlert($ionicPopup);
            $scope.showAlert('Cambio de status','Su Disponibilidad ha sido cambiada');
            $rootScope.$broadcast('updateReloj');
        });*/
    };
});