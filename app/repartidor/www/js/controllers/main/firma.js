controllers.controller('Firma', function($scope,$rootScope,$http,$stateParams,$ionicPopup,Api,UI,User,Group) {
    $scope = User.getData($scope);
    $scope.loadingSpinner = false;
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.firmante = '';
    var wrapper = document.getElementById("signature-pad"),
    clearButton = wrapper.querySelector("[data-action=clear]"),
    saveButton = wrapper.querySelector("[data-action=save]"),
    canvas = wrapper.querySelector("canvas"),
    signaturePad;
    signaturePad = new SignaturePad(canvas);
        
    $scope.subir = function(firmante){
        if(firmante!==''){
            $scope.data = {
                pedidos_id:$stateParams.id,
                firma:signaturePad.toDataURL(),
                firmante:firmante
            };
            Api.query('subirFirma',$scope,$http,function(data){
                $scope.showAlert('Firma',data);
                $rootScope.$broadcast('triggerUpdate');
                history.back();
            });
        }else{
            $scope.showAlert('Firmante','Por favor indique el nombre de la persona que esta realizando la firma?');
        }
    };
    $scope.clear = function(){
        signaturePad.clear();
    };
    
});