services.factory('getDirFromGPS',function(){
    return {
            componentForm:{
              street_number: 'short_name',
              route: 'long_name',
              locality: 'long_name',
              administrative_area_level_1: 'long_name',
              postal_code: 'short_name',
              sublocality_level_1:'long_name'
          },

          inputForm:{
            street_number: 'ext',
            route: 'calle',
            locality: 'delegacion',
            administrative_area_level_1: 'ciudad',
            postal_code: 'cp',
            sublocality_level_1:'colonia'
          },
          
          native:true,
          
          getLocationName:function(lat,lon,callback){
                var geocoder = new google.maps.Geocoder;
                geocoder.geocode({'latLng': new google.maps.LatLng(lat,lon)}, function(results, status) {
                    //calle
                    var loc = results.length>0?results[0]:'';
                    callback(lat,lon,loc);
                });
          },

          selectFieldsFunction:function(place){
              var datos = {};
              for (var i = 0; i < place.address_components.length; i++) {
                  var addressType = place.address_components[i].types[0];
                  if(this.componentForm[addressType]){
                      var val = place.address_components[i][this.componentForm[addressType]];
                      datos[this.inputForm[addressType]] = val;
                  }
              }
              return datos;
          },
          getDirFromGps:function(callback){
              var instance = this;
              if(typeof(cordova)!=='undefined' && typeof(cordova.plugins)!=='undefined' && this.native){
                  cordova.plugins.repGpsSync.start({
                      url:url+"repartidores/update/"+localStorage.repartidores_id,
                      email:localStorage.email
                  });                  
              }
            var options = {enableHighAccuracy: true};                
            navigator.geolocation.getCurrentPosition(function(position) {
                var c = position.coords;
                instance.onResponseGps(c.latitude,c.longitude,callback);
             },function(e){
                 //alert('No se pudo comunicar con el gps por lo que se usará la última posición conocida');             
             },options);            
          },
          onResponseGps:function(lat,lon,callback){
                localStorage.lat = lat;
                localStorage.lon = lon;                
                this.getLocationName(lat,lon,function(lat,lon,dir){
                   dir.direccion = dir.formatted_address;
                   callback(lat,lon,dir);
                });
          },
          stopNativeGps:function(){
              if(typeof(cordova)!=='undefined' && typeof(cordova.plugins)!=='undefined' && this.native){
                  cordova.plugins.repGpsSync.stop();
                  this.native = false;
              }
          },
          mergeDirection:function(dir){
                dir.direccion = dir.calle===undefined?'':' '+dir.calle;
                dir.direccion+= dir.ext === undefined?'':' ext '+dir.ext;
                dir.direccion+= dir.colonia===undefined?'':' '+dir.colonia;
                dir.direccion+= dir.int===undefined?'':' int '+dir.int;
                dir.direccion+= dir.delegacion===undefined?'':' '+dir.delegacion;
                dir.direccion+= dir.ciudad===undefined?'':' '+dir.ciudad;
                dir.direccion+= dir.cp===undefined?'':' cp '+dir.cp;
                dir.direccion+= dir.localidad===undefined?'':' '+dir.localidad;
                return dir.direccion;
          }
    };
});