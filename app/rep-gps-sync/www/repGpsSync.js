var argscheck = require('cordova/argscheck'),
    utils = require('cordova/utils'),
    exec = require('cordova/exec'),
    channel = require('cordova/channel');

var starter = false;

window.repgpssync = function(lat,lon){
    exports.onUpdateLocation(lat,lon);
}
exports.onUpdateLocation = function(){};
exports.start = function(data){
    if(!starter){
        starter = true;
        exec(null, null, "RepGpsSync", "start", [data]);
    }
}

exports.stop = function(){
    if(starter){
        starter = false;
        exec(null, null, "RepGpsSync", "stop", []);
    }
}