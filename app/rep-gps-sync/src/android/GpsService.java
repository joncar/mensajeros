package rep.gps.sync;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import org.apache.cordova.CordovaInterface;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Jonathan on 24/06/2016.
 */
public class GpsService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private int idNotify = 19555482;
    public static Sync sync = null;
    private String PackName = "rep.gps.sync";
    //GPS
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest = null;
    public Location mLastLocation;
    public CordovaInterface cordova;
    public Boolean enable = true;
    @Override
    public IBinder onBind(Intent intent) {return null;}

    /* Crear Foreground */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Guardamos preferencias
        if(sync!=null){
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(PackName, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("url",sync.params.optString("url"));
            editor.putString("email",sync.params.optString("email"));
            editor.commit();
        }
        //Iniciamos Notificacion
        Context context = getApplicationContext();
        //Iniciamos conexion con google play
        Log.v("sync", "Consiguiendo Locacion");
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mLocationRequest=new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mGoogleApiClient.connect();
        }
        return START_STICKY;
    }

    private int getIconResId() {
        Context context = getApplicationContext();
        Resources res   = context.getResources();
        String pkgName  = context.getPackageName();

        int resId;
        resId = res.getIdentifier("icon", "drawable", pkgName);

        return resId;
    }
    /* Fin Foreground */

    private void sendData(String lat, String lon) throws IOException {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(PackName, Context.MODE_PRIVATE);
        String uri = sharedPreferences.getString("url", "");
        String email = sharedPreferences.getString("email","");
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String urlParameters = "lat=" + lat + "&lon=" + lon + "&email=" + email;
        URL url = new URL(uri);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);
        DataOutputStream dStream = new DataOutputStream(urlConnection.getOutputStream());
        dStream.writeBytes(urlParameters); //Writes out the string to the underlying output stream as a sequence of bytes
        dStream.flush(); // Flushes the data output stream.
        dStream.close(); // Closing the output stream.
        int responseCode = urlConnection.getResponseCode(); // getting the response code
        Log.v("sync", "Request URL " + url);
        Log.v("sync", System.getProperty("line.separator") + "Request Parameters " + urlParameters);
        Log.v("sync", System.getProperty("line.separator") + "Response Code " + responseCode);
        BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        String line = "";
        StringBuilder responseOutput = new StringBuilder();
        System.out.println("output===============" + br);
        while ((line = br.readLine()) != null) {
            responseOutput.append(line);
        }
        br.close();
        Log.v("sync", System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());
    }
    @Override
    public void onLocationChanged(Location location) {
        //Actualizar coordenadas en el servicio
        //Log.v("sync", String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude()));
        if(sync!=null) {
            sync.sayLocation(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
        }
        this.mLastLocation = location;
        //Enviar coordenadas al servidor
        //Api Vars
        try {
            sendData(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            //Log.v("sync", String.valueOf(mLastLocation.getLatitude()) + "," + String.valueOf(mLastLocation.getLongitude()));
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onDestroy(){
        //LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    /* Conectar servidor */

}
