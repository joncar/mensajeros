package rep.gps.sync;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.Manifest.permission;

/**
 * Created by Jonathan on 24/06/2016.
 */
public class Sync extends CordovaPlugin {

    public String lat = "0";
    public String lon = "0";
    private static final String JS_NAMESPACE = "cordova.plugins.rep-gps-sync.www.android";
    private static final int REQUEST_LOCATION = 2;
    public static JSONObject params = null;
    public static Boolean enable = false;
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {}
        public void onServiceDisconnected(ComponentName className) {}
    };

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        GpsService.sync = this;
    }

    @Override
    public void onDestroy() {
        GpsService.sync = null;
        Activity context = cordova.getActivity();
        context.unbindService(mConnection);
        super.onDestroy();
    }

    @Override
    public boolean execute (String action, JSONArray args,CallbackContext callback) throws JSONException {

        if (action.equalsIgnoreCase("start")) {
            Sync.params = args.getJSONObject(0);
            initService();
            Sync.enable = true;
            return true;
        }

        if (action.equalsIgnoreCase("stop")) {
            stopService();
            Sync.enable = false;
            return true;
        }

        return false;
    }
    private void initService(){
        if(Build.VERSION.SDK_INT < 23){
            _initService();
        }else {
            Activity context = cordova.getActivity();
            if (
                    context.checkSelfPermission(permission.ACCESS_FINE_LOCATION) == (int) PackageManager.PERMISSION_GRANTED &&
                    context.checkSelfPermission(permission.ACCESS_COARSE_LOCATION) == (int) PackageManager.PERMISSION_GRANTED &&
                    context.checkSelfPermission(permission.ACCESS_LOCATION_EXTRA_COMMANDS) == (int) PackageManager.PERMISSION_GRANTED
               ) {
                Log.v("sync","Permiso otorgado");
                _initService();
            } else {
                Log.v("sync", "Permiso no otorgado");
                cordova.requestPermissions(this,PackageManager.PERMISSION_GRANTED,new String[]{permission.ACCESS_FINE_LOCATION,permission.ACCESS_COARSE_LOCATION,permission.ACCESS_LOCATION_EXTRA_COMMANDS});
            }
        }
    }
    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) throws JSONException
    {
        for(int r:grantResults)
        {
            if(r == PackageManager.PERMISSION_DENIED)
            {
                return;
            }
        }
        _initService();
    }

    private void _initService(){
        Activity context = cordova.getActivity();
        Intent i = new Intent(context,GpsService.class);
        context.bindService(i, mConnection, Context.BIND_AUTO_CREATE);
        context.startService(i);
    }

    private void stopService(){
        Activity context = cordova.getActivity();
        Intent i = new Intent(context,GpsService.class);
        context.unbindService(mConnection);
        context.stopService(i);
    }

    public void sayLocation(final String lat, final String lon){
        cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript:window.repgpssync('"+lat+"','"+lon+"')");
            }
        });
    }
}
