MapApp.directive('appForm',function($compile){
    return {
        restrict:"AE",
        replace:true,
        require: '',
        template:'<form></form>',
        scope:{
            inputs:'=',
            submit:'=',
            buttons:'=',
            alert:'='
        },
        link:function(scope,element,attrs,model){            
            var input = element[0];
            for(var i in scope.inputs){
                if(typeof(scope.inputs[i].name)!=='undefined'){
                    var select = 'input';
                    switch(scope.inputs[i].type){
                        case 'enum':
                        case 'dropdown':
                            select = 'select';
                        break;
                        default:
                            select = 'input';
                        break;
                    }                
                    if(select==='input' || (select==='select' && typeof(scope.inputs[i].options)!=='undefined')){
                        label = getLabel(select,scope.inputs[i]);
                        inp = getInp(select,scope.inputs[i]);
                        label.append(inp);
                        input.append(label);
                    }else{
                        console.log('Falto el parametro data para el select '+scope.inputs[i].name);
                    }
                }else{
                    console.log('Falto el parametro name para el input '+i);
                }
            }
            //Buttons
            var div = document.createElement('div');
                div.className = 'button-bar';
            for(var i in scope.buttons){                
                if(typeof(scope.buttons[i].name)!=='undefined' && typeof(scope.buttons[i].type)!=='undefined'){
                    switch(scope.buttons[i].type){
                        case 'a':
                            var b = document.createElement('a');
                            b.href=scope.buttons[i].href||'#';
                            b.className = 'button '+scope.buttons[i].class||'button';
                            b.innerHTML = scope.buttons[i].name;
                        break;
                        case 'submit':
                            var b = document.createElement('button');
                            b.type=scope.buttons[i].type||'button';
                            b.className = 'button '+scope.buttons[i].class||'button';
                            b.innerHTML = scope.buttons[i].name;
                        break;
                    }
                    div.append(b);
                }else{
                    console.log('Al boton '+i+' le falto un parametro [name|type]');
                }
            }
            div.style.marginTop = '30px';
            input.append(div);
            var content = $compile(input)(scope);
            //input.append(content);
            
            //Event submit
            input.addEventListener("submit",function(event){
                event.preventDefault();
                var form = new FormData(this);
                var pass = true;
                var msj = '';
                for(i in scope.inputs){
                    //Validate
                    scope.inputs[i].value = form.get(scope.inputs[i].name);
                    var rules = typeof(scope.inputs[i].rules)!=='undefined'?scope.inputs[i].rules.split('|'):[];
                    for(var k in rules){
                        switch(rules[k]){
                            case 'required':
                                if(scope.inputs[i].value==='' || scope.inputs[i].value===undefined){
                                    pass = false;
                                    msj+= '<div>El campo <b>'+scope.inputs[i].name+'</b> es requerido</div>';
                                    break;
                                }
                            break;
                            case 'numeric':                                
                                var pattern = /^\d+$/;
                                if(pattern.test(scope.inputs[i].value)){
                                    msj+= '<div>El campo <b>'+scope.inputs[i].name+'</b> debe ser numérico</div>';
                                    break;
                                }
                            break;
                            case 'valid_email':
                                var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                if(pattern.test(scope.inputs[i].value)){
                                    msj+= '<div>El campo <b>'+scope.inputs[i].name+'</b> debe ser un email válido</div>';
                                    break;
                                }
                            break;
                            case 'min_length':
                                
                            break;
                            case 'max_length':
                                
                            break;
                        }
                    }
                }
                //trigger event submit
                if(pass && typeof(scope.submit)!=='undefined'){
                    var datos = [];
                    for(i in scope.inputs){
                        datos[scope.inputs[i].name] = scope.inputs[i].value;
                    }
                    scope.submit(datos);
                }else{
                    if(typeof(scope.alert)==='undefined'){
                        alert(msj);
                    }else{
                        scope.alert(msj);
                    }
                }
            });
                                    
            function getLabel(select,input){
                var label = document.createElement('label');
                    label.className = 'item item-input';
                    if(select==='select'){
                        label.className+= ' item-select';
                        var div = document.createElement('div');
                        div.className = 'input-label';
                        div.innerHTML = input.placeholder||input.name;
                        label.append(div);
                    }
                return label;
            }
            
            function getInp(select,input){
                var inp = document.createElement(select);
                inp.type = input.type||"text";
                inp.name = input.name;
                inp.setAttribute('ng-model','formulario.'+input.name);
                if(select==='select'){//Meter options
                    for(var i in input.options){
                        var o = document.createElement('option');
                        o.value = i;
                        o.innerHTML = input.options[i];
                        inp.append(o);
                    }
                }else{
                    inp.placeholder = input.placeholder||input.name;
                }
                return inp;
            }
        }
    };
});