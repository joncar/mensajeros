MapApp.directive('placesAutocomplete',function($window){
    return {
        restrict:"AE",
        replace:true,
        require: 'ngModel',
        template:'<input type="text" class="autocompletegoogleplace" id="autocompletegoogleplace" style="width:100%">',
        scope:{
            selectfields:'=',
            indice:'=',
            tipo:'=',
        },
        link:function(scope,element,attrs,model){
            var input = element[0];
            var autocomplete;
            autocomplete = new google.maps.places.Autocomplete(input,{types: ['geocode']});
            autocomplete.addListener('place_changed', fillInAddress);                        
            function fillInAddress() {
                // Get the place details from the autocomplete object.
                scope.$apply(function() {
                    model.$setViewValue(element.val());                
                });
                
                var place = autocomplete.getPlace();               
                scope.selectfields(place,scope.indice,scope.tipo);
              }
              element.bind('focus',function(){
                    //Disable Tap
                   var container = document.getElementsByClassName('pac-container');
                   angular.element(container).attr('data-tap-disabled', 'true');
                   angular.element(container).css('pointer-events', 'auto');
                   angular.element(container).on("click", function(){
                       var e = document.getElementsByClassName('autocompletegoogleplace').length;
                       for(var i=0;i<e;i++){
                           document.getElementsByClassName('autocompletegoogleplace')[i].blur();
                       }
                   }); 
              });              
        }
    };
});

MapApp.directive("appMap", function ($window) {
    return {
        restrict: "E",
        replace: true,        
        template: "<div data-tap-disabled='true'></div>",
        scope: {
            center: "=",        // Center point on the map (e.g. <code>{ latitude: 10, longitude: 10 }</code>).
            markers: "=",       // Array of map markers (e.g. <code>[{ lat: 10, lon: 10, name: "hello" }]</code>).
            map:'=',
            direccion:'=',
            location:'=',
            icon:'=',
            lugares:'=',
            widthmap: "=",         // Map width in pixels.
            heightmap: "=",        // Map height in pixels.
            zoom: "=",          // Zoom level (one is totally zoomed out, 25 is very much zoomed in).
            mapTypeId: "@",     // Type of tile to show on the map (roadmap, satellite, hybrid, terrain).
            panControl: "@",    // Whether to show a pan control on the map.
            zoomControl: "@",   // Whether to show a zoom control on the map.
            scaleControl: "@",   // Whether to show scale control on the map.
            normalmarkers:'=',
            clickevent:'='
        },
        link: function (scope, element, attrs,ngModelCtrl) {            
            var map;
            var positionMark = null;
            var directionsService;
            var directionsDisplay;
            var marksdrawedspoints = [];
            var el = element[0];
            el.style.width = scope.widthmap===undefined?window.innerWidth+'px':scope.widthmap;
            el.style.height = scope.heightmap===undefined?window.innerHeight-100+'px':scope.heightmap;
            // callback when google maps is loaded            
            function createMap() {
                    console.log("map: create map start");
                    var center = typeof(scope.center)!=='undefined'?new google.maps.LatLng(scope.center.lat,scope.center.lon):new google.maps.LatLng(0,0);
                    var mapOptions = {
                                zoom: parseInt(scope.zoom),
                                center: center,
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                panControl: false,
                                zoomControl: false,
                                mapTypeControl: false,
                                scaleControl: false,
                                streetViewControl: false,
                                navigationControl: false,
                                disableDefaultUI: true,
                                overviewMapControl: false
                    };
                    
                    if (!(map instanceof google.maps.Map)) {
                            map = new google.maps.Map(el, mapOptions);                            
                            directionsService = new google.maps.DirectionsService;
                            directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
                            directionsDisplay.setMap(map);
                            if(scope.direccion!==null && scope.direccion!==undefined && scope.direccion!==' '){                                
                                searchDireccion(scope.direccion);
                            }
                            if(typeof(scope.clickevent)!=='undefined'){
                                map.addListener('click',function(e){
                                   scope.clickevent(e);
                                });
                            }
                            if(scope.heightmap===undefined){
                                google.maps.event.addDomListener(window, "resize", function() {
                                    var center = map.getCenter(); 
                                    el.style.width = scope.widthmap===undefined?window.innerWidth+'px':scope.widthmap;
                                    el.style.height = scope.heightmap===undefined?window.innerHeight-100+'px':scope.heightmap;
                                    google.maps.event.trigger(map, "resize");
                                    map.setCenter(center);
                                });
                                
                                google.maps.event.addDomListener(window, "load", function() {
                                    var center = map.getCenter(); 
                                    el.style.width = scope.widthmap===undefined?window.innerWidth+'px':scope.widthmap;
                                    el.style.height = scope.heightmap===undefined?window.innerHeight-100+'px':scope.heightmap;
                                    google.maps.event.trigger(map, "resize");
                                    map.setCenter(center);
                                });
                            }
                     }
             }
             
            createMap();
            updateMarkers();

            scope.$watch('center', function() {                    
                    updateMarkers();                    
            });
            
            scope.$watch('markers', function() {                    
                    drawMarkers();
            });
            
            scope.$watch('direccion', function() {                    
                    if(scope.direccion!==null && scope.direccion!==undefined && scope.direccion!==' '){                                
                        searchDireccion(scope.direccion);
                    }
            });
            
            scope.$watch('zoom',function(){                
                map.setZoom(parseInt(scope.zoom));                
            });
            
            scope.$watch('normalmarkers',function(){                
                drawNormalMarkers();                
            });
            
            function searchDireccion(direccion){
                if (map) {
                    if(direccion!==''){
                        var geocoder = new google.maps.Geocoder();
                        var address = direccion;
                        console.log(direccion);
                        geocoder.geocode({'address': address}, function(results, status) {
                                if (status === google.maps.GeocoderStatus.OK) {
                                    console.log(results);
                                    map.setCenter(results[0].geometry.location);
                                    for(var i in results){                                    
                                        CrearMarcas(results[i]);
                                    }
                                    if(scope.lugares!==undefined){
                                        scope.lugares(results);
                                    }
                                } else {
                                  //alert('No se ha podido encontrar la ubicación indicada');
                                  //window.history.back();
                                }
                        });
                    }
                }
            }
            
            function CrearMarcas(place){
                loc = place.geometry.location;                
            }
            
            var normalmarkers = [];
            function drawNormalMarkers(){
                for(var i in normalmarkers){
                    normalmarkers[i].setMap(null);
                }
                normalmarkers = [];
                if(typeof(scope.normalmarkers)!=='undefined' && scope.normalmarkers.length>0){
                    for(var i in scope.normalmarkers){
                        var l = scope.normalmarkers[i];                                
                        var m =  new google.maps.Marker({ position: new google.maps.LatLng(l.lat,l.lon), map: map, title: l.name,icon:l.icon});
                        m.id = scope.normalmarkers[i].id;
                        m.datos = scope.normalmarkers[i];
                        normalmarkers.push(m);
                    }
                }
            }
            
            function updateMarkers() {                    
                if (map) {
                    if(typeof(scope.center)!=='undefined'){
                        map.panTo(new google.maps.LatLng(scope.center.lat,scope.center.lon));
                    }else{
                        map.panTo(new google.maps.LatLng(0,0));
                    }
                }
            }
            var markers = [];
            function drawMarkers(){
                if(map!==undefined && scope.markers !== undefined && scope.markers.length>1){
                    for(i in marksdrawedspoints){
                        marksdrawedspoints[i].setMap(null);
                    }

                    var marksdraweds = [];
                    for(var i in scope.markers){
                        l = scope.markers[i];
                        marksdraweds.push({location:new google.maps.LatLng(l.lat,l.lon),stopover:true});
                    }
                    if(marksdraweds.length>6){
                        marksdraweds.splice(6,2);
                    }                    
                    
                    if(marksdraweds.length==1){
                        marksdraweds[1] = marksdraweds[0];
                        marksdraweds[0] = {location:new google.maps.LatLng(scope.center.lat,scope.center.lon),stopover:true}                        
                    }
                    
                    if(marksdraweds.length>1){
                    directionsService.route({
                        origin: marksdraweds[0].location,
                        destination: marksdraweds[marksdraweds.length-1].location,
                        waypoints:marksdraweds,
                        travelMode: google.maps.TravelMode.DRIVING
                      }, function(response, status) {
                        if (status === google.maps.DirectionsStatus.OK) {
                          directionsDisplay.setDirections(response);
                           
                          for(i in marksdrawedspoints){
                                marksdrawedspoints[i].setMap(null);
                          }
                          for(var i in scope.markers){
                                var l = scope.markers[i];                                
                                var m =  new google.maps.Marker({ position: new google.maps.LatLng(l.lat,l.lon), map: map, title: l.name,icon:scope.markers[i].icon});
                                m.id = scope.markers[i].id;
                                m.datos = scope.markers[i];
                                marksdrawedspoints.push(m);
                            }
                          
                        } else {
                          window.alert('No se ha podido encontrar una ruta');
                        }
                      });
                    }
                }
                else if(map!==null && scope.markers !== undefined && scope.markers.length==1){
                    for(var i in markers){
                        markers[i].setMap(null);
                    }
                    markers = [];
                    for(i in scope.markers){
                        markers.push(new google.maps.Marker({
                            map:map,
                            position:new google.maps.LatLng(scope.markers[i].lat,scope.markers[i].lon),
                            icon:scope.markers[i].icon||''
                        }));
                    }
                }
            }

            // convert current location to Google maps location
            function getLocation(loc) {
                    if (loc == null) return new google.maps.LatLng(40, -73);
                    if (angular.isString(loc)) loc = scope.$eval(loc);
                    return new google.maps.LatLng(loc.lat, loc.lon);
                }

            } // end of link:
            
    }; // end of return
});