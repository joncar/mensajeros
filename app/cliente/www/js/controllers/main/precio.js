controllers.controller('Precio', function($scope,$state,$rootScope,$http,$ionicPlatform,$ionicModal,$ionicLoading,$ionicPopup,Api,UI,getDirFromGPS,strings,User) {    
    //Api.servicio = {"ciudades_id":"1","clientes_id":"7","detalles":[{"direccion":"1","direccion1":"Avenida 6 de Diciembre, Quito 170138, Ecuador","direccion2":"","telefono":"3","contacto":"2","lat":-0.1692798,"lon":-78.4763552,"status":0,"limitacion1":true,"limitacion2":true,"$$hashKey":"object:134"},{"direccion":"4","direccion1":"Jorge Erazo, Quito 170104, Ecuador","direccion2":"","telefono":"6","contacto":"5","lat":-0.143974,"lon":-78.49753399999997,"status":0,"limitacion1":true,"limitacion2":true,"$$hashKey":"object:135"}],"tipotramite":0,"fechaactual":"2016-11-17T01:06:08.264Z","hora":"2016-11-15T12:30:08.264Z","turno":"Mañana","tipo_tramite":"1","fecha_solicitud":"2016-11-16 8:0","fecha":"2016-11-15 20:36","precio":"10.00","precio_fuera":"0.65","porcentaje":[],"limite":"10","valor_paquete":1313,"descripcion":"1313"}
    if(typeof(Api.servicio)==='undefined'){
        document.location.href="main.html";
    }else{        
        console.log(JSON.stringify(Api.servicio));
        $scope.servicio = Api.servicio;
        $scope.detalles = Api.servicio.detalles;
        $scope.showAlert = UI.getShowAlert($ionicPopup);
        $scope.loading = UI.getLoadingBox($ionicLoading,'Calculando precio');
        $scope = User.getData($scope);
        console.log($scope);
        //Calcular precio        
        Api.list('ajustes',{},$scope,$http,function(data){
            $scope.ajustes = data[0];            
            $scope.calcularPrecio();
        });
        $scope.calcularPrecio = function(){
            $scope.loading('show');
            var destinations = [];
            var origen = $scope.servicio.detalles[0];
            origen = [new google.maps.LatLng(origen.lat,origen.lon)];
            for(var i=1;i<$scope.servicio.detalles.length;i++){
                var d = $scope.servicio.detalles[i];
                destinations.push(new google.maps.LatLng(d.lat,d.lon));
            }
            var service = new google.maps.DistanceMatrixService;
                service.getDistanceMatrix({
                  origins: origen,
                  destinations: destinations,
                  travelMode: google.maps.TravelMode.DRIVING,
                  unitSystem: google.maps.UnitSystem.METRIC,
                  avoidHighways: false,
                  avoidTolls: false
                }, function(response, status) {
                  if (status !== google.maps.DistanceMatrixStatus.OK) {
                    alert('Error was: ' + status);
                  } else {                
                    var distance = 0;
                    if(response.rows.length>0){                        
                        if(response.rows[0].elements.length>0){
                            var el = response.rows[0].elements;                     
                            var precio = parseFloat($scope.servicio.precio);
                            var precio_fuera = parseFloat($scope.servicio.precio_fuera);
                            var limite = parseInt($scope.servicio.limite);
                            var porcentaje = $scope.servicio.porcentaje;
                            var monto = 0;
                            var distancia = 0;
                            var recargo = 0;
                            for(var i=0;i<el.length;i++){
                                var point = $scope.servicio.detalles[i+1];
                                point.distancia = (el[0].distance.value/1000);
                                distancia+= el[0].distance.value;
                                /*monto += precio*point.distancia;
                                console.log(precio);
                                if(!point.limitacion2 && porcentaje.length>0){//Recargar porcentaje
                                    var porcentajev = 1;                                    
                                    for(var i in porcentaje){
                                        if(point.distancia >= parseFloat(porcentaje[i].desde) && point.distancia <= parseFloat(porcentaje[i].hasta)){
                                            porcentajev = porcentaje[i].porcentaje_recargo;
                                        }
                                    }
                                    porcent = point.distancia*(porcentajev/100);
                                    recargo+= porcent;
                                    monto+= porcent;
                                }*/
                                var km = distancia/1000;
                                var km_extra = 0;
                                if(km>=limite){
                                    km_extra = km-limite;
                                    km = limite;
                                }
                                monto = precio+(km_extra*precio_fuera);
                                recargo = km_extra*precio_fuera;
                            }                            
                            /*if($scope.servicio.detalles[0].lat!==undefined && $scope.servicio.detalles[0].lon !==undefined){
                                distancia+= el[0].distance.value;
                            }*/
                            $scope.servicio.precio = monto.toFixed(2);
                            $scope.servicio.distancia = distancia/1000;
                            $scope.servicio.distancia.toFixed(2);
                            $scope.servicio.precio2 = recargo.toFixed(2);
                            if(!$scope.$$phase){
                                $scope.$apply();
                            }
                        }
                    }
                  }
                });
        };
        
        $scope.add = function(){
            if($scope.procesar()){
                $state.go('tab.entrega');
            }
        };
        if($scope.convenio==='1'){
            $scope.servicio.tipo_pago='5';
        }
        $scope.solicitar = function(){
            if(typeof($scope.servicio.tipo_pago)!=='undefined'){             
                if($scope.procesar()){
                    switch($scope.servicio.tipo_pago){
                        case '1':                                         
                            $scope.servicio.status = 0;
                            $scope.servicio.tipo_pago_detalle = 'Tarjeta de Crédito';
                        break;
                        case '2':
                            $scope.servicio.status = 0;
                            $scope.servicio.tipo_pago_detalle = 'Depósito transferencia Bancaria';
                        break;
                        case '3':
                            $scope.servicio.status = 2;
                            $scope.servicio.tipo_pago_detalle = $scope.servicio.detalles[$scope.servicio.efectivo_donde].direccion;
                        break;
                        case '5':
                            $scope.servicio.status = 2;
                            $scope.servicio.tipo_pago_detalle = 'Pago por afiliación';
                        break;
                    }
                    $scope.data = $scope.servicio;                    
                    $scope.data.detalles = angular.toJson($scope.data.detalles);
                    Api.insert('pedidos',$scope,$http,function(data){
                        Api.servicio = undefined;                        
                        var id = data.insert_primary_key;
                        if($scope.servicio.tipo_pago==='1'){
                            //Ir a payphone
                            $scope.openpayphone(id);
                        }else{
                            $scope.showAlert('Su pedido ha sido solicitado. con éxito');
                            document.location.href="#/tab/rastrear/"+id
                        }
                    });
                }               
            }else{
                $scope.showAlert('Verifique los datos','Por favor elija un medio de pago para su pedido');
                return false;
            }
        };
        
        $scope.procesar = function(){
            Api.servicio = $scope.servicio;
            return true;            
        };
        
        $scope.openpayphone = function(id){
            var loc = document.location.href;
            loc = loc.split('#')[0];
            loc = loc+"#tab/rastrear/"+id;            
            gWindow = window.open('https://mensajeros.ec/pedidos/frontend/pagarMovil/'+id+'/'+btoa(loc),'_blank','location=no');
            gWindow.addEventListener('loadstop',function(event){                
                var url = event.url;
                var filename = url.substring(url.lastIndexOf('/'));
                if(filename == "/pagookMovil"){
                     gWindow.close();
                     document.location.href="#/tab/rastrear/"+id
                 }             
            });            
        };

        $ionicPlatform.ready(function(){
            if(typeof(cordova)!=='undefined'){
                window.open = cordova.InAppBrowser.open;
            }
            $scope.consultar();        
        });
        
        
        $scope.toggleGroup = function(group) {
            if ($scope.isGroupShown(group)) {
              $scope.shownGroup = null;
            } else {
              $scope.shownGroup = group;
            }
          };
          $scope.isGroupShown = function(group) {
            return $scope.shownGroup === group;
          };
        
        
    }
});