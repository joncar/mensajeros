controllers.controller('Perfil', function($scope,$rootScope,$http,$ionicPopup,$ionicLoading,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.datos = {
        nombre:$scope.nombre,
        password: $scope.password,
        celular:$scope.celular,
        telefono:$scope.telefono,
        ruc:$scope.ruc,
        direccion:$scope.direccion
    };
    $scope.send = function(datos){
        $scope.data = datos;
        $scope.data.email = $scope.email;
        Api.update('clientes',$scope.user,$scope,$http,function(data){
            $scope.showAlert('Datos almacenados con éxito');
            $scope.data = {id:$scope.user};
            Api.list('clientes',$scope.data,$scope,$http,function(data){
                datos = data[0];
                datos.clientes_id = JSON.parse(datos.admin);
                datos.clientes_id = datos.clientes_id.clientes_id;
                User.setData(datos);
            });                               
        });
    };
    
    //Subir foto
    $scope.triggerDevice = function(sourceType){
        navigator.camera.getPicture(function(imageURI){
            $scope.selectImage(imageURI);            
        },
        function(message) {  },
        {
            quality: 50, 
            destinationType: navigator.camera.DestinationType.DATA_URL,
            sourceType: sourceType,            
            targetWidth: 284, 
            targetHeight: 284,
            allowEdit: true
        }
        );
    };
    
    $scope.triggerFile = function(){        
        // An elaborate, custom popup        
        var myPopup = $ionicPopup.show({
          template: 'Elige una opción para seleccionar la imagén',
          title: 'Dispositivo Multimedia',
          subTitle: 'Elige una opción',
          scope: $scope,
          buttons: [
            {
              text: '<b>Camara</b>',              
              onTap: function(e) {
                  $scope.triggerDevice(navigator.camera.PictureSourceType.CAMERA);
                  myPopup.close();
              }
            },
            {
              text: '<b>Galería</b>',
              onTap: function(e) {
                  $scope.triggerDevice(navigator.camera.PictureSourceType.SAVEDPHOTOALBUM);
                  myPopup.close();
              }
            }
          ]
        });
    };
    
    $scope.selectImage = function(imageURI,foto){
        $scope.data = {image:"data:image/jpeg;base64,"+imageURI,user_id:$scope.user};
        $scope.loading = UI.getLoadingBox($ionicLoading,'Cargando Foto');        
        Api.query('uploadPhoto',$scope,$http,function(data){                
                data = {id:$scope.user}
                Api.list('clientes',data,$scope,$http,function(data){                     
                    data = data[0];
                    data.clientes_id = JSON.parse(data.admin);
                    data.clientes_id = data.clientes_id.clientes_id;
                    User.setData(data);
                    $scope = User.getData($scope);
                    $rootScope.$broadcast('updateTabs');
                    if(!$scope.$$phase){
                        $scope.$apply();
                    }
                    $scope.showAlert('Foto subida','Foto subida con éxito');
               },'where');
        });
        if(!$scope.$$phase){
            $scope.$apply();
        }
    };    
});