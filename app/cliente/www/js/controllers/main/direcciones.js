controllers.controller('Direcciones', function($scope,$http,Api,User,UI,$ionicPopup,$ionicLoading,$rootScope) {    
    $scope = User.getData($scope);
    $scope.pedidos = [];
    $scope.dir = [];
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.confirm = UI.getConfirmBox($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.consultar = function(){
        Api.list('favoritos',{'user_id':$scope.user},$scope,$http,function(data){
            $scope.pedidos = data;
            for(var i in data){
                $scope.dir.push({nombre:data[i].nombre});
            }
            if(!$scope.$$phase){
                $scope.$apply();
            }
        });
    };
    $scope.editar = function(id,nombre){
        if(nombre!==undefined && nombre!==''){
            $scope.data = {nombre:nombre};
            Api.update('favoritos',id,$scope,$http,function(data){
               $scope.showAlert('Edicion','Direccion editada con exito');
            });
        }
    };
    
    $scope.borrar = function(id){
        $scope.confirm('Eliminar direccion','Esta usted seguro que desea eliminar esta direccion favorita?',function(){
                 Api.deleterow('favoritos',id,$scope,$http,function(data){
                    $scope.showAlert('Edicion','Direccion editada con exito');
                    $scope.consultar();
                 });
        });
    };
    
    $scope.select = function(i){
        $rootScope.$broadcast('selectFav',{
            lat:i.lat,
            lon:i.lon,
            dir:i.direccion1,
            direccion:i.direccion,
            contacto:i.contacto,
            telefono:i.telefono
        });
    };
    $scope.consultar();
});