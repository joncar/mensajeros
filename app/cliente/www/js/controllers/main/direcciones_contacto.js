controllers.controller('Direcciones_contacto', function($scope,$state,$rootScope,$http,$ionicModal,$ionicLoading,$ionicPopup,Api,UI,getDirFromGPS,strings) {    
    //Api.servicio = {"ciudades_id":"1","clientes_id":"7","detalles":[{"limitacion1":true,"limitacion2":true,"direccion1":"Tomás Bermur (N14b), Quito 170135, Ecuador","direccion2":"","lat":-0.1753073,"lon":-78.47165890000002,"status":0,"direccion":"Alguno","contacto":"Alguno","telefono":"Alguno"},{"limitacion1":true,"limitacion2":true,"lat":-0.2084728,"lon":-78.48572239999999,"direccion1":"Madrid, Quito 170143, Ecuador","direccion2":"","status":0}]};
    if(typeof(Api.servicio)==='undefined'){
        document.location.href="main.html";
    }else{        
        //Nuevo pedido 
        $scope.showAlert = UI.getShowAlert($ionicPopup);
        console.log(JSON.stringify(Api.servicio));
        $scope.dir = Api.servicio.detalles[Api.servicio.detalles.length-1];
        $scope.basel = {
            lat:$scope.dir.lat,
            lon:$scope.dir.lon
        };
        $scope.iconMap='https://mensajeros.ec/img/clientemark.png';
        $scope.zoomMap = 17;
        $scope.servicio = Api.servicio;
        $scope.markers = [$scope.basel];
        $scope.markers[0].icon = $scope.iconMap;
        if(!$scope.$$phase){
            $scope.$apply();
        }
        
        $scope.solicitar = function(){
            if(typeof($scope.dir.contacto)!=='undefined' && $scope.dir.contacto!=='' && typeof($scope.dir.telefono)!=='undefined' && $scope.dir.telefono!=='' && typeof($scope.dir.direccion)!=='undefined' && $scope.dir.direccion!==''){
                Api.servicio.detalles[Api.servicio.detalles.length-1] = $scope.dir;
                if(!Api.ready){
                    $state.go('tab.entrega');
                }else{
                    $state.go('tab.confirmar');
                }
            }else{
                $scope.showAlert('Complete los datos','Por favor complete los datos solicitados para continuar con su solicitud');
            }
        };
    }
});