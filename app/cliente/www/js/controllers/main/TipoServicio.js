controllers.controller('TipoServicio', function($scope,$state,$rootScope,$http,$ionicPlatform,$ionicModal,$ionicLoading,$ionicPopup,Api,UI,getDirFromGPS,strings) {    
    //Api.servicio = {"detalles":[{"lat":-0.1806532,"lon":-78.46783820000002,"direccion":"El Batan &  Quito","status":0}]}
    if(typeof(Api.servicio)==='undefined'){
        document.location.href="main.html";
    }else{  
        console.log(JSON.stringify(Api.servicio));
        console.log(Api.servicio);
        //UI
        $scope.showAlert = UI.getShowAlert($ionicPopup);
        $scope.loading = UI.getLoadingBox($ionicLoading),    
        //Configuration
        $scope.servicio = Api.servicio;
        $scope.servicio.tipotramite = 0;
        $scope.basel = {lat:localStorage.lat||0,lon:localStorage.lon||0};
        $scope.dir = {direccion1:'',direccion2:''};
        $scope.direccion = '';
        $scope.task = [];
        $scope.zoomMap = 17;
        $scope.markers = [];        
        $scope.innerheight = localStorage.innerHeight;
        $scope.iconMap = 'https://mensajeros.ec/img/repartidormark.png';
        $scope.zoomMap = JSON.parse(localStorage.ciudades)[0].zoom2;
        
        Api.list('tipos_servicios',{},$scope,$http,function(data){
            for(var i in data){
                data[i].descripcion = JSON.parse(data[i].descripcion)
            }
            $scope.tiposServicios = data;
        });
                
        $scope.validarHora = function(){
            var mod = false;
            if(typeof($scope.servicio.hora)!=='undefined'){
                    //Hora elegida
                    var horaelegida = $scope.servicio.hora.getHours();
                    //Si no esta dentro del rango lo metemos en el mismo
                    if(horaelegida<8 || (horaelegida>12 && horaelegida<14) || horaelegida>18){
                        horaelegida = horaelegida<12?8:horaelegida<14?14:horaelegida>=18?8:18;
                        var horafechaelegida = new Date();
                        horafechaelegida.setHours(horaelegida);
                        horafechaelegida.setMinutes(0);
                        $scope.servicio.hora = new Date(horafechaelegida.getTime());
                        mod = true;
                    }                    
                                        
                    var hora = new Date();
                    hora.setHours($scope.servicio.hora.getHours());
                    hora.setMinutes($scope.servicio.hora.getMinutes());
                    var fecha = $scope.servicio.fechaactual;
                    
                    var ahora = new Date();
                    var fechaf = fecha.getFullYear()+'-'+(fecha.getMonth()+1)+'-'+fecha.getDate();
                    var ahoraf = ahora.getFullYear()+'-'+(ahora.getMonth()+1)+'-'+ahora.getDate();
                    //Añadimos una hora como minimo de limite                    
                    ahora = new Date($scope.servicio.fechaactual.getTime()+60*60*1000);                                        
                    if($scope.servicio.fechaactual!==undefined && fechaf===ahoraf && hora<ahora){                        
                        $scope.servicio.hora = ahora;
                        hora = ahora;
                        mod = true;
                    }
                    if(mod){
                        //$scope.showAlert('Modificación','El horario de servicio express es desde las 07H00 hasta las 17H00');
                    }
            }
        };
        
        $scope.servicio.fechaactual = new Date();
        $scope.servicio.hora = new Date($scope.servicio.fechaactual.getTime()+60*60*1000);
        
        //Validaciones de hora
        $scope.validarTurno = function(){
          var mod = false;
          var ahora = new Date();
          var fecha = $scope.servicio.fechaactual;
          if(fecha<ahora){
              $scope.servicio.fechaactual = ahora;
              mod = true;
              fecha = ahora;
          }
          
          var fechaf = fecha.getFullYear()+'-'+(fecha.getMonth()+1)+'-'+fecha.getDate();
          var ahoraf = ahora.getFullYear()+'-'+(ahora.getMonth()+1)+'-'+ahora.getDate();
          
          if(ahoraf===fechaf){
            if(ahora.getHours()>=8 && ahora.getHours()<=13){
                $scope.turno = 1;
            }else if(ahora.getHours()>=14 && ahora.getHours()<18){
                $scope.turno = 2;
            }else{
                if(ahora.getHours()>=18){
                    var fecha = new Date();
                    $scope.servicio.fechaactual = new Date(fecha.getTime()+24*60*60*1000); //Añadimos 1 dia 
                    mod = true;
                    $scope.validarTurno();
                }else{
                    $scope.servicio.fechaactual = new Date();
                }
                 
                $scope.turno = 1;
            }
          }else{
              $scope.turno = 1;
          }
          if(mod){
                //$scope.showAlert('Modificación','El horario de servicio express es desde las 07H00 hasta las 17H00');
          }
          $scope.validarHora();
        };        
        $scope.validarTurno();
        
        $scope.solicitar = function(){            
            if(typeof($scope.servicio.fechaactual)!=='undefined'){
                console.log($scope.servicio.tipotramite);
                console.log($scope.servicio.hora);
                if(($scope.servicio.tipotramite==0 && typeof($scope.servicio.turno)!=='undefined') || ($scope.servicio.tipotramite==1 && typeof($scope.servicio.hora)!=='undefined')){
                    if($scope.turno!==3){
                        $scope.servicio.tipo_tramite = $scope.tiposServicios[$scope.servicio.tipotramite].id;
                        var fecha = $scope.servicio.fechaactual;
                        var fecha2 = new Date();
                        var hora = $scope.servicio.hora;
                        if(typeof(fecha)!=='string'){
                            hora = hora||new Date();
                            $scope.servicio.fecha_solicitud = fecha.getFullYear()+'-'+(fecha.getMonth()+1)+'-'+fecha.getDate()+' '+hora.getHours()+':'+hora.getMinutes();
                            $scope.servicio.fecha = fecha2.getFullYear()+'-'+(fecha2.getMonth()+1)+'-'+fecha2.getDate()+' '+fecha2.getHours()+':'+fecha2.getMinutes();
                        }
                        $scope.servicio.precio = $scope.tiposServicios[$scope.servicio.tipotramite].precio;
                        $scope.servicio.precio_fuera = $scope.tiposServicios[$scope.servicio.tipotramite].precio_fuera;
                        $scope.servicio.porcentaje = $scope.tiposServicios[$scope.servicio.tipotramite].descripcion.porcentaje;
                        $scope.servicio.limite = $scope.tiposServicios[$scope.servicio.tipotramite].limite;
                        Api.servicio = $scope.servicio;
                        console.log(Api.servicio);
                        $state.go('tab.paquete');
                    }else{
                        $scope.showAlert('Fuera de Rango','Disculpe, pero nuestro horario de atención es de (08 AM - 18 PM)');
                    }
                }else{
                    $scope.showAlert('Verifique los datos','Por favor complete los datos solicitados antes de continuar.');
                }
            }else{
                $scope.showAlert('Verifique los datos','Por favor complete los datos solicitados antes de continuar.');
            }
        };
    }
    
    $scope.pickExpress = function(){
        var fecha = new Date();
        var horaelegida = fecha.getHours();
        if(horaelegida<8 || (horaelegida>12 && horaelegida<14) || horaelegida>17){
            $scope.servicio.tipotramite = 0;
            $scope.showAlert('Modificación','El horario de servicio express es desde las 07H00 hasta las 17H00');
        }else{
            $scope.servicio.tipotramite = 1;
        }
    };
});