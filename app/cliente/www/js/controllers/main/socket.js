controllers.controller('Socket', function($scope,$rootScope,$http,$ionicPopup,$ionicPlatform,$ionicSideMenuDelegate,getDirFromGPS,Api,User,oneSignal){
    $scope = User.getData($scope); 
    $rootScope.$on('updateTabs',function(){
        $scope = User.getData($scope); 
        if(!$scope.$$phase){
            $scope.$apply();
        }
    });
    $scope.showMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };
    $scope.desconectar = function(){
        User.cleanData();
        document.location.href="index.html";
        navigator.app.exitApp();
    };
    var timeout = null;
    $scope.goPerfil = function(){
        document.location.href='#/tab/perfil';
        $ionicSideMenuDelegate.toggleLeft();
    }
    $scope.consultar = function(){
        /*getDirFromGPS.native = true;        
        $scope.data = {'repartidores_id':$scope.repartidores_id};
        Api.query('getUpdates',$scope,$http,function(data){
            $rootScope.$broadcast('onUpdate',data);
            console.log(data);
            if(typeof(localStorage.disponible)==='undefined' || localStorage.disponible==='1'){
                getDirFromGPS.native = true;
                getDirFromGPS.getDirFromGps(function(){});
            }else{
                 getDirFromGPS.stopNativeGps(function(){});
            }
        },'where');
        timeout = setTimeout($scope.consultar,10000);*/
    };
    
    $rootScope.$on('triggerUpdate',function(){
       clearTimeout(timeout);
       $scope.consultar(); 
    });
    
    $ionicPlatform.ready(function(){
        //Existe conexión?
        if(window.Connection) {
            if(navigator.connection.type === Connection.NONE) {
                $scope.showAlert('¿Haz perdido la conexión a internet?, necesitas conectarte para poder seguir usando la aplicación');
            }
        }
        oneSignal.init($scope);        
        $scope.consultar();
    });        
});