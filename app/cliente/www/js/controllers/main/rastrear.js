controllers.controller('Rastrear', function($scope,$rootScope,$ionicPlatform, $http,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.pedidos = [];
    $scope.guia = 'consultando';
    $scope.basel = {lat:localStorage.lat||0,lon:localStorage.lon||0};
    $scope.zoomMap = 13;
    $scope.iconMap = 'https://mensajeros.ec/img/repartidormark.png';
    $scope.innerheight = localStorage.innerHeight;
    $scope.normalmarkers = [];
    $rootScope.$on('$stateChangeStart',function(){
        if(typeof(Api.timer)!=='undefined'){
            clearTimeout(Api.timer);
        }
    });
    Api.list('ajustes',{},$scope,$http,function(data){
        $scope.ajustes = data[0]; 
    });
        
    $scope.consultar = function(){
        Api.list('pedidos',{'id':$stateParams.id},$scope,$http,function(data){
            for(i in data){
                data[i].descripcion = JSON.parse(data[i].descripcion);
                data[i].sdfe80649 = data[i].sdfe80649.split('|');
            }
            $scope.pedidos = data;
            if($scope.pedidos.length>0){
            $scope.pedidos[0].status = parseInt($scope.pedidos[0].status);
                for(var i in $scope.pedidos){
                    var foto = 'https://mensajeros.ec/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';                
                    if($scope.pedidos[i].repartidores_id!==null){
                        var d = $scope.pedidos[i].sdfe80649;
                        if(d.length===3){
                            foto = 'http://mensajeros.ec/img/fotos/'+d[2];
                        }
                    }
                    $scope.pedidos[i].foto = foto;
                }
                $scope.pedido = $scope.pedidos[0];
                $scope.guia = $scope.pedido.guia;
                if(!$scope.$$phase){                    
                    $scope.$apply('pedido');
                    $scope.$apply('guia');
                    $scope.$apply();
                }
            }            
            
            $scope.drawMarker();
        });
    };
    
    $scope.transformarStatus = function(pedido){
        switch(pedido.tipo_tramite){
            case '1':
            case '2':
                switch(pedido.status){
                    case '1': return 'Esperando Pago'; break;
                    case '2': return 'Mensajero Asignado';  break;
                    case '3': return 'Paquete Recogido';  break;
                    case '4': return 'En Tránsito';  break;
                    case '5': return 'Entregado';  break;
                }
            break;
            case '3':
                switch(pedido.status){
                    case '1': return 'Esperando Pago'; break;
                    case '2': return 'Mensajero Asignado';  break;
                    case '3': return 'Trámite en proceso';  break;
                    case '5': return 'Trámite Finalizado';  break;
                }
            break;
        }
    };
    
    $scope.transformarSolicitud = function(pedido){
        switch(pedido.tipo_tramite){
            case '1':
                return 'Estandar';
            break;
            case '2':
                return 'Express';
            break;
            case '3':
                return 'Tramite';
            break;
        }
    };
    
    var draweds = false;
    $scope.drawMarker = function(){
            if($scope.pedidos.length>0){
                if(!draweds){
                    $scope.markers = [];
                    for(var i in $scope.pedidos[0].descripcion.tareas){
                        var tarea =  $scope.pedidos[0].descripcion.tareas[i];
                        $scope.markers.push({lat:tarea.lat,lon:tarea.lon,icon:'https://mensajeros.ec/img/clientemark.png'});
                    }  
                    draweds = true;
                }
            }
            $scope.updateMensajero();
    };
    
    $scope.updateMensajero = function(){
        if($scope.pedidos.length>0){
            Api.list('mensajeros',{'id':$scope.pedidos[0].repartidores_id},$scope,$http,function(data){
                if(data.length>0){
                    data = data[0];                
                    $scope.normalmarkers = [{lat:data.lat,lon:data.lon,icon:'https://mensajeros.ec/img/repartidormark.png'}];
                }
            });
            Api.timer = setTimeout($scope.consultar,2000);
        }
    };
    
    $scope.calificar = function(){
        document.location.href='#/tab/calificar/'+$scope.pedidos[0].id;
    };
    
    $scope.buscarRepartidor = function(){
        $scope.basel = $scope.normalmarkers[0];
    };
    
    $scope.openpayphone = function(){
        window.open('https://mensajeros.ec/pedidos/frontend/pagarMovil/'+ $scope.pedidos[0].id+'/'+btoa(document.location),'_blank','location=no');
    };
    
    $ionicPlatform.ready(function(){
        if(typeof(cordova)!=='undefined'){
            window.open = cordova.InAppBrowser.open;
        }
        $scope.consultar();        
    });
        
});