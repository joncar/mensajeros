controllers.controller('Calificar', function($scope,$http,$stateParams,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.consultar = function(){
       Api.list('pedidos',{'id':$stateParams.id},$scope,$http,function(data){
            for(i in data){
                data[i].descripcion = JSON.parse(data[i].descripcion);
                data[i].sdfe80649 = data[i].sdfe80649.split('|');
            }
            $scope.pedidos = data;
            $scope.pedidos[0].status = parseInt($scope.pedidos[0].status);
            for(var i in $scope.pedidos){
                var foto = 'https://mensajeros.ec/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';                
                if($scope.pedidos[i].repartidores_id!==null){
                    var d = $scope.pedidos[i].sdfe80649;
                    if(d.length===3){
                        foto = 'http://mensajeros.ec/img/fotos/'+d[2];
                    }
                }
                $scope.pedidos[i].foto = foto;
                console.log($scope.pedidos)
            }
        });
    };    
    $scope.datos = {
        calificacion:'',
        comentario:''
    };
    $scope.calificar = function(calificacion,comentario){
        $scope.data = $scope.datos;        
        $scope.data.repartidores_id =$scope.pedidos[0].repartidores_id;
        console.log($scope.data);
        Api.insert('calificaciones',$scope,$http,function(data){
            $scope.data = {status:6};
            Api.update('pedidos',$stateParams.id,$scope,$http,function(){
                $scope.showAlert('Calificacion completada','Gracias por calificar a nuestro mensajero, Tu opinion es muy importante');
                document.location.href="#/tab/main";
            });
        });
    };
    $scope.consultar();
});