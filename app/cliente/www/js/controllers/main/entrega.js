controllers.controller('entrega', function($scope,$state,$rootScope,$http,$ionicPlatform,$ionicModal,$ionicLoading,$ionicPopup,Api,UI,getDirFromGPS,strings) {    
    //Api.servicio = {"ciudades_id":"1","clientes_id":"7","detalles":[{"limitacion1":true,"limitacion2":true,"direccion1":"Tomás Bermur (N14b), Quito 170135, Ecuador","direccion2":"","lat":-0.1753073,"lon":-78.47165890000002,"status":0,"direccion":"Alguno","contacto":"Alguno","telefono":"Alguno"}]};
    if(typeof(Api.servicio)==='undefined'){
        document.location.href="main.html";
    }else{
        console.log(JSON.stringify(Api.servicio));
        $scope.bindInputs = function(){
            if(typeof($scope.dir.direccion1)!=='undefined' && $scope.dir.direccion1!==''){
                var dire = strings.ucwords($scope.dir.direccion1);
                if(typeof($scope.dir.direccion2)!=='undefined' && $scope.dir.direccion2!==''){
                    dire+= ' & '+strings.ucwords($scope.dir.direccion2);
                }
                $scope.direccion = dire+' '+'Quito';
                $scope.dir.direccion1 = strings.ucwords($scope.dir.direccion1);
                $scope.dir.direccion2 = strings.ucwords($scope.dir.direccion2);
                $scope.zoomMap = JSON.parse(localStorage.ciudades)[0].zoom;
            }
        };
        
        $scope.nada = function(){
            return false;  
          };
    
        $scope.bindGPS = function(){
            $scope.loading('show','Buscando ubicación del gps');
            setTimeout(function(){$scope.loading('hide');},3000);
            getDirFromGPS.getDirFromGps(function(lat,lon,dir){                
                $scope.dir.direccion1 = dir.formatted_address;
                //$scope.direccion = dir.formatted_address;
                $scope.basel = {lat:lat,lon:lon,icon:'https://mensajeros.ec/img/clientemark.png'};
                $scope.zoomMap = JSON.parse(localStorage.ciudades)[0].zoom;
                $scope.drawMarkers();
                $scope.marker.push($scope.basel);
                $scope.markers = $scope.marker;
                $scope.fav = false;
                if(!$scope.$$phase){
                    $scope.$apply();
                }
            });

        };
        
        $scope.clickevent = function(e){        
            getDirFromGPS.getLocationName(e.latLng.lat(),e.latLng.lng(),function(lat,lon,dir){
                console.log(dir);
                $scope.dir.direccion1 = dir.formatted_address;
                //$scope.direccion = dir.formatted_address;
                $scope.zoomMap = JSON.parse(localStorage.ciudades)[0].zoom;
                $scope.basel = {lat:lat,lon:lon,icon:'https://mensajeros.ec/img/clientemark.png'};
                $scope.markers = [$scope.basel];
                $scope.fav = false;
                if(!$scope.$$phase){
                    $scope.$apply();
                }
            });
        };
    
        $scope.bindLatLonFromInput = function(results){
            if(results.length>0){
                $scope.basel = {
                    lat:results[0].geometry.location.lat(),
                    lon:results[0].geometry.location.lng(),
                    icon:'https://mensajeros.ec/img/clientemark.png'
                };
                $scope.drawMarkers();
                $scope.marker = [$scope.basel];
                $scope.markers = $scope.marker;
                //$scope.fav = false;
            }else{
                $scope.showAlert('Ubicación no encontrada','Lo sentimos pero no hemos podido encontrar la ubicación que ha escrito, por favor utilice otras palabras o algún punto cercano');
            }
        };
        //UI
        $scope.dir = {
            direccion1:'',
            direccion2:''
        };
        $scope.buttonlabel = 'CONFIRMAR ENTREGA';
        $scope.showAlert = UI.getShowAlert($ionicPopup);
        $scope.loading = UI.getLoadingBox($ionicLoading),    
        //Configuration
        $scope.servicio = Api.servicio;        
        $scope.basel = {lat:localStorage.lat||0,lon:localStorage.lon||0};
        $scope.markers = [];
        $scope.dir = {direccion1:'',direccion2:''};
        $scope.direccion = '';        
        $scope.zoomMap = 17;
        $scope.innerheight = localStorage.innerHeight;
        $scope.iconMap = 'https://mensajeros.ec/img/repartidormark.png';
        $scope.zoomMap = JSON.parse(localStorage.ciudades)[0].zoom2; 
        $scope.fav = false;
        $scope.puedemarcar = function(lat,lng){
            var latLng = new google.maps.LatLng(lat,lng);
            var ciudad = JSON.parse(localStorage.ciudades)[0];
            var lim1 = new google.maps.Polygon({paths: JSON.parse(ciudad.json_limitacion)});
            var lim2 = new google.maps.Polygon({paths: JSON.parse(ciudad.json_limitacion2)});
            var result1 = google.maps.geometry.poly.containsLocation(latLng,lim1);
            var result2 = google.maps.geometry.poly.containsLocation(latLng, lim2);
            if(!result1 && !result2){
                $scope.showAlert('Limitacion geografica excedida','Por favor marque una región dentro de la ciudad de Quito');
                return false;
            }

            $scope.limite1 = result1;
            $scope.limite2 = result2;

            return true;
        };
        
        $scope.solicitar = function(){
            if($scope.puedemarcar($scope.basel.lat,$scope.basel.lon)){
                if(typeof($scope.dir.direccion1)!=='undefined' && $scope.dir.direccion1!==''){                    
                    Api.servicio.detalles.push({limitacion1:$scope.limite1,limitacion2:$scope.limite2,lat:$scope.basel.lat,lon:$scope.basel.lon,direccion1:$scope.dir.direccion1,direccion2:$scope.dir.direccion2,status:0});
                    Api.ready = true;
                    if(!$scope.fav){
                        document.location.href="#/tab/direcciones_contacto";
                    }else{
                        newData = Api.servicio.detalles[Api.servicio.detalles.length-1];
                        newData.direccion = $scope.dir.direccion;
                        newData.contacto = $scope.dir.contacto;
                        newData.telefono = $scope.dir.telefono;
                        Api.servicio.detalles[Api.servicio.detalles.length-1] = newData;
                        $state.go('tab.confirmar');
                    }                    

                }else{
                    $scope.showAlert('Verifique los datos','Por favor introduzca una dirección, elija una dirección favorita o marque la ubicación de su GPS.');
                }
            }
        };
        
        $scope.drawMarkers = function(){
            $scope.marker = [];
            for(var i in $scope.servicio.detalles){
                $scope.marker.push({lat:$scope.servicio.detalles[i].lat,lon:$scope.servicio.detalles[i].lon,icon:'https://mensajeros.ec/img/clientemark.png'});
            }
            
        };
        $scope.drawMarkers();
        
        $scope.pickFav = function(){
            UI.getModalBox($ionicModal,'templates/main/modal/fav.html',$scope,function(modal){
                $scope.modal = modal;
                $scope.modal.show();
            });
        };

        $scope.$on('selectFav',function(evt,lat){
            if($scope.puedemarcar(lat.lat,lat.lon)){
                $scope.zoomMap = JSON.parse(localStorage.ciudades)[0].zoom;
                $scope.basel = {
                    lat:lat.lat,
                    lon:lat.lon,
                    icon:'https://mensajeros.ec/img/clientemark.png'
                };
                $scope.markers = [];
                $scope.dir.direccion1 = lat.dir;
                $scope.dir.direccion = lat.direccion;
                $scope.dir.contacto = lat.contacto;
                $scope.dir.telefono = lat.telefono;
                $scope.fav = true;
                $scope.direccion = lat.dir;
                $scope.modal.hide();
                if(!$scope.$$phase){
                    $scope.$apply();
                }
            };
        });
    }
});