controllers.controller('Pedidos', function($scope,$http,$ionicPopup,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.pedidos = [];
    $scope.consultar = function(){
        Api.list('pedidos',{'clientes_id':$scope.clientes_id},$scope,$http,function(data){
            for(i in data){
                data[i].descripcion = JSON.parse(data[i].descripcion);
                data[i].sdfe80649 = data[i].sdfe80649.split('|');
            }
            $scope.pedidos = data;
            console.log($scope.pedidos);
            for(var i in $scope.pedidos){
                var foto = 'https://mensajeros.ec/assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';                
                if($scope.pedidos[i].repartidores_id!==null){
                    var d = $scope.pedidos[i].sdfe80649;
                    if(d.length===3){
                        foto = 'http://mensajeros.ec/img/fotos/'+d[2];
                    }
                }
                $scope.pedidos[i].foto = foto;
                if(!$scope.$$phase){
                    $scope.$apply();
                }
            }
        });
    };
    
    $scope.transformarStatus = function(pedido){
        switch(pedido.tipo_tramite){
            case '1':
            case '2':
                switch(pedido.status){
                    case '1': return 'Esperando Pago'; break;
                    case '2': return 'Mensajero Asignado';  break;
                    case '3': return 'Paquete Recogido';  break;
                    case '4': return 'En Tránsito';  break;
                    case '5': return 'Entregado';  break;
                }
            break;
            case '3':
                switch(pedido.status){
                    case '1': return 'Esperando Pago'; break;
                    case '2': return 'Mensajero Asignado';  break;
                    case '3': return 'Trámite en proceso';  break;
                    case '5': return 'Trámite Finalizado';  break;
                }
            break;
        }
    };
    
    $scope.transformarSolicitud = function(pedido){
        switch(pedido.tipo_tramite){
            case '1':
                return 'Estandar';
            break;
            case '2':
                return 'Express';
            break;
            case '3':
                return 'Tramite';
            break;
        }
    };
    
    $scope.goRastrear = function(item){
        document.location.href="#tab/rastrear/"+item.id;
    };
    
    $scope.consultar();
});