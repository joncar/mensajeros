controllers.controller('Confirmar', function($scope,$state,$rootScope,$http,$ionicModal,$ionicLoading,$ionicPopup,Api,UI,getDirFromGPS,strings) {    
    //Api.servicio = {"ciudades_id":"1","clientes_id":"7","detalles":[{"limitacion1":true,"limitacion2":true,"direccion1":"Avenida 6 de Diciembre, Quito 170138, Ecuador","direccion2":"","lat":-0.1692798,"lon":-78.4763552,"status":0,"direccion":"1","contacto":"2","telefono":"3"},{"limitacion1":true,"limitacion2":true,"lat":-0.1713728,"lon":-78.48617689999998,"direccion1":"Conor, Quito 170135, Ecuador","direccion2":"","status":0,"direccion":"3","contacto":"4","telefono":"5"}]}
    if(typeof(Api.servicio)==='undefined'){
        document.location.href="main.html";
    }else{        
        //Nuevo pedido 
        console.log(JSON.stringify(Api.servicio));
        console.log(Api.servicio);
        
        $scope.dir = Api.servicio.detalles[Api.servicio.detalles.length-1];
        $scope.basel = {
            lat:$scope.dir.lat,
            lon:$scope.dir.lon
        };
        $scope.iconMap='https://mensajeros.ec/img/clientemark.png';
        $scope.zoomMap = 17;
        $scope.servicio = Api.servicio;
        $scope.markers = [];
        for(var i in $scope.servicio.detalles){
            var x = {
                lat:$scope.servicio.detalles[i].lat,
                lon:$scope.servicio.detalles[i].lon,
                icon:$scope.iconMap
            };
            $scope.markers.push(x);
        }
        if(!$scope.$$phase){
            $scope.$apply();
        }
        
        $scope.add = function(){
            Api.servicio = $scope.servicio;
            $state.go('tab.entrega');
        };
        
        $scope.solicitar = function(){ 
            
                //Meter el priemr viaje como ultimo
                var de = [];
                for(var i in $scope.servicio.detalles){
                    de.push({
                        direccion:$scope.servicio.detalles[i].direccion,
                        direccion1:$scope.servicio.detalles[i].direccion1,
                        direccion2:$scope.servicio.detalles[i].direccion2,
                        telefono:$scope.servicio.detalles[i].telefono,
                        contacto:$scope.servicio.detalles[i].contacto,
                        lat:$scope.servicio.detalles[i].lat,
                        lon:$scope.servicio.detalles[i].lon,
                        status:$scope.servicio.detalles[i].status,
                        limitacion1:$scope.servicio.detalles[i].limitacion1,
                        limitacion2:$scope.servicio.detalles[i].limitacion2
                    });
                }
                if($scope.servicio.ida_vuelta){
                    var sr = $scope.servicio.detalles[0];                
                    de.push({
                        direccion:sr.direccion,
                        direccion1:sr.direccion1,
                        direccion2:sr.direccion2,
                        telefono:sr.telefono,
                        contacto:sr.contacto,
                        lat:sr.lat,
                        lon:sr.lon,
                        status:sr.status,
                        limitacion1:sr.limitacion1,
                        limitacion2:sr.limitacion2
                    });
                    
                }
            $scope.servicio.detalles = de;
            Api.servicio = $scope.servicio;
            console.log($scope.servicio);
            $state.go('tab.tipoServicio');
        };
    }
});