controller.controller('Registrar', function($scope,$http,$ionicLoading,$ionicPopup,User,Api,UI,Formularios) {
    $scope.loading = UI.getLoadingBox($ionicLoading);       
    $scope.showAlert = UI.getShowAlert($ionicPopup);              
    $scope = Formularios.registro($scope);
    $scope.registrar = function(param){
        $scope.consultar(param.email,function(data){
            if(data.length===0){
                //Registramos
                $scope.data = param;
                Api.insert('clientes',$scope,$http,function(data){
                    $scope.showAlert('Usuario registrado con éxito');
                    $scope.consultar(param.email,function(datos){
                        datos = datos[0];
                        datos.clientes_id = JSON.parse(datos.admin);
                        datos.clientes_id = datos.clientes_id.clientes_id;
                        User.setData(datos);
                        document.location.href="main.html";
                    });                    
                });
            }else{
                $scope.showAlert('El usuario ya se encuentra registrado');
            }
        });
    };
    
    $scope.consultar = function(email,callback){
        $scope.data = {email:email};
        Api.list('clientes',$scope.data,$scope,$http,function(data){
            callback(data);
        });
    };
});