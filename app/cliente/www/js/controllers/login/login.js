var controller = angular.module('starter.controllers', []);

controller.controller('Login', function($scope,$http,$ionicLoading,$ionicPopup,User,Api,UI) {
       $scope.loading = UI.getLoadingBox($ionicLoading);       
       $scope.showAlert = UI.getShowAlert($ionicPopup);       
       $scope.validEnter = function(){
            if(localStorage.email!==undefined){                     
               $scope = User.getData($scope);               
               document.location.href="main.html";
            }
       };

       $scope.login = function(param){
           if(param!==undefined){
                $scope.email = param.email;
                $scope.password = param.password;
                $scope.isRegister();
            }
       };

       $scope.isRegister = function(){
           data =  {email:$scope.email,password:$scope.password};            
           Api.list('clientes',data,$scope,$http,function(data){
                 if(data.length==0){                   
                    $scope.showAlert('Inicio de sesión','Email o Contraseña Incorrecta');
                 }
                 else{
                       data = data[0];
                       data.clientes_id = JSON.parse(data.admin);
                       data.convenio = data.clientes_id.convenio;
                       data.clientes_id = data.clientes_id.clientes_id;
                       User.setData(data);
                       document.location.href="main.html";
                 }
           },'where');         
       }
       $scope.validEnter();
})

.controller('Recover', function($scope,$http,$ionicLoading,$ionicPopup,User,Api,UI) {
       $scope.loading = UI.getLoadingBox($ionicLoading);       
       $scope.showAlert = UI.getShowAlert($ionicPopup);
       
       $scope.login = function(data){
            $scope.data = {email:data.email};
            Api.query('recover',$scope,$http,function(datos){                
                if(datos=='1'){
                    $scope.showAlert('Reestablecimiento de contraseña','Su contraseña ha sido reseteada, por favor verifique su correo para continuar con el proceso de activación');
                    document.location.href="index.html";
                }else{
                    $scope.showAlert('Recuperación de contraseña',datos);                    
                }
            });
       };
});
