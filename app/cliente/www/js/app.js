// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var MapApp = angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/main/tabs.html',
    controller: 'Socket'
  })

  // Each tab has its own nav history stack:

  .state('tab.main', {
    url: '/main',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/main.html',
        controller:'Main'
      }
    }
  })
  
  .state('tab.tipoServicio', {
    url: '/tipoServicio',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/solicitar/tipoServicio.html',
        controller:'TipoServicio'
      }
    }
  })
 
  .state('tab.entrega', {
    url: '/entrega',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/main.html',
        controller:'entrega'
      }
    }
  })
  
  .state('tab.paquete', {
    url: '/paquete',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/solicitar/paquete.html',
        controller:'Paquete'
      }
    }
  })
  
  .state('tab.precio', {
    url: '/precio',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/solicitar/precio.html',
        controller:'Precio'
      }
    }
  })
  
 .state('tab.pedidos', {
    url: '/pedidos',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/pedidos.html',
        controller:'Pedidos'
      }
    }
  })
  
  .state('tab.rastrear', {
    url: '/rastrear/:id',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/rastrear.html',
        controller:'Rastrear'
      }
    }
  })
  
  .state('tab.calificar', {
    url: '/calificar/:id',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/calificar.html',
        controller:'Calificar'
      }
    }
  })
  
  .state('tab.direcciones', {
    url: '/direcciones',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/direcciones.html',
        controller:'Direcciones'
      }
    }
  })
  
  .state('tab.direcciones_contacto', {
    url: '/direcciones_contacto',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/solicitar/direcciones_contacto.html',
        controller:'Direcciones_contacto'
      }
    }
  })
  
  .state('tab.confirmar', {
    url: '/confirmar',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/solicitar/confirmar.html',
        controller:'Confirmar'
      }
    }
  })
  .state('tab.pagos', {
    url: '/pagos',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/pagos.html',
        controller:'Pagos'
      }
    }
  })
  
  .state('tab.perfil', {
    url: '/perfil',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main/perfil.html',
        controller:'Perfil'
      }
    }
  })
  
 .state('tour', {
    url: '/tour',
    cache:false,
    templateUrl: 'templates/tour/tour.html',
    controller: 'Tour'
  })
  
  .state('tour.tour', {
    url: '/tour',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/tour/tour1.html',
        controller:'Tour'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/main');

});
