services.factory('appKeepRunning',function(){
    return {
            init:function(msj){
                if(typeof(cordova)!=='undefined'){
                    cordova.plugins.backgroundMode.enable();
                    cordova.plugins.backgroundMode.onactivate = function () {
                        cordova.plugins.backgroundMode.configure({
                            title:msj,
                            text:msj
                        });
                    };
                    navigator.vibrate(1000);
                }
            }
    };
});