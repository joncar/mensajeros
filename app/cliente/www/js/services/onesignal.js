services.factory('oneSignal',function($rootScope,UI,$ionicLoading,$ionicPlatform,Api,$http,$ionicPopup){
    return {
            init:function($scope){                
                if(typeof(window.plugins)!=='undefined' && typeof(window.plugins.OneSignal)!=='undefined'){
                 
                 var notificationOpenedCallback = function(jsonData) {
                 console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
                 };
                 
                 window.plugins.OneSignal
                 .startInit("8486056a-23b5-4103-abd5-db26a28d3ed1", "572047331159")
                 .handleNotificationOpened(notificationOpenedCallback)
                 .endInit();
                 window.plugins.OneSignal.getIds(function(data){
                        if(localStorage.gcm!==data.userId){
                        $scope.data = {clientes_id:$scope.clientes_id,valor:data.userId,tipo:ionic.Platform.isIOS()?'IOS':'Android'};
                            Api.insert('gcm_clientes',$scope,$http,function(data){
                                       localStorage.gcm = $scope.data.valor;
                            });
                        }
                 });
                 }
            }
    };
});
