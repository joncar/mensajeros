//var url = 'http://localhost/proyectos/pizzas/api/';
//var ipServer = 'http://localhost:3001'; //Ip del servidor socket
var url = 'https://www.mensajeros.ec/api_clientes/';
var urlS = 'https://www.mensajeros.ec/';
var myPopup;
var ws;
var userconnect = localStorage.user;
var services = angular.module('starter.services', [])
.factory('Api', function() {
  var email = '';
  var password = '';
  var data = "";
  return {
    list:function(controller,data,$scope,$http,successFunction,operator){
        this.datos = data;
        if(typeof(data)=='object'){
            var s = '';            
            for(var i in data){
                s+= 'search_field[]='+i+'&search_text[]='+data[i]+'&';
            }
            data = s;
            data += operator==undefined?'operator=where':'operator='+operator;
        }
        if($scope.loading!=undefined){
            $scope.loading('show');
        }
        var insta = this;
        $http({
            url:url+controller+'/json_list',
            method: "POST",
            data:data,
            transformRequest:angular.identity,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
              'ver': version,
              'repartidor':userconnect
            }           
          }).success(function(data){
             if(data==='denied'){
              alert('Esta version de la app esta obsoleta, debe actualizarla a su mas reciente version');
             }
             if(successFunction!==undefined){                 
                 successFunction(data);
                if($scope.loading!=undefined){
                    $scope.loading('hide');
                }
             }
          })
        .catch(function(data, status, headers, config){ // <--- catch instead error            
            if($scope.loading!=undefined){
                $scope.loading('hide');
            }
            //var insta = this;
            setTimeout(function(){
                insta.list(controller,this.datos,$scope,$http,successFunction,operator);
            },5000);
            //lert('Ha sido imposible conectar con el servidor, por favor verifica tu conexión a internet');
        });
    },
    insert:function(controller,$scope,$http,successFunction){       
       if(!$scope.data){//Si ls datos son de un formulario
            d = document.getElementById('formreg');
            data = new FormData(d);  
        }//Si se envia el array
        else{
            data = new FormData();
            for(i in $scope.data){
              data.append(i,$scope.data[i]);
            }            
        }
       if(typeof($scope.loading)!=='undefined'){
            $scope.loading('show');
        }
       $http({
            url:url+controller+'/insert_validation',
            method: "POST",
            data:data,
            transformRequest: angular.identity,
            headers: {
              'Content-Type':undefined,
              'ver': version,
              'repartidor':userconnect
            }           
          }).success(function(data){
              data = data.replace('<textarea>','');
              data = data.replace('</textarea>','');
              data = JSON.parse(data);
              if(data.success){
                  if(!$scope.data){//Si ls datos son de un formulario
                        d = document.getElementById('formreg');
                        data = new FormData(d);  
                  }//Si se envia el array
                  else{
                      data = new FormData();
                      for(i in $scope.data){
                        data.append(i,$scope.data[i]);
                      }            
                  }       
                  $http({
                        url:url+controller+'/insert',
                        method: "POST",
                        data:data,
                        transformRequest: angular.identity,
                        headers: {
                          'Content-Type':undefined,
                          'ver': version,
                          'repartidor':userconnect
                        }           
                      }).success(function(data){
                          if(data==='denied'){
                            alert('Esta version de la app esta obsoleta, debe actualizarla a su mas reciente version');
                          }
                          data = data.replace('<textarea>','');
                          data = data.replace('</textarea>','');
                          data = JSON.parse(data);                          
                          if(data.success){
                               if(successFunction!==undefined){
                                   successFunction(data);
                                   if(typeof($scope.loading)!=='undefined'){
                                        $scope.loading('hide');
                                    }
                               }
                          }
                          else{
                             if(typeof($scope.loading)!=='undefined'){
                                        $scope.loading('hide');
                                    }
                             $scope.showAlert('Ocurrio un error al añadir','Ocurrio un error al añadir');
                          }
                        })
                        .error(function(){
                            alert('Ha ocurrido un error interno, contacte con un administrador');
                        });
              }
              else{
                  if(typeof($scope.loading)!=='undefined'){
                        $scope.loading('hide');
                    }
                  $scope.showAlert('Ocurrio un error al añadir',data.error_message);
              }
          })
          .error(function(data){
              alert('Ha ocurrido un error interno, contacte con un administrador');
          });
        },
        
        update:function(controller,id,$scope,$http,successFunction){
        if(!$scope.data){//Si ls datos son de un formulario
          d = document.getElementById('formreg');
          data = new FormData(d);
        }//Si se envia el array
        else{
          data = new FormData();
          for(i in $scope.data){
            data.append(i,$scope.data[i]);
          }
        }
        if(typeof($scope.loading)!=='undefined'){
            $scope.loading('show');
        }
        $http({
             url:url+controller+'/update_validation/'+id,
             method: "POST",
             data:data,
             transformRequest: angular.identity,
             headers: {
               'Content-Type':undefined,
               'ver': version,
               'repartidor':userconnect
             }           
           }).success(function(data){
               data = data.replace('<textarea>','');
               data = data.replace('</textarea>','');
               data = JSON.parse(data);
               if(data==='denied'){
                alert('Esta version de la app esta obsoleta, debe actualizarla a su mas reciente version');
               }
               if(data.success){
                    if(!$scope.data){//Si ls datos son de un formulario
                      d = document.getElementById('formreg');
                      data = new FormData(d);
                    }//Si se envia el array
                    else{
                      data = new FormData();
                      for(i in $scope.data){
                        data.append(i,$scope.data[i]);
                      }
                    }       
                   $http({
                         url:url+controller+'/update/'+id,
                         method: "POST",
                         data:data,
                         transformRequest: angular.identity,
                         headers: {
                           'Content-Type':undefined,
                           'ver': version,
                           'repartidor':userconnect
                         }           
                       }).success(function(data){
                           data = data.replace('<textarea>','');
                           data = data.replace('</textarea>','');
                           data = JSON.parse(data);                          
                           if(data.success){
                                if(successFunction!==undefined){
                                   successFunction(data);
                                   if(typeof($scope.loading)!=='undefined'){
                                        $scope.loading('hide');
                                    }                                   
                                }
                           }
                           else{
                              if(typeof($scope.loading)!=='undefined'){
                                    $scope.loading('hide');
                                }
                              $scope.showAlert('Ocurrio un error al añadir','Ocurrio un error al añadir');
                           }
                    });
               }
               else{
                   if(typeof($scope.loading)!=='undefined'){
                        $scope.loading('hide');
                    }
                   $scope.showAlert('Ocurrio un error al añadir',data.error_message);
               }
          });
        },
        
        deleterow:function(controller,id,$scope,$http,successFunction){        
        $scope.loading('show');
        $http({
             url:url+controller+'/delete/'+id,
             method: "GET",
             data:'',
             transformRequest: angular.identity,
             headers: {
               'Content-Type':undefined,
               'ver': version,
               'repartidor':userconnect
             }           
           }).success(function(data){
               if(data==='denied'){
                alert('Esta version de la app esta obsoleta, debe actualizarla a su mas reciente version');
               }
               if(data.success){
                    if(successFunction!==undefined){
                        successFunction(data);
                        $scope.loading('hide');
                     }
               }
               else{
                   $scope.loading('hide');
                   $scope.showAlert('Ocurrio un error al eliminar',data.error_message);
               }
          });
        },

        initInterface:function($scope){

          return $scope;
        },

        query:function(controller,$scope,$http,successFunction){
            if(!$scope.data){//Si ls datos son de un formulario
              d = document.getElementById('formreg');
              data = new FormData(d);
            }
            else{
                data = new FormData();
                for(i in $scope.data){
                  data.append(i,$scope.data[i]);
                }
            }            
            
            if($scope.loading!=undefined){
                $scope.loading('show');
            }
            $http({
                 url:url+controller,
                 method: "POST",
                 data:data,
                 transformRequest: angular.identity,
                 headers: {
                   'Content-Type':undefined,
                   'ver': version,
                   'repartidor':userconnect
                 }           
               }).success(function(data){
                  if(data==='denied'){
                    alert('Esta version de la app esta obsoleta, debe actualizarla a su mas reciente version');
                   }
                    if(successFunction!==undefined){
                        successFunction(data);
                        if($scope.loading!=undefined){
                            $scope.loading('hide');
                        }
                     }
              }).error(function(data){
                  $scope.loading('hide');
                  $scope.showAlert('Ocurrio un error al enviar los datos',data);
              });
        }
    }   
})

.factory('UI', function() {
    return {    
        showPopupBox:function($ionicPopup,$scope,message,title,buttonAccept){
            $scope.data = {}
            // An elaborate, custom popup
              myPopup = $ionicPopup.show({
              template: message,
              title: title,              
              scope: $scope,
              buttons: [
                { 
                 text: 'Cancelar',
                 onTap:function(e){
                     myPopup.close();
                 }                 
                },
                buttonAccept
              ]
            });
            myPopup.then(function(res) {
              console.log('Tapped!', res);
            });
            
            /*ButtonAccept = {text: buttonAccept['text'],
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.data.wifi) {
                //don't allow the user to close unless he enters wifi password
                e.preventDefault();
              } else {
                return $scope.data.wifi;
              }
            }
          }}*/
        },
        getShowAlert:function($ionicPopup){            
            return function(title,template){
                if(this.alertMostrado===undefined){                                    
                    var scope = this;
                    this.alertMostrado = $ionicPopup.show({
                    template: template,
                    title: title,                
                    buttons: [                  
                      {
                        text: '<b>Aceptar</b>',
                        type: 'button-positive',
                        onTap: function(e) {                      
                          scope.alertMostrado.close();
                          scope.alertMostrado = undefined;
                        }
                      }
                    ]
                  });
                };
            };
        },
        getLoadingBox:function($ionicLoading,message){
            message = message==undefined?'Cargando...':message;
            return function(attr,msj){            
                attr=='hide'?$ionicLoading.hide():$ionicLoading.show({template: msj||message});
            };
        }, 
        
        getConfirmBox:function($ionicPopup) {
           return function(title,template,acceptFunction,denyFunction){
               var confirmPopup = $ionicPopup.confirm({
                    title: title,
                    template: template
                  });
                  confirmPopup.then(function(res) {
                    if(res) {
                      if(acceptFunction)acceptFunction();
                    } else {
                      if(denyFunction)denyFunction();
                    }
                  });
            };
        },
        
        getModalBox:function($ionicModal,template,$scope,callback){
            $ionicModal.fromTemplateUrl(template, {//Invitar personas
                scope: $scope,
                animation: 'slide-in-up'
              }).then(function(modal) {
                $scope.modal = modal;
                if(typeof(callback)!=='undefined'){
                    callback(modal);
                }
              });

            $scope.toggleModal = function(attr) {
              if(attr==undefined || attr=='show'){
                  $scope.modal.show();
              }
              else{
                  $scope.modal.hide();    
              }
            };            
            
            $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {                
                $scope.toggleModal('hide');
            });
        },
        
        getLocationName:function(lat,lon,callback){
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'latLng': new google.maps.LatLng(lat,lon)}, function(results, status) {
                //calle                                 
                callback(results[0]);
            });
        }
    };
})

.factory('User', function() {  
  return {
        setData:function(data){            
            localStorage.user = data.id;
            localStorage.nombre = data.nombre;
            localStorage.email = data.email;            
            localStorage.celular = data.celular;
            localStorage.foto = data.foto!==''?urlS+'img/fotos/'+data.foto:'img/vacio.png';
            localStorage.placa = data.placa;
            localStorage.rank = data.calificacion;
            localStorage.disponible = 1;
            localStorage.clientes_id = data.clientes_id;
            localStorage.password = data.password;
            localStorage.ruc = data.ruc;
            localStorage.direccion = data.direccion;
            localStorage.convenio = data.convenio;
        },
        
        getData:function($scope){
            $scope.user = localStorage.user;
            $scope.email = localStorage.email;        
            $scope.nombre = localStorage.nombre;                  
            $scope.telefono = localStorage.telefono;
            $scope.celular = localStorage.celular;
            $scope.foto = localStorage.foto;
            $scope.placa = localStorage.placa;
            $scope.rank = localStorage.rank;
            $scope.clientes_id = localStorage.clientes_id;
            $scope.password = localStorage.password;
            $scope.ruc = localStorage.ruc;
            $scope.direccion = localStorage.direccion;
            $scope.convenio = localStorage.convenio;
            return $scope;
        },
        
        cleanData:function(){
            localStorage.removeItem('user');
            localStorage.removeItem('nombre');            
            localStorage.removeItem('email');            
            localStorage.removeItem('celular');            
            localStorage.removeItem('foto');            
            localStorage.removeItem('placa');            
            localStorage.removeItem('rank');
            localStorage.removeItem('lat');
            localStorage.removeItem('lon');
            localStorage.removeItem('gcm');
            localStorage.removeItem('disponible');
            localStorage.removeItem('clientes_id');
            localStorage.removeItem('password');
            localStorage.removeItem('tour');
            localStorage.removeItem('ruc');
            localStorage.removeItem('direccion');
            localStorage.removeItem('convenio');
        }
    }  
})

.factory('Group', function() {  
  return {
     list:[],    
     add:function(cliente){
        var pos = this.getPosition(cliente.id);
        if(pos===null)
        this.list.push(cliente);
        else this.list[pos] = cliente;
    },    
    remove:function(cliente){
        if(cliente!==undefined){
            var aux = [];
            for(var i in this.list){
                if(this.list[i].id!==cliente.id){
                    aux.push(this.list[i]);
                }
            }
            this.list = aux;
        }
    },
    update:function(cliente){
        for(var i in this.list){
            if(this.list[i].id===cliente.id){
                this.list[i] = cliente;
            }
        }
    },
    getFromId:function(id){
        var cliente = null;
        for(var i in this.list){
            if(this.list[i].id===id){
                cliente = this.list[i];
            }
        }
        return cliente;
    },    
    getPosition:function(id){
        var cliente = null;
        for(var i in this.list){
            if(this.list[i].id===id){
                cliente = i;
            }
        }
        return cliente;
    },
    save:function(name){
        this.erase(name);
        localStorage[name] = JSON.stringify(this.list);
    },
    load:function(name){
        var l = typeof(localStorage[name])!=='undefined'?JSON.parse(localStorage[name]):[];
        this.list = typeof(this.list)!=='undefined'?l:[];
        if(this.list===undefined){
            this.list = [];
        }
        return this;
    },
    erase:function(name){
        localStorage.removeItem(name);
        console.log(name);
    }
  };
});