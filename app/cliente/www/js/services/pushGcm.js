services.factory('pushGcm',function($rootScope,UI,$ionicLoading,$ionicPlatform,Api,$http,$ionicPopup){
    return {
            init:function($scope){                
                if(typeof(window.PushNotification)!=='undefined'){
                    var push = window.PushNotification.init({
                        android: {
                            senderID: "572047331159"
                        },
                        ios: {
                            alert: "true",
                            badge: true,
                            sound: 'false'
                        },
                        windows: {}
                    });                    
                    push.on('registration', function(data) {                        
                         if(localStorage.gcm!==data.registrationId){                            
                            $scope.data = {clientes_id:$scope.clientes_id,valor:data.registrationId,tipo:ionic.Platform.isIOS()?'IOS':'Android'};
                            Api.insert('gcm_clientes',$scope,$http,function(data){
                                localStorage.gcm = $scope.data.valor;
                            });
                        }
                    });

                    push.on('notification', function(data) {
                        if(typeof($scope.alertPopup)==='undefined'){
                            $scope.showAlert = UI.getShowAlert($ionicPopup);
                            $scope.showAlert(data.title,data.message);
                            $rootScope.$broadcast('updateReloj');
                            if(typeof(data.additionalData.pedido)!=='undefined'){
                                document.location.href="#/tab/rastrear/"+data.additionalData.pedido;
                            }                                                            
                        }
                    });

                    push.on('error', function(e) {
                        alert( e.message);
                    });
                }
            }
    };
});