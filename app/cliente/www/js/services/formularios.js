services.factory('Formularios', function() {
    return {      
      registro:function($scope){
        $scope.inputs = [
            {name:'nombre',placeholder:'Nombre',rules:'required'},
            {name:'ciudades_id',placeholder:'Ciudad',type:'enum',options:{1:'Quito'},rules:'required|numeric'},
            {name:'email',placeholder:'Email de contacto',type:'email',rules:'required|valid_email'},
            {name:'celular',placeholder:'Celular',type:'number',rules:'required|numeric'},
            {name:'password',placeholder:'Contraseña',type:'password',rules:'required'},
            {name:'direccion',placeholder:'Direccion',rules:'required'}
        ];
        $scope.buttons = [
            {name:'CANCELAR',type:'a',href:'#/tab/main',class:'button-royal'},
            {name:'REGISTRAR',type:'submit',class:'button-calm'}
        ];
        return $scope;
      }      
    };
});